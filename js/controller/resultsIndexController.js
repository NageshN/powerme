PowerMe.ResultsIndexController = Ember.ObjectController.extend({
    needs: ['navigation', 'content','results'],
    navigation: Ember.computed.alias('controllers.navigation'),
    isExpandedResults: false,
   
    
    pageCount: function(){
    	var pageCount = Math.ceil(this.get('data.totalFiles')/200)-1 ;
    	if(pageCount === -1 ){ pageCount = 0;}
    	this.set('controllers.results.pageCount', pageCount);
    	this.set('controllers.results.totalFiles', this.get('data.totalFiles'));
    	return pageCount;
    }.observes('data.totalFiles'),

    
    
    order : "descending",
	orderFlag : function(){
		if(this.get('order') === "descending") {
			return true;
		}else{
			return false;
		}
	}.property('order'),
	orderKey : function(){
		if(this.get('order') === "descending") {
			return "desc";
		}else{
			return "asc";
		}
	}.property('order'),
    actions: {
    	/**
    	 * backtoHome action takes the user at a homepage
    	 */
        backtoHome: function() {
        	window.scrollTo(0,0);
        	this.set('controllers.content.fromResultPage', false);
            this.transitionToRoute('navigation.index');
        },
        /**
    	 * gotoContentpage action will tells the application that it reaches to content page via result template.
    	 * with the help of "fromResultPage" variable goto "search" option is visible on the content page.
    	 */
        gotoContentPage: function() {
            this.set('controllers.content.fromResultPage', true);
            this.set('controllers.navigation.search', "");
            this.set('controllers.content.type', "resultsPage");
            window.scrollTo(0,0);
        },
        
        /**
         * bookmark action from bookmarklet component is handled here
         */
        bookmark: function(data){
        	// this will update the result, cache inside content model for the report user bookmarked
        	this.store.find('content', data.id).then(function(result) {
        		console.log("render data");
        		result.set("bookmark.bookmarkState",true);
        		result.set("bookmark.bookmarkId",data.get("bookmark.bookmarkId"));
        	});
        	//this.store.find('content', data.id).reload();
        	console.log("bookmark me!!");
        },
        /**
         * unbookmark action from bookmarklet component is handled here
         */
        unBookmark: function(data){
        	// this will update the result, cache inside content model for the report user unbookmarked
        	this.store.find('content', data.id).then(function(result) {
        		console.log(result.get("bookmark.bookmarkState"));
        		result.set("bookmark.bookmarkState",false);
        		result.set("bookmark.bookmarkId","");
        	});
        	//this.store.find('content', data.id).reload();
        },
        sortByUsage : function(){
        	console.log("sort By Usage");
			if(this.get('order') === "descending") {
				this.set('order',"ascending");
			}else{
				this.set('order',"descending");
			}
			var query = this.get('controllers.navigation.searchQuery');
    		var queryType = this.get('controllers.navigation.clickedSelectedOption');
    		var sort = this.get('orderKey');
    		var currentPage = parseInt(this.get('controllers.results.startPage'));
			this.transitionToRoute('results',queryType, query ,currentPage,sort  );
		}
    }
});