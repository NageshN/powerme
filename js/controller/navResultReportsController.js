PowerMe.NavResultReportsController = Ember.ObjectController.extend({
	needs: ['content', 'navigationResults'],
	actions: {
    	/**
    	 * backtoHome action takes the user at a homepage
    	 */
        backtoHome: function() {
            this.transitionToRoute('navigation.index');
            window.scrollTo(0,0);
        },
        /**
    	 * gotoContentpage action will tell the application that it reaches to content page via result template.
    	 * with the help of "fromResultPage" variable goto "search" option is visible on the content page.
    	 */
        gotoContentPage: function() {
            this.set('controllers.content.fromResultPage', true);
            this.set('controllers.content.type', "navigationRsultsPage");
            window.scrollTo(0,0);
        },
        /**
         * bookmark action from bookmarklet component is handled here
         */
        bookmark: function(data){
        	// this will update the result, cache inside content model for the report user bookmarked
        	this.store.find('content', data.id).then(function(result) {
        		result.set("bookmark.bookmarkState",true);
        		result.set("bookmark.bookmarkId",data.get("bookmark.bookmarkId"));
        	});
        	console.log("bookmark me!!");
        },
        /**
         * unbookmark action from bookmarklet component is handled here
         */
        unBookmark: function(data){
        	// this will update the result, cache inside content model for the report user unbookmarked
        	this.store.find('content', data.id).then(function(result) {
        		console.log(result.get("bookmark.bookmarkState"));
        		result.set("bookmark.bookmarkState",false);
        		result.set("bookmark.bookmarkId","");
        	});
        	console.log("roadblock 1");
        }
	}
});