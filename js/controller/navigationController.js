/**
 * PowerMe Navigation Controller
 */
PowerMe.NavigationController = Ember.ObjectController.extend({
    needs: ['content', 'loginPage', 'results','bookmarks', 'homePage','navigationResults','navigationIndex','resultsIndex','sankey'],
    logoUrl: "images/arrowOff.png",
    mode: false,
    persistId: null,
    search: '',
    searchQuery:null,
    isSystemExpanded: false,
    selectedOption : "All",
    clickedSelectedOption: "All",
    usrename: function(){
    	if(this.session.get('isAuthenticated')){
    		return this.session.get('firstName') + this.session.get('lastName');
    	}else{
    		return "";
    	}
    }.observes('session.isAuthenticated'),
    actions: {
        /**
         * expandCollapse action controls the hide and show of Navigation template over content template.
         */
        expandCollapse: function() {
            if (this.get('mode')) {
                this.set('mode', false);
                this.set('logoUrl', "images/arrowOff.png");
                //Setting variable to expand the content and title on the right 
                this.set('controllers.content.isExpanded', false);
                this.set('controllers.results.isExpandedResults', false);
                this.set('controllers.bookmarks.isExpandedBookmarks', false);
                this.set('controllers.homePage.isExpandedHome', false);
                this.set('controllers.navigationResults.isExpandedNavigationResults', false);
            } else {
                this.set('mode', true);
                this.set('logoUrl', "images/arrowOn.png");               
                //Setting variable to collapse the content and title on the right 
                this.set('controllers.content.isExpanded', true);
                this.set('controllers.results.isExpandedResults', true);
                this.set('controllers.bookmarks.isExpandedBookmarks', true);
                this.set('controllers.homePage.isExpandedHome', true);
                this.set('controllers.navigationResults.isExpandedNavigationResults', true);
            }
        },
        
        selectValue : function(data){
        	this.set('selectedOption',data)
        	//console.log(data);
        },
        openNavBar : function(){
        	$(".canvasMenu").toggleClass("off-screen");
        },
        blankSearchBox : function(){
        	
        	this.set("search","");
        	this.set('controllers.navigationIndex.flag',true);
        	$('.selectpicker').val('filesLocation');
        	this.set('controllers.content.fromResultPage', false);
        	window.scrollTo(0,0);
        	/*this.refresh();*/
        },
       
        depExpandCollapse: function(key){
        	if($('li.departmentNavigation.' + key).hasClass('dep-collapse') == true){
        		$('li.departmentNavigation.' + key).removeClass('dep-collapse');
            	$('li.departmentNavigation.' + key).addClass('dep-expand');
            	$('span.' + key).removeClass('fa-angle-right');
            	$('span.' + key).addClass('fa-angle-down');
            	$('li.subjectAreaNavigation.' + key).removeClass('hidden');
        	}
        	
        	else{
        		console.log('false');
        		$('li.departmentNavigation.' + key).addClass('dep-collapse');
            	$('li.departmentNavigation.' + key).removeClass('dep-expand');
            	
            	$('span.' + key).addClass('fa-angle-right');
            	$('span.' + key).removeClass('fa-angle-down');
            	$('li.subjectAreaNavigation.' + key).addClass('hidden'); 
            	$('li.subjectAreaNavigation.' + key).addClass('hidden'); 
            	$('li.dashboardAreaNavigation.' + key).addClass('hidden');
            	$('li.subjectAreaNavigation.' + key).addClass('dep-collapse');
            	$('li.subjectAreaNavigation.' + key).removeClass('dep-expand');
            	
            	$('.iconSubject').removeClass('fa-angle-down');
            	$('.iconSubject').addClass('fa-angle-right');
        	}
        
        },
        
        subjectAreaExpandCollapse: function(key){
        	//console.log('click dep');
            if($('li.subjectAreaNavigation.' + key).hasClass('dep-collapse') == true){
            	
                $('li.subjectAreaNavigation.' + key).removeClass('dep-collapse');
                $('li.subjectAreaNavigation.' + key).addClass('dep-expand');
                $('span.' + key).removeClass('fa-angle-right');
                $('span.' + key).addClass('fa-angle-down');
                $('li.dashboardAreaNavigation.' + key).removeClass('hidden');
            }
            
            else{
            	
                $('li.subjectAreaNavigation.' + key).addClass('dep-collapse');
                $('li.subjectAreaNavigation.' + key).removeClass('dep-expand');
                $('span.' + key).addClass('fa-angle-right');
                $('span.' + key).removeClass('fa-angle-down');
                $('li.dashboardAreaNavigation.' + key).addClass('hidden');  
            }
        },
        menuCollapse:function(){
        	console.log('menuicon click');
        	console.log($('#collapse-menu').hasClass('hidden'));
        	if($('#collapse-menu').hasClass('hidden') != true){
        		$('#collapse-menu').addClass('hidden');
        		$('.navbar-collapse, .app-content, .app-footer').css('margin-left','60px');
            	$('#collapsed-menu').removeClass('hidden');
            	$('#collapsed-menu').addClass('app-aside-collapse');
            	$('#small-logo').removeClass('hidden');
            	$('#main-logo').addClass('hidden');
            	$('.navbar-header').css('width','0px');
            	$('.click-to-collapse').removeClass('fa fa-outdent');
            	$('.click-to-collapse').addClass('fa fa-indent');
        	}
        	else{
        		$('#collapse-menu').removeClass('hidden');
        		$('.navbar-collapse, .app-content, .app-footer').css('margin-left','250px');
            	$('#collapsed-menu').addClass('hidden');
            	$('#collapsed-menu').removeClass('app-aside-collapse');
            	$('#main-logo').removeClass('hidden');
            	$('#small-logo').addClass('hidden');
            	$('.navbar-header').css('width','250px');
            	$('.click-to-collapse').removeClass('fa fa-indent');
            	$('.click-to-collapse').addClass('fa fa-outdent');
        	}
        	
        	
        },
       
       
        /*refreshNavigationIndex : function(){
        	this.set("search","");
        	this.transitionToRoute('navigation.index');
        	console.log("go charley go");
        },*/
        /**
         * Here gotoContentPage action will tells the application that user doesn't land on content page from results template.
         */
        gotoContentPage: function() {
            this.set('controllers.content.fromResultPage', false);
            this.set('controllers.content.type', "navigation");
            window.scrollTo(0,0);
        },
        /**
         * searchText action will send the query string to the results template where search functionality is handled and 
         * displayed on the results template.
         */
        searchText: function() {
        	
        	
        	$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
        	this.set('controllers.resultsIndex.order',"descending");
        	this.set('controllers.navigationIndex.flag',true);
        	this.set('controllers.results.subjectAreaData',"");
        	this.set('clickedSelectedOption', this.get('selectedOption'));
            var query = encodeURIComponent(this.get('search'));
            var queryType = this.get('selectedOption');
            query = (query === "") ? "*" : query;
            this.set('searchQuery', query);
            var sort = this.get('controllers.resultsIndex.orderKey');
            window.scrollTo(0,0);
            this.transitionToRoute('results',queryType, query ,"1", sort );
            
        },
        persistState: function(objectId) {
            /**
             * persist the state when user directly view any content
             */
            this.setProperties({                
                persistId: objectId
            });
        },
        moveToHomePage: function(){
        	this.set("controllers.loginPage.isUserLoggedIn",false);  	      	
        },
        showFilenLocation: function(){
        	console.log("i was in showFilenLocation");
        },
        showSizenLocation: function(){
        	console.log("i was in showSizenLocation");
        }	
       
        
    }
});

PowerMe.NavigationIndexController = Ember.ObjectController.extend({	
	flag:true,
    expandSubjectTiles: false,   
	init: function(){		
		this.set("flag",true);		
	},
	actions : {
		categoryClickBusi : function(e){
			$('#system').hide();
			$('#business').removeClass('hidden');
			$('#bus-butn').removeClass('search-category-li');
			$('#bus-butn').addClass('search-category-li-active');
			$('#sys-butn').removeClass('search-category-li-active');
			$('#sys-butn').addClass('search-category-li');
			$('#business').show();
			
		},
		categoryClickSystem : function(){
			$('#business').hide();
			$('#system').show();
			$('#sys-butn').removeClass('search-category-li');
			$('#sys-butn').addClass('search-category-li-active');
			$('#bus-butn').removeClass('search-category-li-active');
			$('#bus-butn').addClass('search-category-li');
			
		},
		switchGraph: function(params){
			var graphType = $(".selectpicker").val();
			if(graphType === "filesLocation"){
				this.set("flag",true);
				$('#fileNSizeTitle').html("No. of Files Vs. Location (top 10)");
			
				
			}else{
				this.set("flag",false);
				$('#fileNSizeTitle').html("Size Vs. Location (top 10)");
			}
        	//console.log("i was in switchGraph");
        },
        showfileLocationBar: function(){
        	$('#fileLocationDrillDownBar').empty();
        	$('#fileLocationDrillDownBar').hide();
            $('#fileLocationBar').css('display','block');
            $('#fileLocationBackButton').css('display','none');
            $('#fileNSizeTitle').html("No. of Files Vs. Location (top 10)");
        },
        showfileLastAccessedBar: function(){
        	$('#fileLastAccessedDrillDownBar').empty();
        	$('#fileLastAccessedDrillDownBar').hide();
            $('#fileLastAccessedBar').css('display','block');
            $('#fileLastAccessedBackButton').css('display','none');
            $('#fileLastAccessedTitle').html("No. of Files Vs. Last Accessed (top 5)");
        },
        showSizeLocationBar: function(){
        	$('#sizeLocationDrillDownBar').empty();
        	$('#sizeLocationDrillDownBar').hide();
            $('#sizeLocationBar').css('display','block');
            $('#sizeLocationBackButton').css('display','none');
            $('#fileNSizeTitle').html("Size Vs. Location (top 10)");
        },
        showAllSubjectTiles: function(){
        	console.log("In show all subject tiles");
        	if(this.get('expandSubjectTiles') == true){
        		this.set('expandSubjectTiles', false);        		
        	}else{
        		this.set('expandSubjectTiles', true);
        	}        	
        },        
        showFileUsageVsSystemPie: function(){
        	$('#fileUsageSubjectAreaPie').empty();
        	$('#fileUsageSubjectAreaPie').hide();
        	$('#filesUsageSystemPie').css('display', 'block');
        	$('#fileUsageSubjectBack').css('display', 'none');
        	$('#fileUsageNSystemTitle').html("Report Usage by System");
        },
        showFileNumberVsSystemPie: function(){
        	$('#filesCountSubjectAreaPie').empty();
        	$('#filesCountSubjectAreaPie').hide();
        	$('#filesCountSystemPie').css('display', 'block');
        	$('#fileNumberSubjectBack').css('display', 'none');
        	$('#fileCountNSystemTitle').html("Reports by System");
        },
        showFileNumberVsSubjectPie: function(){
        	$('#filesNUsageCountPie').empty();
        	$('#filesNUsageCountPie').hide();
        	$('#filesCountSubjectAreaPie').css('display', 'block');
        	$('#fileNumberSubjectBack').show();
        	$('#fileNFileUsageBack').css('display', 'none');
        	$('#fileCountNSystemTitle').html("Reports by Application");
        },
       
        updateTopReportsTiles: function(dataArray, dataModel){        	 
        	var arrayLength = dataModel.content.top5ReportsData.length;
        	var dataArrayLength = dataArray.length;
        	for(var count=0; count< arrayLength; count++){
        		dataModel.content.top5ReportsData.popObject();
        	}   
        	for(var count=0; count< dataArrayLength; count++){
        		dataModel.content.top5ReportsData.pushObject(dataArray[count]);
        	}       	
        },
        updateTopUsersTiles: function(dataArray, dataModel){
        	var arrayLength = dataModel.content.top5UsersData.length;
        	var dataArrayLength = dataArray.length;
        	for(var count=0; count< arrayLength; count++){
        		dataModel.content.top5UsersData.popObject();
        	}
        	for(var count=0; count< dataArrayLength; count++){
        		dataModel.content.top5UsersData.pushObject(dataArray[count]);
        	} 
        }
    }
	
});