/**
 * PowerMe Content Controller starts here
 */
PowerMe.ContentController = Ember.ObjectController.extend({
    needs: ['navigation','navigationResults','resultsIndex','results', 'content'],
    isExpanded: false,
    fromResultPage: false,
    
    
    type: "content",
    commentMessage:"",
    actions: {
        backtoResult: function(type) {
        	switch(type){
        	case "bookmarks":
        		window.scrollTo(0,0);
        		this.set('fromResultPage', false);
        		this.transitionToRoute('bookmarks'); 	      		
        		break;
        	case "resultsPage":
        		var query = this.get('controllers.navigation.searchQuery');
        		var queryType = this.get('controllers.navigation.clickedSelectedOption');
        		var sort = this.get('controllers.resultsIndex.orderKey');
        		this.set('controllers.results.subjectAreaData',"");
        		this.set('fromResultPage', false);
        		window.scrollTo(0,0);
                this.transitionToRoute('results',queryType,query,"1",sort);             
        		break;
        	case "navigationRsultsPage":
        		var query = this.get('controllers.navigationResults.navigationQueryEncoded');
        		var system = this.get('controllers.navigationResults.navigationSystem');
        		var department = this.get('controllers.navigationResults.navigationDepartment'); 
        		this.set('fromResultPage', false);
        		window.scrollTo(0,0);
        		this.transitionToRoute('navigationResults', system, department, query);       		
        		break;
        	}
            
        },
        // bookmark action from bookmarklet will be handled here
        bookmark: function(data){
        	console.log("bookmark me!!");
        },
        addComment:	function(commentsData,reportId){
        	console.log("***************inside addComment***************")
        	var self = this;
        	var comment = this.store.createRecord('Comments', {
        		"userName":this.session.get("firstName")+" "+this.session.get("lastName"),
        		"userId":this.session.get("email"),
        		"docId":reportId,
        		"message" : this.get("commentMessage"),
        		"timing" : new Date().getTime()
    			});
        	comment.save().then(function(data){
        		commentsData.comments.pushObject(data);
        		self.set("commentMessage","");
        	});
        	var commentList = Ember.$(".commentList");
    		commentList.scrollTop(commentList.prop("scrollHeight"));
        	
        },
        // unbookmark action from bookmarklet will be handled here
        unBookmark: function(data){
        	console.log("unbookmark me!!");
        },
        gotoContentPage: function() {
            this.set('fromResultPage', true);
            this.set('controllers.navigation.search', "");
            this.set('controllers.content.type', "resultsPage");
            window.scrollTo(0,0);
        },
        gotoContentPageNested: function(){
        	this.set('fromResultPage', false);
            this.set('controllers.navigation.search', "");
            this.set('controllers.content.type', "content");
            window.scrollTo(0,0);
        },
        bookmarkNestedTile: function(data){
        	this.store.find('content', data.id).then(function(result) {
        		console.log("render data");
        		result.set("bookmark.bookmarkState",true);
        		result.set("bookmark.bookmarkId",data.get("bookmark.bookmarkId"));
        	});
        	console.log("Bookmarked!!");
        },
        unBookmarkNestedTile: function(data){
        	this.store.find('content', data.id).then(function(result) {
        		console.log(result.get("bookmark.bookmarkState"));
        		result.set("bookmark.bookmarkState",false);
        		result.set("bookmark.bookmarkId","");
        	});
        	console.log("Unbookmarked!!");
        },
        techExpandCollapse: function(){
        	if($('span.expandCollapseTechImage').hasClass('fa-angle-right')){
        		$('span.expandCollapseTechImage').removeClass('fa-angle-right');
        		$('span.expandCollapseTechImage').addClass('fa-angle-down');
        		
        	}
        	else{
        		$('span.expandCollapseTechImage').addClass('fa-angle-right');
        		$('span.expandCollapseTechImage').removeClass('fa-angle-down');
        		
        	}
        	
        }
    }
});
