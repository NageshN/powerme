PowerMe.ResultsController = Ember.ObjectController.extend({
	needs: ['navigation', 'content','resultsIndex','reports','sankey'],
	subjectAreaData: "",
	totalFiles: 0,
	startPage : 0,
	from:0,
	to:0,
	updateFromTo : function(){
		var from = this.get("totalFiles") ? (this.get("startPage")-1) *200 + 1 : 0;
		var	to	=  (this.get('startPage') > this.get("pageCount")) ? this.get("totalFiles") : (this.get("startPage") *200);
		
		this.set('from',from);
		this.set('to',to);
		
		
	}.observes("startPage","pageCount","totalFiles"),
	pageCount:0,
	type:"",
	actions: {
		resetAll : function(){
			
			var query = this.get('controllers.navigation.searchQuery');
    		var queryType = this.get('controllers.navigation.clickedSelectedOption');
    		var sort = this.get('controllers.resultsIndex.orderKey');
    		this.set('subjectAreaData',"");
    		$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
            this.transitionToRoute('results',queryType,query,"1",sort);
        },
        fillSubjectArea : function(subjectArea){
        	this.set("subjectAreaData",subjectArea);
        },
        
        fetchNextRecords: function(pageNumber,type){
        	console.log('In fetch Records');
        	var query = this.get('controllers.navigation.searchQuery');
    		var queryType = this.get('controllers.navigation.clickedSelectedOption');
    		
    		this.set("startPage",pageNumber);
    		if(this.get("type") === "reports"){
    			var sort = this.get('controllers.reports.orderKey');
    			var subjectArea= this.get('subjectAreaData');
    			var encodedSubjectArea = encodeURIComponent(subjectArea);
    			this.transitionToRoute('reports', encodedSubjectArea,pageNumber,sort);
    		}else{
    			var sort = this.get('controllers.resultsIndex.orderKey');
    			this.transitionToRoute('results', queryType,query,pageNumber,sort);
    		}
        } 
	}
	
});

