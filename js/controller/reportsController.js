PowerMe.ReportsController = Ember.ObjectController.extend({
	needs: ['navigation','content','results'],
	order : "descending",
	subjectAreaData : "",
	
	pageCount: function(){
    	var pageCount = Math.ceil(this.get('data.totalFiles')/200)-1 ;
    	if(pageCount === -1 ){ pageCount = 0;}
    	this.set('controllers.results.pageCount', pageCount);
    	this.set('controllers.results.totalFiles', this.get('data.totalFiles'));
    	return pageCount;
    }.observes('data.totalFiles'),
	
	orderFlag : function(){
		if(this.get('order') === "descending") {
			return true;
		}else{
			return false;
		}
	}.property('order'),
	orderKey : function(){
		if(this.get('order') === "descending") {
			return "desc";
		}else{
			return "asc";
		}
	}.property('order'),
	actions : {
		sortByUsage : function(){
			if(this.get('order') === "descending") {
				this.set('order',"ascending");
			}else{
				this.set('order',"descending");
			}
			var subjectArea= this.get('controllers.results.subjectAreaData');
			var orderKey = this.get('orderKey');
			var encodedSubjectArea = encodeURIComponent(subjectArea);
			var currentPage = this.get('controllers.results.startPage');
			this.transitionToRoute('reports',encodedSubjectArea,currentPage,orderKey);			
		},
		gotoContentPage: function() {
            this.set('controllers.content.fromResultPage', true);
            this.set('controllers.navigation.search', "");
            this.set('controllers.content.type', "resultsPage");
            window.scrollTo(0,0);
        },
        /**
         * bookmark action from bookmarklet component is handled here
         */
        bookmark: function(data){
        	// this will update the result, cache inside content model for the report user bookmarked
        	this.store.find('content', data.id).then(function(result) {
        		console.log("render data");
        		result.set("bookmark.bookmarkState",true);
        		result.set("bookmark.bookmarkId",data.get("bookmark.bookmarkId"));
        	});
        	//this.store.find('content', data.id).reload();
        	console.log("bookmark me!!");
        },
        /**
         * unbookmark action from bookmarklet component is handled here
         */
        unBookmark: function(data){
        	// this will update the result, cache inside content model for the report user unbookmarked
        	this.store.find('content', data.id).then(function(result) {
        		console.log(result.get("bookmark.bookmarkState"));
        		result.set("bookmark.bookmarkState",false);
        		result.set("bookmark.bookmarkId","");
        	});
        	//this.store.find('content', data.id).reload();
        	console.log("roadblock 1");
        }
	}
})