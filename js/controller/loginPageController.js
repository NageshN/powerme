/*PowerMe Login Page Controller*/

PowerMe.LoginPageController = Ember.ObjectController.extend({
	 error: { //error messages
		 	hasError : false, msg : ""
		},
		isUserLoggedIn: false,
		currentUser: '',
		username:"",
		password:"",
		
		actions: {
		
			authenticate: function(){
			    var username = Ember.$.trim(this.get('username')), 
				    password = this.get('password');
		
			    if(Ember.isEmpty(username)){ //empty username
			    	this.set('error.msg',"Enter an email address to log in.");
			    	Ember.$("input[name=username]").focus();
			    	this.validationFailed();		    	
			    }else if(Ember.isEmpty(password)){ //empty password
			    	this.set('error.msg',"Enter your password to log in.");
			    	Ember.$("input[name=password]").focus();
			    	this.validationFailed();		    	
			    }else if(!this.validateEmail(username)){ //invalid email address
			    	this.set('error.msg',"Enter a Valid Email Address");
			    	Ember.$("input[name=username]").focus();
			    	this.validationFailed();		    	
			    }else{
				    this.set('error.hasError',false);	
				    this.transitionToRoute('loading');
		
				    var self=this,
				    params = {
				        url: PowerMe.Constants.serviceUrl.loginURL ,
				        type: "POST",
				        async: true,
				        contentType: "application/json",
				        data: JSON.stringify({ email: username, password: password}),
				        success : function(response) {
					        if (response.status === "success" && response.object.sessionId) {			                    
								self.xhrsuccessful(response);
					        }
					        else {
					        	self.xhrfailed(response);
					        }
					      },
					    error : function(response) {
					        self.xhrfailed(response.responseJSON);
					    }
				  	};
				  	Ember.$.ajax(params);
				}
		 	}  
		},
		
		xhrsuccessful: function(obj){
			
			function setCookie(cname, cvalue, exdays) {
			    var d = new Date();
			    d.setTime(d.getTime() + (exdays*24*60*60*1000));
			    var expires = "expires="+d.toUTCString();
			    document.cookie = cname + "=" + cvalue + "; " + expires;
			}
			setCookie("sessionId",obj.object.sessionId,1);
			setCookie("userId",obj.object.userId,1);
			setCookie("firstName",obj.object.firstName,1);
			setCookie("lastName",obj.object.lastName,1);
			setCookie("email",obj.object.email,1);
		
			//set the session obj
			this.session.set('sessionId', obj.object.sessionId);
			this.session.set('userId', obj.object.userId);
			this.session.set('firstName', obj.object.firstName);
			this.session.set('lastName', obj.object.lastName);
			this.session.set('email', obj.object.email);
			this.set('username', null);
			this.set('password', null);
		
			try{
				var attemptedTransition = this.session.get("attemptedTransition");
				//transition to intro 
				if(attemptedTransition){
					this.session.set("attemptedTransition",null);
					//this.transitionToRoute(route);
					attemptedTransition.retry();
					
				}else{
					this.transitionToRoute('navigation');
				}
				
		   }
		   catch(e){}
		},
		
		xhrfailed: function(obj){
			var msg = obj.message;
			this.set('error.hasError',true);
			this.set('error.msg',msg);
			this.set('username', null);
			this.set('password', null);
			
			try{
		   	this.transitionToRoute('loginPage');
		   }
		   catch(e){}
		
			setTimeout(function(){
				Ember.$("input[name=username]").focus();
			},250);
		},
		
		validationFailed: function(){
			this.set('error.hasError',true);
			
			try{
		   	this.transitionToRoute('loginPage');
		   }
			catch(e){}
		},
		
		validateEmail: function(email) {
			var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return reg.test(email);
}});

