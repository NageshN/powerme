PowerMe.SessionController = Ember.ObjectController.extend({
	  sessionId: 				  "",
	  userId:                    "",
	  firstName:                   "",
	  lastName:                   "",
	  email:                      "",
	  attemptedTransition:					 "",

	  isAuthenticated: function(){
	    if(!Ember.empty(this.get('sessionId'))){
	      return true;
	    }
	    return false;
	  }.property('sessionId')
	
})