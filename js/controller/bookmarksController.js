PowerMe.BookmarksController = Ember.ObjectController.extend({
    needs: ['navigation','navigationIndex', 'content'],
    navigation: Ember.computed.alias('controllers.navigation'),
    isExpandedBookmarks: false,
    init: function(){
    	//alert('sss');
	},
    actions: {
    	/**
    	 * backtoHome action takes the user at a homepage
    	 */
        backtoHome: function() {
        	 window.scrollTo(0,0);
        	 this.set('controllers.content.fromResultPage', false);
            this.transitionToRoute('navigation.index');
           
        },
        /**
    	 * gotoContentpage action will tells the application that it reaches to content page via result template.
    	 * with the help of "fromResultPage" variable goto "search" option is visible on the content page.
    	 */
        gotoContentPage: function() {
            this.set('controllers.content.fromResultPage', true);
            this.set('controllers.content.type', "bookmarks");
            window.scrollTo(0,0);
        },
        /**
         * bookmark action from bookmarklet component is handled here
         */
        bookmark: function(data){
        	// this will update the result, cache inside content model for the report user bookmarked
        	this.store.find('result', data.get("docId")).then(function(result) {
        		console.log("render data");
        		result.set("bookmark.bookmarkState",true)
        	});
        	//this.store.find('content', data.id).reload();
        	console.log("bookmark me in bookmarks template!!");
        },
        /**
         * unbookmark action from bookmarklet component is handled here
         */
        unBookmark: function(data){
        	// this will update the result, cache inside content model for the report user unbookmarked
        	this.store.find('result', data.get("docId")).then(function(result) {
        		console.log("render data");
        		result.set("bookmark.bookmarkState",false)
        	});
        	/*this.store.find('bookmark', data.id).then(function(result) {
        		result.deleteRecord();
        	});*/
        	//this.store.find('content', data.id).reload();
        	console.log("unbookmark me in bookmarks template!!");
        }
    }
});