Ember.Application.initializer({
  name: 'session',
  after:'store',

  initialize: function(container, application) {
    application.register('session:main', PowerMe.SessionController , { instantiate: true, singleton: true });
    application.inject('adapter', 'session', 'session:main');  
    application.inject('controller', 'session', 'session:main'); 
    application.inject('route', 'session', 'session:main');  
    application.inject('component', 'session', 'session:main'); 
  }
});