/**
 * Different constant variables used across the application is defined below
 */
 PowerMe.Constants = {
		serviceUrl : {
			/**
			 * hostURL below can be used in the case where data needs to be loaded from different host. 
			 * To work with any host it needs to be CORS supported
			 */
			//hostURL:'http://localhost',
			// here "data" acts as a relative URL , to use as an absolute URL use "/data" 
			// In case no redirect is applied use "http://54.191.26.201:9200/powerme/" in place of /powermeservices
			
			baseURL: "http://localhost:9200/powermeservice",			
			navigationObjectURL:'/system/_search',
			searchResultURL : '/system/_search',
			bookmarkURL : '/bookmark/_search',
			bookmarkDelete : '/bookmark/',
			createBookmark : '/bookmark/',
			homePageURL: '/system/_search',
			navigationResultsURL: '/system/_search',
			filenLocationURL:'/doc/_search?pretty',
			sizenLocationURL:'/doc/_search?pretty',
			fileNLastAccessedURL:'/doc/_search?pretty',
			fileNOwnerURL:'/doc/_search?pretty',
			sizeNOwnerURL: '/doc/_search?pretty',
			fileNFileTypeURL: '/doc/_search?pretty',
			tagCloud : '/doc/_search?pretty',
			obieeNavigationURL: '/system/_search',
			obieeSubjectTileURL: '/system/_search',
			resultsSubjectTileURL: '/system/_search',
			obieeReportTileURL: '/system/_search',
			obieeNavigationResultsURL: '/system/_search',
			resultsReportsURL : '/system/_search',
			usageChartURL: '/usagegraph/_search',
			similarTilesURL: '/system/_similarity',
			loginURL:'/powerme-rest/login',
			logoutURL:	'/powerme-rest/logout/userid',
			commentsURL:	'/comments/_search',
			addCommentsURL:	'/comments/',
			top5ReportsDashboard: '/system/_search',
			navigationLeftPanelURL: '/system/_search',
			navigationLeftResultsURL: '/system/_search',
			newSearchResultURL: '/system/_search',
			newHomePageURL: '/system/_search',
			newSimilarTilesURL: '/system/_similarity',
			newResultsSubjectTileURL: '/system/_search',
			fileUsageNSystemURL: '/system/_search',
			fileNumbersNSystemURL: '/system/_search',
			fileUsageSubjectAreaURL: '/system/_search',
			top5UsersDashboard: '/usagegraph/_search',
			filesAccessedNMonthURL: '/monthgraph/_search',
			updateUsageChartURL:'/usagegraph/_search',
			dasboardTilesURL: '/system,usagegraph/_search'
		}
};