/**
 * Content Route starts below
 */
PowerMe.ContentRoute = Ember.Route.extend({	
    model: function(params) {
    	window.scrollTo(0,0);
    	var self = this;
    	console.log("inside content route");
    	var subjectName, seedValue, reportName, pathValue;
    	var newSeedValue = seedValue? seedValue: Math.floor((Math.random() * 4000) + 300);
    	
        /**
         * When user directly try to open any content page below code will persist the state in navigation
         * bar and the navigation bar collapse to the left.
         */
        this.controllerFor('navigation').send("persistState", params.content_id);
        
        
        /**
         * find the report related to provided ID 
         */
       var commentsQuery = {
               "type": "commentsURL",
               "query":  {
            	    "from": 0,
            	    "size": 100,
            	    "sort":{"timing" : { "order": "asc","mode" : "max"}},
            	    "query": {
            	        "query_string": {
            	            "default_field": "comments.docId",
            	            "query": params.content_id
            	        }
            	    }
            	}
           };
    	
        var completeData = Ember.RSVP.hash({
        	data: this.store.find('Content', params.content_id).then(function(contentData) {        		
            	reportName = contentData._data.name;
            	pathValue = contentData._data.path;
            	subjectName = contentData._data.subjectArea1;
            	seedValue = contentData._data.id;
            	console.log("roadblock 2");
            	var queryString = {
                	    "type": "bookmarkURL",
                	    "query": {
                	        "query": {
                	            "bool": {
                	                "must": [{
                	                    "query_string": {
                	                        "default_field": "bookmark.docId",
                	                        "query": contentData.get("id").toString()
                	                    }
                	                }, {
                	                    "query_string": {
                	                        "default_field": "bookmark.userId",
                	                        "query": self.session.get("email")
                	                    }
                	                }]
                	            }
                	        }
                	    }
                	};
            	
            	/**
            	 * The ID is further used to get information related to bookmark. 
            	 */
            		var result = contentData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){		
                		console.log("roadblock 3");
                		if(bookmarkData.filterBy("docId").length != 0){
                			console.log("inside bookmark");
                			contentData.set("bookmark.bookmarkName",bookmarkData.getEach("bookmarkName")[0]);
                    		contentData.set("bookmark.bookmarkState",true);
                    		contentData.set("bookmark.bookmarkId",bookmarkData.getEach("id")[0]);
                    		contentData.set("bookmark.reportId",bookmarkData.getEach("docId")[0]);  
                		}  
                		return contentData;
                	});     
            		return result; 
            }),
           
            similarTilesResult: this.store.find('Content', params.content_id).then(function(contentData){
            	var similarTilesQuery = {
            			"type": "newSimilarTilesURL",
            			"query":{
            				"system": contentData.get('system'),
            				"department": contentData.get('department'),
            				"subjectArea": contentData.get('subjectArea'),
            			    "path": contentData.get('path'),
            			    "name": contentData.get('reportName'),
            			    "presentationColumn": contentData.get('presentationColumn') == undefined ? "[]" :contentData.get('presentationColumn')
            			}
            	};
            	
            	var similarTilesData = contentData.store.findQuery('similarityReport', similarTilesQuery).then(function(tilesData){
            		var tilesArray = tilesData.content;            		
            		var queryString = {
                    	    "type": "bookmarkURL",
                    	    "query": {
                    	    	"from":0,
                    	    	"size":200,
                    	        "query": {
                    	            "bool": {
                    	                "must": [{
                    	                    "query_string": {
                    	                        "default_field": "bookmark.docId",
                    	                        "query": tilesData.getEach("id").toString().replace(/,/g," or ")
                    	                    }
                    	                }, {
                    	                    "query_string": {
                    	                        "default_field": "bookmark.userId",
                    	                        "query": self.session.get("email")
                    	                    }
                    	                }]
                    	            }
                    	        }
                    	    }
                    	};
            		var similarTilesResult = tilesData.store.findQuery('Bookmarking', queryString).then(function(similarBookmarkData){	
                		console.log(similarBookmarkData);
                		console.log("roadblock 4");
                		
                		var reportListArray = similarBookmarkData.getEach("docId");
                		for(var responseIndex=0; responseIndex < reportListArray.length ; responseIndex += 1){
                    		var reportData = tilesData.filterBy("id",reportListArray[responseIndex]);
                    		var temp = similarBookmarkData.filterBy("docId",reportListArray[responseIndex]);
                    		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
                    		reportData.setEach("bookmark.bookmarkState",true);
                    		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
                    		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);                    		
                    	}     
                		return tilesData;
                	});
            		return similarTilesResult;
            		
            	});
            	return similarTilesData;
            }),
            
            comments : this.store.findQuery("comments",commentsQuery).then(function(data){
            	console.log("comments Data :" + data);
            	return data;
            })
       
        })
        return completeData;
    },

});