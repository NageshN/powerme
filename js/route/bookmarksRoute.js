/**
 * Results Route starts from here
 */
PowerMe.BookmarksRoute = Ember.Route.extend({
    model: function(params) {
        // params contains the query string for search.Business Owner: "Scott"Business category: "Supply Chain"
 
       // PowerMe.userName=this.controllerFor('navigation').get("searchQuery");
        var queryData = {
        	    "type": "bookmarkURL",        	    
        	    "query": {
        	    	"from": 0,
           	        "size": 100,
        	        "query" : {
        	            "query_string" : {
        	    "default_field":"bookmark.userId",
        	    "query":this.session.get("email")
        	    }
        	        }
        	    }
        	};
        /**
         * All the bokmark information related to particular user is retrieve here 
         */
        var data = this.store.findQuery('Bookmark', queryData).then(function(bookmarkData) {
        	bookmarkData.totalFiles = bookmarkData.get("firstObject") ? bookmarkData.get("firstObject")._data.totoalFile : 0;
        	var relatedReportId=bookmarkData.getEach("docId").toString().replace(/,/g," or ");
        	var queryData={
        			"type": "searchResultURL",
            	    "query":  {
            	    	"from": 0,
               	        "size": 100,
            	        "query": {
            	            "query_string": {
            	                "default_field": "_id",
            	                "query": relatedReportId
            	            }
            	        }
            	    }
        	};
        	//PowerMe.bookmarkData= bookmarkData;
        	/**
        	 * with the help of bookmarkData all the related report ID is retrieved.
        	 * This ID is further used to retrieve each related report. 
        	 */
        	var result = bookmarkData.store.findQuery("Result",queryData).then(function(data){
        		var reportList = bookmarkData.getEach("docId");
            	for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
            		//var normalizedSource = normalizeData(responseData[responseIndex]._source);
            		var reportData = data.filterBy("id",reportList[responseIndex]);
            		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
            		temp.setEach("extension",reportData.getEach("extension")[0]);
            		temp.setEach("reportName",reportData.getEach("reportName")[0]);            		
            		temp.setEach("department",reportData.getEach("department")[0]);
            		temp.setEach("path",reportData.getEach("path")[0]);
            		temp.setEach("owner",reportData.getEach("owner")[0]);
            		temp.setEach("system",reportData.getEach("system")[0]);
                    temp.setEach("size",reportData.getEach("size")[0]);
                    temp.setEach("usageCount",reportData.getEach("usageCount")[0]);
            		temp.setEach("dayLastAccessed",reportData.getEach("dayLastAccessed")[0]?reportData.getEach("dayLastAccessed")[0]:"");
            		temp.setEach("monthLastAccessed",reportData.getEach("monthLastAccessed")[0]?reportData.getEach("monthLastAccessed")[0]:"-");
            		temp.setEach("yearLastAccessed",reportData.getEach("yearLastAccessed")[0]?reportData.getEach("yearLastAccessed")[0]:"");
            		temp.setEach("subjectArea",reportData.getEach("subjectArea")[0]);
            	}
        		return bookmarkData;
        		
        	});
        	/*return bookmarkData;*/
        	/*var queryURL = PowerMe.Constants.serviceUrl.baseURL + "/doc/_search";
        	var query = JSON.stringify(queryData.query);*/
        	 /*Ember.$.post(queryURL,query).then(function(response) {
                if (response.error) {
                	alert("something is wrong");
                }else{
                	var responseData=response.hits.hits;
                		
                	
                	for(var responseIndex=0; responseIndex < responseData.length ; responseIndex += 1){
                		var normalizedSource = normalizeData(responseData[responseIndex]._source);
                		var temp = bookmarkData.filterBy("docId",responseData[responseIndex]._id)
                		temp.setEach("description",normalizedSource.description);
                		temp.setEach("businessOwner",normalizedSource.businessOwner);
                		temp.setEach("businessCategory",normalizedSource.businessCategory);
                		
                		bookmarkData.filterBy("docId",responseData[responseIndex]._id).setProperties({
                			"description":normalizedSource.description,
                			"businessOwner":normalizedSource.businessOwner
                		});
                		console.log("pause");
                	}
                	
                	
                	
                	console.log("success");
                	console.log(bookmarkData);
                	//data.set("bookmark.bookmarkState",true);
            		//self.sendAction('bookmark',data);
                }
            },function(error){
            	alert("something is wrong");
            	console.log(error);
            });*/
        	
        	
        	
    	/*    params = {
            url: 'http://54.191.26.201:9200/powerme/doc/_search',
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(queryData.query),
            success : function(response) {
    	       console.log("success");
    	      },
    	      error : function() {
    	    	  console.log("failed");
    	      }
    	    };
    	  	var result = Ember.$.ajax(params);*/
    	  	return result;
        });
        return data;
    }
}) 

