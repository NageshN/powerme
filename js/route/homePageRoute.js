/**
 * This will render homePage template in the outlet of navigation/index template.
 */
PowerMe.HomePageRoute = Ember.Route.extend({
    renderTemplate: function() {
        this.render('homePage', { // the template to render
            into: 'navigation/index',
            outlet: 'landingPage'
        });
    }
});