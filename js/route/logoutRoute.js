PowerMe.LogoutRoute = Ember.Route.extend({

activate: function(){
		this.logout();
	},

	logout: function(){

    	var self=this,
    	sessionId = this.get('session.sessionId'),
        userId = this.get('session.userId'),
        params = {
            url: PowerMe.Constants.serviceUrl.logoutURL + userId,
            type: "POST", //or GET
            contentType: "application/json",
            data: JSON.stringify({ "sessionId": sessionId}),
            success : function(response) {
                Ember.Logger.log('signout xhr ',response);                      
                self.finishLogout();
            },
            error : function() {
                Ember.Logger.log('signout failed');  
                self.finishLogout();
            }
        };
    	Ember.$.ajax(params);
	},

	finishLogout: function(){
        //invalidate the session
        if(this.get('session')){
        	this.session.set('sessionId', null);
			this.session.set('userId', null);
			this.session.set('firstName', null);
			this.session.set('lastName', null);
			this.session.set('email', null);
        }
        function setCookie(cname, cvalue, exdays) {
		    var d = new Date();
		    d.setTime(d.getTime() + (exdays*24*60*60*1000));
		    var expires = "expires="+d.toUTCString();
		    document.cookie = cname + "=" + cvalue + "; " + expires;
		}
		setCookie("sessionId","",0);
		setCookie("userId","",0);
		setCookie("firstName","",0);
		setCookie("lastName","",0);
		setCookie("email","",0);
		this.transitionTo("loginPage");
        //call container's logout api which will send them to signin
        /*if(this.flow){
            this.flow.logout('access.signin');
        }*/
	}
	
})