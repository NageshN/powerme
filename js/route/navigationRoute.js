/**
 * Navigation Route starts from here
 */
 var system;
 var screenHeight,paddingTop,paddingBottom;

PowerMe.NavigationRoute = PowerMe.SecuredRoute.extend({
		
		
		init : function(){
				system = 'system';
			 screenHeight = screen.height;
			paddingTop = Math.round(screenHeight*(1.3/100))+'px';
			paddingBottom = Math.round(screenHeight*(1.4/100))+'px';
			
		},
	actions: {
		
    	 changeDashboardMap : function(system){
    		 
    		 	
    		 
    		if(system == 'all'){
    			if($('#hidden-sys-all').val() == '0'){
					$('#hidden-sys-obiee').val('0');
					$('#label-sys-OBIEE').removeClass('opacity1');
					$('#hidden-sys-tableau').val('0');
					$('#label-sys-TABLEAU').removeClass('opacity1');
        			$('#hidden-sys-all').val('1');
        			// alert($('#hidden-sys-all').val());
        			$('#label-sys-'+system).addClass('opacity1');	
                    $("#sys-all").attr("checked",true)
        			var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": 'system'
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                            
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
					        	"type": "top5UsersDashboard",
					        	"query": {
					        	    "size": 0,
					        	    "aggs": {
					        	        "users": {
					        	            "terms": {
					        	                "field": "USER_NAME",
					        	                "size": 5
					        	            }
					        	        }
					        	    }
					        	}
					        };
					
        		}
        		else{
        			$('#hidden-sys-all').val('0');
        			$('#label-sys-'+system).removeClass('opacity1');
        			$("#sys-all").attr("checked",false)
        		}
    			
    			
    		}
    		if(system == 'OBIEE'){
    			if($('#hidden-sys-obiee').val() == '0'){
        			$('#hidden-sys-obiee').val('1');
        			$('#hidden-sys-all').val('0');
        			// alert($('#hidden-sys-all').val());
        			$('#label-sys-OBIEE').addClass('opacity1');
					$('#label-sys-all').removeClass('opacity1');
                    $("#sys-obiee").attr("checked",true)
        			var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			    "query": {
    			       "match": {
    			       "system": system
    			       }
    			       },
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": "system"
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                            "query": {
                                "match": {
                                    "system": system
                                }
                            },
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
						         "type": "top5UsersDashboard",
						         "query": {
						             "size": 0,
						    "query": {
						              "match": {
						              "system": system
						              }
						              },
						             "aggs": {
						                 "users": {
						                     "terms": {
						                         "field": "USER_NAME",
						                         "size": 5
						                     }
						                 }
						             }
						         }
						        };
					
				 
        		}
        		
        		else{
        			$('#hidden-sys-obiee').val('0');
        			$('#label-sys-OBIEE').removeClass('opacity1');
        			$("#sys-obiee").attr("checked",false)
        		}
    		}
    		if(system == 'TABLEAU'){
    			if($('#hidden-sys-tableau').val() == '0'){
        			$('#hidden-sys-tableau').val('1');
        			$('#hidden-sys-all').val('0');
        			// alert($('#hidden-sys-all').val());
        			$('#label-sys-TABLEAU').addClass('opacity1');
					$('#label-sys-all').removeClass('opacity1');
                    $("#sys-tableau").attr("checked",true)
        			var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			    "query": {
    			       "match": {
    			       "system": system
    			       }
    			       },
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": "system"
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                            "query": {
                                "match": {
                                    "system": system
                                }
                            },
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
						         "type": "top5UsersDashboard",
						         "query": {
						             "size": 0,
						    "query": {
						              "match": {
						              "system": system
						              }
						              },
						             "aggs": {
						                 "users": {
						                     "terms": {
						                         "field": "USER_NAME",
						                         "size": 5
						                     }
						                 }
						             }
						         }
						        };
        		}
        		
        		else{
        			$('#hidden-sys-tableau').val('0');
        			$('#label-sys-TABLEAU').removeClass('opacity1');
                    $("#sys-tableau").attr("checked",false)     			
        		}
    		}
    		
    		
    		
    		
    		
				if($('#hidden-sys-tableau').val() == '1' && $('#hidden-sys-obiee').val() == '1'){
					var tilesQuery = {
				            "type": "dasboardTilesURL",
				            "query":  {
				                "size": 0,
				                "aggs": {
				                    "systems": {
				                        "cardinality": {
				                            "field": 'system'
				                        }
				                    },
				                    "totalNoOfFiles": {
				                        "value_count": {
				                            "field": "path"
				                        }
				                    },
				                    "subjectArea": {
				                        "cardinality": {
				                            "field": "subjectArea1"
				                        }
				                    },
				                    "dashboard": {
				                        "cardinality": {
				                            "field": "dashBoard1"
				                        }
				                    }
				                }
				            }

				        };
					var top5ReportsQuery = {
			        		"type": "top5ReportsDashboard",
			        		"query": {
			        		    "size": 5,
			        		   
			        		     "_source": [
			        		        "reportName",
			        		        "system",
			        		        "subjectArea",
			        		        "usageCount",
			        		        "owner",
			        		        "department"
			        		    ],
			        		    "sort": {
			        		        "usageCount": {
			        		            "order": "desc",
			        		            "mode": "max",
			        		            "missing": "_last"
			        		        }
			        		    }
			        		}
			        };
					var top5UsersQuery = {
				        	"type": "top5UsersDashboard",
				        	"query": {
				        	    "size": 0,
				        	    "aggs": {
				        	        "users": {
				        	            "terms": {
				        	                "field": "USER_NAME",
				        	                "size": 5
				        	            }
				        	        }
				        	    }
				        	}
				        };
				}
				if($('#hidden-sys-tableau').val() == '1' && $('#hidden-sys-obiee').val() == '0'){
					$('#hidden-sys-all').val('0');
					$('#label-sys-all').removeClass('opacity1');
					var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			    "query": {
    			       "match": {
    			       "system": 'TABLEAU'
    			       }
    			       },
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": "system"
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                            "query": {
                                "match": {
                                    "system": 'TABLEAU'
                                }
                            },
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
						         "type": "top5UsersDashboard",
						         "query": {
						             "size": 0,
						    "query": {
						              "match": {
						              "system": 'TABLEAU'
						              }
						              },
						             "aggs": {
						                 "users": {
						                     "terms": {
						                         "field": "USER_NAME",
						                         "size": 5
						                     }
						                 }
						             }
						         }
						        };
				}
				if($('#hidden-sys-obiee').val() == '1' && $('#hidden-sys-tableau').val() == '0'){
					$('#hidden-sys-all').val('0');
					$('#label-sys-all').removeClass('opacity1');
					var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			    "query": {
    			       "match": {
    			       "system": 'OBIEE'
    			       }
    			       },
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": "system"
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                            "query": {
                                "match": {
                                    "system": 'OBIEE'
                                }
                            },
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
						         "type": "top5UsersDashboard",
						         "query": {
						             "size": 0,
						    "query": {
						              "match": {
						              "system": 'OBIEE'
						              }
						              },
						             "aggs": {
						                 "users": {
						                     "terms": {
						                         "field": "USER_NAME",
						                         "size": 5
						                     }
						                 }
						             }
						         }
						        };
				}
				if($('#hidden-sys-all').val() == '1' && $('#hidden-sys-tableau').val() == '0' && $('#hidden-sys-obiee').val() == '0'){
					var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": 'system'
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                           
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
						var top5UsersQuery = {
					        	"type": "top5UsersDashboard",
					        	"query": {
					        	    "size": 0,
					        	    "aggs": {
					        	        "users": {
					        	            "terms": {
					        	                "field": "USER_NAME",
					        	                "size": 5
					        	            }
					        	        }
					        	    }
					        	}
					        };
				}
				if($('#hidden-sys-all').val() == '0' && $('#hidden-sys-tableau').val() == '0' && $('#hidden-sys-obiee').val() == '0'){
					var tilesQuery = {
    			            "type": "dasboardTilesURL",
    			            "query":  {
    			                "size": 0,
    			                "aggs": {
    			                    "systems": {
    			                        "cardinality": {
    			                            "field": 'system'
    			                        }
    			                    },
    			                    "totalNoOfFiles": {
    			                        "value_count": {
    			                            "field": "path"
    			                        }
    			                    },
    			                    "subjectArea": {
    			                        "cardinality": {
    			                            "field": "subjectArea1"
    			                        }
    			                    },
    			                    "dashboard": {
    			                        "cardinality": {
    			                            "field": "dashBoard1"
    			                        }
    			                    }
    			                }
    			            }

    			        };
					var top5UsersQuery = {
				        	"type": "top5UsersDashboard",
				        	"query": {
				        	    "size": 0,
				        	    "aggs": {
				        	        "users": {
				        	            "terms": {
				        	                "field": "USER_NAME",
				        	                "size": 5
				        	            }
				        	        }
				        	    }
				        	}
				        };
						var top5ReportsQuery = {
                        "type": "top5ReportsDashboard",
                        "query": {
                            "size": 5,
                           
                            "_source": [
                                "reportName",
                                "system",
                                "subjectArea",
                                "usageCount",
                                "owner",
                                "department"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            }
                        }
                    }; 
				}
				
				
			barTop5ReportsData: this.store.find("Top5Reporting", top5ReportsQuery).then(function(chartData) {
				
                function createChartArrays(dataHolder, ticksHolder, reportName, usageCount) {
                    var length = reportName.length,
                        count = 0;
                    for (count; count < length; count++) {
                        //dataHolder.push([usageCount[count],count]);
                        //ticksHolder.push([count,reportName[count]]);
                        ticksHolder.push([reportName[count], count]);
                        dataHolder.push([count, usageCount[count]]);
                    }
                }

                var chartDataArray = [],
                    chartTicksArray = [],

                    reportName = chartData.getEach('reportName'),
                    usageCount = chartData.getEach('usageCount');
               // console.log('reportname ' + reportName);
                //console.log('count '+usageCount);
                //reportName = reportName.reverse();
                //usageCount = usageCount.reverse();
                createChartArrays(chartDataArray, chartTicksArray, reportName, usageCount);

                // console.log('top5dataBefore '+chartDataArray);
              
                //console.log('top5dataNew ' + chartDataArray);
                //console.log('top5ticksNew ' + chartTicksArray);

                var dataForChart = {

                    /*chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray*/
                    chartDataArray: chartDataArray,
                    chartTicksArray: chartTicksArray
                }
                
               
                var dataSet = [];
                var colors = ["#e87352","#60cd9b", "#66b5d7", "#eec95a","#8876d6"];
                //console.log('asd '+chartDataArray);
                for(var j=0;j<chartDataArray.length;j++){
                	console.log('in a for loop '+chartDataArray[j]);
                	dataSet.push({
                		"color" : colors[j],
                		"data" : [chartDataArray[j]]
                	});
                }
                var chartXLabel="Report Name";
                var chartYLabel="Usage Count";
                var placeholder = 'topDashboard';
                var previousPoint = null,
                previousLabel = null;
            
            var clickedSystemForNumberofFiles;
                var options = createOptionsArray(chartTicksArray, chartXLabel, chartYLabel);
                renderChartandTooltip(placeholder, dataSet, options);
                
                function createOptionsArray(chartTicksArray, chartXLabel, chartYLabel) {
                    var options = {
                    		
                    		seriesDefaults: {

                    		    rendererOptions:
                    		    {
                    		    	 highlightMouseOver: false,
                    		         highlightMouseDown: false,
                    		         highlightColor: null,
                    		    }
                    		},
                    		
                    	series: {
                            bars: {
                                show: true
                            },
                            
                        },
                       
                        bars: {
                            align: "center",
                            barWidth: .5,
                            horizontal: false,
                            fillColor: {
                                colors: [{
                                    opacity: 0.5
                                }, {
                                    opacity: 1
                                }]
                            },
                            lineWidth: .5
                     
                        },
                        axesDefaults: {
                        	ticks: chartDataArray
                        },
                        xaxis: {
                        	ticks: chartTicksArray,
                        	axisLabel: chartXLabel,
                        	axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 10,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 20,
                            tickColor: "#CCC",
                        	//renderer: $.jqplot.CategoryAxisRenderer,
                        	
                        },
                        yaxis: {
                        	//ticks: chartDataArray,
                        	axisLabel: chartYLabel,
                        	 axisLabelUseCanvas: true,
                             axisLabelFontSizePixels: 10,
                             axisLabelFontFamily: 'Verdana, Arial',
                             axisLabelPadding: 20,
                             tickColor: "#CCC",
                            // axisLabel: chartYLabel,
                        	

                        },
                        legend: {
                            noColumns: 0,
                            labelBoxBorderColor: "#858585",
                            position: "ne"
                        },
                        grid: {
                            hoverable: true,
                            borderWidth: 0.1,
                            clickable: true,
                            
                        }
                    };

                    return options;
                }
            });
				 function renderChartandTooltip(placeholder, dataSet, options) {
			            $.plot($("#" + placeholder), dataSet, options);
			            setTitleAttribute.apply($("#" + placeholder));
			            useTooltip.apply($("#" + placeholder));
			        }
				 function setTitleAttribute() {
			            var dataArrayLength = $('.yAxis').children('.tickLabel').length;
			            for (var t = 0; t < dataArrayLength; t++) {
			                var content = $('.yAxis .tickLabel')[t].innerHTML;
			                $('.yAxis .tickLabel')[t].setAttribute('title', content);
			                //$('.yAxis > .tickLabel')[t].innerHTML = $('.yAxis > .tickLabel')[t].innerHTML.slice(0,4);
			            }
			        }
				 function showTooltip(x, y, contents) {
			            $('<div id="tooltip">' + contents + '</div>').css({
			                position: 'absolute',
			                display: 'none',
			                top: y - 10,
			                left: x + 10,
			                padding: '6px',
			                border: '1px solid #666',
			                'font-size': '12px',
			                'border-radius': '5px',
			                'background-color': '#fff',
			                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
			            }).appendTo("body").fadeIn(200);
			        }
				 var previousPoint = null,
		            previousLabel = null;

		        function useTooltip() {
		            $(this).bind("plothover", function(event, pos, item) {
		            	//console.log(item);
		                if (item) {
		                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
		                        previousPoint = item.dataIndex;
		                        previousLabel = item.series.label;
		                        $("#tooltip").remove();
		                        //console.log(item.datapoint);
		                       // console.log(item.datapoint[0]);
		                        var x = item.datapoint[1],
		                            y = item.datapoint[0];
		                        var color = item.series.color;
		                       // console.log(y);
		                        showTooltip(pos.pageX - 10, pos.pageY + 30, "</strong>" + item.series.xaxis.options.ticks[y][0] + ":<strong>&nbsp;" + x + "</strong>");
		                    } else {
		                        $("#tooltip").css("top", pos.pageY + 20);
		                        $("#tooltip").css("left", pos.pageX);
		                    }
		                } else {
		                    $("#tooltip").remove();
		                    previousPoint = null;
		                }
		            });
		        };
				tilesData: this.store.findQuery("tile", tilesQuery);
		        
		        //third fraph updating 
		        pieFileTop5UsersData: this.store.findQuery("Top5UsingPanel", top5UsersQuery).then(function(data){            	
	            	function renderdataForChart(dataHolder, systemCount, usageCount) {
	                    var length = systemCount.length,
	                        count = 0;
	                    for (count; count < length; count += 1) {
	                    	
	                    	dataHolder.push({"label" :systemCount[count].toUpperCase(), "data":usageCount[count]});
	                    }
	                    
	                }
	                
	                var fileUsageNSystem = [],
	                systemCount = data.getEach('key'),
	                usageCount = data.getEach('doc_count');
	               console.log('systemCountNagesh '+systemCount);
	               console.log('usageCountNagesh '+usageCount);
	                var length = systemCount.length;
	                
	                renderdataForChart(fileUsageNSystem, systemCount, usageCount );
	                console.log('fileUsageNSystem '+fileUsageNSystem);
	                var dataForChart = {
	                		fileUsageNSystem: fileUsageNSystem
	                }    
	                
	                var pieChartId = "filesUsageSystemPie";
	                var chartTitleId = "fileUsageNSystemTitle";
	                var chartTitle = "Top Users";
	                var previousPoint = null,
	                    previousLabel = null;
	                
	                var clickedSystemForNumberofFiles;
	                var data = fileUsageNSystem;
	                renderPieChart(pieChartId, data);
	               // console.log(data);
	                function renderPieChart(pieChartId, data){
	                	 $.plot("#" + pieChartId, data, {
	                		 series: {
	                             pie: {
	                                 show: true,
	                                 offset: {
	                                	 'top': 1
	                                 }
	                             },
	                             
	                         },
	                         legend: {
	        			         show: true,
	        			         rendererOptions: {
	        			                numberRows: 1
	        			            },
	        			            
	        			         location: 'w'
	        			     },
	        			     
	                         grid: {
	                             hoverable: true,
	                             clickable: false
	                         },
	                         colors: ["#e87352","#60cd9b", "#66b5d7", "#eec95a","#8876d6"]
	                     });
	                	 
	                	 $('#' + chartTitleId).html(chartTitle); 
	                	 showMemo.apply($("#" + pieChartId));
	                }
	                function showTooltipPie(x, y, color, contents) {
	                    $('<div id="tooltip">' + contents + '</div>').css({
	                        position: 'absolute',
	                        display: 'none',
	                        top: y - 10,
	                        left: x + 10,
	                        padding: '10px',
	                        border: '1px solid #666',
	                        'font-size': '12px',
	                        'border-radius': '5px',
	                        'background-color': '#fff',
	                        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
	                        opacity: 0.9
	                    }).appendTo("body").fadeIn(200);
	                }
	                function showMemo() {
	                    $(this).bind("plothover", function(event, pos, item) {
	                    	console.log(item);
	                        if (item) {
	                            if ((previousLabel != item.series.label) ||
	                                (previousPoint != item.dataIndex)) {
	                                previousPoint = item.dataIndex;
	                                previousLabel = item.series.label;
	                                $("#tooltip").remove();

	                                var x = item.datapoint[0];
	                                var y = item.datapoint[1];

	                                var color = item.series.color;            
	                               console.log(item.series.label);
	                                
	                               showTooltipPie(pos.pageX - 10,
	                                    pos.pageY + 30,color,
	                                    "</strong>" + item.series.label +
	                                    " : <strong>" + item.series.data[0][1] + "</strong>");
	                            } else {
	                                $("#tooltip").css("top", pos.pageY + 20);
	                                $("#tooltip").css("left", pos.pageX);
	                            }
	                        } else {
	                            $("#tooltip").remove();
	                            previousPoint = null;
	                        }
	                    });
	                }
	               

	              

	                
	            });
         },
        /**
         * error handling when tool doesn't get proper response
         */
        error: function (error) {
            console.log("error occurs reason for that :" + error.status + "  " + error.statusText);
            /*if(PowerMe.count < 20){
    			console.log(PowerMe.count);
    			this.transitionTo('index');
    			PowerMe.count += 1;
    		}else{
    			return true;
    		}*/


        }

        /**
         * loading action will be triggered when route takes some time to load data.
         * with the help of this we can show some spinning like image while waiting for data
         * Return true to bubble this event to application
         */

        /*loading: function(transition, originRoute) {
    	      //displayLoadingSpinner();
    	      console.log("loading");
    	      return true;
    	    }*/
    },
	
	update : function(){
		console.log('update');
	},
	
    model: function () {
		
    	console.log('sysytem ' +system);
    	//alert(system)
        var query = {
            "type": "navigationObjectURL",
            "query":  {
                "size": 0,
                "aggs": {
                    "system": {
                        "terms": {
                            "field": 'system'
                        },
                        "aggs": {
                            "rootFolder": {
                                "terms": {
                                    "field": "rootFolder",
                                    "size": 100
                                }
                            }
                        }
                    }
                }
            }
        };
        
        var obieeNavigationBarQuery = {
        	    "type": "obieeNavigationURL",
        	    "query":  {
                    "size": 0,
                    "sort":{"usageCount" : { "order": "desc","mode" : "max", "missing" : "_last"}},
                    "aggs": {
                        "system": {
                            "terms": {
                                "field": "system"
                            },
                            "aggs": {
                                "rootFolder": {
                                    "terms": {
                                        "field": "subjectArea",
                                        "size": 100
                                    }
                                }
                            }
                        }
                    }
                }
        	};
        
       /* var navigationLeftPanelQuery = {
        		"type": "navigationLeftPanelURL",
        		"query":{
        		    "size": 0,

        		    "aggs": {
        		        "system": {
        		            "terms": {
        		                "field": "system"
        		            },
        		            "aggs": {
        		                "department": {
        		                    "terms": {
        		                        "field": "department1",
        		                        "size":10000,
        		                        "order": {
        		                            "_term": "asc"
        		                        }

        		                    },
        		                    "aggs": {
        		                        "subjectArea": {
        		                            "terms": {
        		                                "field": "subjectArea1",
        		                                "size":10000,
        		                                "order": {
        		                                    "_term": "asc"
        		                                }

        		                            }
        		                        }
        		                    }
        		                }
        		            }
        		        }
        		    }
        		}
        		
        };*/
        var navigationLeftPanelQuery = {
                "type": "navigationLeftPanelURL",
                "query": {
                    "size": 0,
     
                    "aggs": {
                        "system": {
                            "terms": {
                                "field": "system"
                            },
                            "aggs": {
                                "department": {
                                    "terms": {
                                        "field": "department1",
                                        "size": 10000,
                                        "order": {
                                            "_term": "asc"
                                        }
                                    },
                                    "aggs": {
                                        "application": {
                                            "terms": {
                                                "field": "subjectArea1",
                                                "size": 10000,
                                                "order": {
                                                    "_term": "asc"
                                                }
                                            },
                                            "aggs": {
                                                "dashBoard": {
                                                    "terms": {
                                                        "field": "dashBoard1",
                                                        "size": 10000,
                                                        "order": {
                                                            "_term": "asc"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
     
            };
        
        var obieeSubjectTileQuery = {
        		"type": "obieeSubjectTileURL",
        		"query": {
                    "size": 0,
                    "aggs": {
                        "group_by_rootFolder": {
                            "terms": {
                                "field": "subjectArea",
                                "size": 15
                            }
                        }
                    }
                }  		       		
        };
        
        var obieeReportTileQuery = {
        		"type": "obieeReportTileURL",
        		"query":  {
        	        "from": 0,
        	        "size": 200,
        	        "query": {
        	            "query_string": {
        	               "fields" : ["subjectArea1"],
        	                "query": "*"
        	            }
        	        }
        	    }   		
        };
        
        var tilesQuery = {
            "type": "dasboardTilesURL",
            "query":  {
                "size": 0,
                "aggs": {
                    "systems": {
                        "cardinality": {
                            "field": "system"
                        }
                    },
                    "totalNoOfFiles": {
                        "value_count": {
                            "field": "path"
                        }
                    },
                    "subjectArea": {
                        "cardinality": {
                            "field": "subjectArea1"
                        }
                    },
                    "dashboard": {
                        "cardinality": {
                            "field": "dashBoard1"
                        }
                    }
                }
            }

        };
        var chartFilesnLocationQuery = {
            "type": "filenLocationURL",
            "query": {
                "size": 0,
                "aggs": {
                    "group_by_rootFolder": {
                        "terms": {
                            "field": "rootFolder",
                            "size": 10
                        }
                    }
                }
            }
        };
        
        var chartSizenLocationQuery = {
        		"type": "sizenLocationURL",
        		"query": {
        		    "size": 0,
        		    "aggs": {
        		        "group_by_rootFolder": {
        		            "terms": {
        		                "field": "rootFolder",
        		                "order": {
        		                    "totalSize": "desc"
        		                },
        		                "size": 10
        		            },
        		            "aggs": {
        		                "totalSize": {
        		                    "sum": {
        		                        "field": "sizeInKB"
        		                    }
        		                }
        		            }
        		        }
        		    }
        		}		
        };
        var chartFileNFileTypeQuery = {
        	    "type": "fileNFileTypeURL",
        	    "query": {
        	        "size": 0,
        	        "query": {
        	            "term": {
        	                "rootFolder": "rvbd-operations-private"
        	            }
        	        },

        	        "aggs": {
        	            "group_by_rootFolder": {
        	                "terms": {
        	                    "field": "fileExtension",
        	                    "size": 20
        	                }
        	            }
        	        }
        	    }
        	};
        var chartFilesNLastAccessedQuery = {
        	    "type": "fileNLastAccessedURL",
        	    "query": {
        	        "size": 0,
        	        "aggs": {
        	            "lastAccessed": {
        	                "terms": {
        	                    "field": "yearLastAccessed",
        	                    "order": {
        	                        "_count": "desc"
        	                    },
        	                    "size": 10
        	                }
        	            }
        	        }
        	    }

        	};
        
        var chartFileNOwnerQuery = {
        		"type": "fileNOwnerURL",
        		"query":  {
        		    "size": 0,
        		    "query": {
        		        "bool" : {
        		        "must_not" : [{"term" : { "owner" :"null/null" }},
        		                             {"term" : { "owner" :"null" }},
        		                             {"term" : { "owner" :"" }}
        		        ]
        		    }
        		    },
        		    "aggs": {
        		        "owners": {
        		            "terms": {
        		                "field": "owner",
        		                "size": 10
        		            }
        		        }
        		    }
        		}       		
        };
        
        var chartSizeNOwnerQuery = {
        		"type": "sizeNOwnerURL",
        		"query": {
        		    "size": 0,
        		    "query": {
        		    "bool" : {
        		            "must_not" : [{"term" : { "owner" :"null/null" }},
        		                                 {"term" : { "owner" :"null" }},
        		                                 {"term" : { "owner" :"" }}
        		            ]
        		        }
        		    },
        		        "aggs": {
        		            "owner": {
        		                "terms": {
        		                    "field": "owner",
        		                    "order": {
        		                        "size": "desc"
        		                    },
        		                    "size": 10
        		                },
        		                "aggs": {
        		                    "size": {
        		                        "sum": {
        		                            "field": "sizeInKB"
        		                        }
        		                    }
        		                }
        		            }
        		        }
        		    }
  		
        }
        
        var dataForTagCloud = {
        		"type": "tagCloud",
        		"query": {
					"size": 0,
					"aggs": {
						"fileExtension": {
							"terms": {
								"field": "fileExtension",
								"size": 50
							}
						}
					}
				}
  		
        }
        
        var top5ReportsQuery = {
        		"type": "top5ReportsDashboard",
        		"query": {
        		    "size": 5,
        		   
        		        
        		       
        		    "_source": [
        		        "reportName",
        		        "system",
        		        "subjectArea",
        		        "usageCount",
        		        "owner",
        		        "department"
        		    ],
        		    "sort": {
        		        "usageCount": {
        		            "order": "desc",
        		            "mode": "max",
        		            "missing": "_last"
        		        }
        		    }
        		}
        };
        
        var top5UsersQuery = {
        	"type": "top5UsersDashboard",
        	"query": {
        	    "size": 0,
        	    "aggs": {
        	        "users": {
        	            "terms": {
        	                "field": "USER_NAME",
        	                "size": 5
        	            }
        	        }
        	    }
        	}
        };
        
        var pieFileUsageVsSystemQuery = {
        	"type": "fileUsageNSystemURL",
        	"query": {
        	    "size": 0,
        	    "query": {
        	        "match_all": {}
        	    },
        	    "aggs": {
        	        "systems": {
        	            "terms": {
        	                "field": "system"
        	            },
        	            "aggs": {
        	                "usageCount": {
        	                    "sum": {
        	                        "field": "usageCount"
        	                    }
        	                }
        	            }
        	        }
        	    }
        	}
        };
       
        
        var pieFilesVsSystemQuery ={
        	"type": "fileNumbersNSystemURL",
        	"query":{
        	    "size": 0,
        	    "aggs": {
        	        "systems": {
        	            "terms": {
        	                "field": "system"
        	            }
        	        }
        	    }
        	}
        };
        
        var barFilesAccessedVsMonthQuery ={
            	"type": "filesAccessedNMonthURL",
            	"query": {
            		"sort":{"count": {"order": "desc"}}
                },
				
            };
        
        var completeData = Ember.RSVP.hash({
            data: this.store.findQuery("navigation", query).then(function (data) {
            	var topObj = [];
            	var systems = data.getEach("key");
            	for (var systemCount=0 ; systemCount < systems.length ; systemCount += 1){
            		topObj.push({
                        'system': systems[systemCount],
            			'rootFolder': data.filterBy("key", systems[systemCount]).getEach("rootFolder")[0].buckets
                    });
            	}
            	return topObj;
            }),
            
            top5ReportsData: this.store.findQuery("Top5Reporting", top5ReportsQuery).then(function(data){
            	var topObj = [];
            	var systems = data.getEach('system');
            	var subjectArea = data.getEach('subjectArea');
            	var owner = data.getEach('owner');
            	var reportName = data.getEach('reportName');
            	var department = data.getEach('department');
            	var usageCount = data.getEach('usageCount');
            	var docId = data.getEach('id');
            	for(var count=0; count< systems.length; count++){
            		topObj.push({
                        'system': systems[count].toUpperCase(),
            			'department': department[count],
            			'subjectArea': subjectArea[count],
            			'reportName': reportName[count],
            			'owner': owner[count],
            			'usageCount': usageCount[count],
            			'docId': docId[count]
                    });
            	}
            	return topObj;
            }),
            
            barTop5ReportsData: this.store.find("Top5Reporting", top5ReportsQuery).then(function(chartData){
           	 function createChartArrays(dataHolder, ticksHolder, reportName, usageCount,customTicksArray) {
                    var length = reportName.length,
                        count = 0;
                    for (count; count < length; count++) {
                       //dataHolder.push([usageCount[count],count]);
                        //ticksHolder.push([count,reportName[count]]);
                    	ticksHolder.push([reportName[count],count]);
                    	dataHolder.push([count,usageCount[count]]);
                    	customTicksArray = [[0,"1st "],[1,'sdwwr']]; 
                    	//customTicks.push([count,reportName[count]]);
                    }
                    
                }
           	
                var chartDataArray = [],
                chartTicksArray = [],
                customTicks = [],
                reportName = chartData.getEach('reportName'),
                usageCount = chartData.getEach('usageCount');
                console.log('reportname '+reportName);
                //console.log('count '+usageCount);
                //reportName = reportName.reverse();
                //usageCount = usageCount.reverse();
                createChartArrays(chartDataArray, chartTicksArray, reportName , usageCount,customTicks);
                
               // console.log('top5dataBefore '+chartDataArray);
               
                console.log('top5dataNew '+chartDataArray);
                console.log('top5ticksNew '+chartTicksArray);
                console.log('top5CustomticksNew '+customTicks);
                
                var dataForChart = {
                		
                		/*chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray*/
                		chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray,
                		customTicks :customTicks
                }
                
                return dataForChart;
                                 
           }),
           
            top5UsersData: this.store.findQuery("Top5UsingPanel", top5UsersQuery).then(function(data){
            	var topObj = [];
            	var userNames = data.getEach("key");
            	var usageValue = data.getEach('doc_count');
            	for (var userCount=0 ; userCount < userNames.length ; userCount += 1){
            		topObj.push({
                        'key': userNames[userCount],
            			'doc_count': usageValue[userCount]
                    });
            	}
            	return topObj;
            }),
            
            pieFileTop5UsersData: this.store.findQuery("Top5UsingPanel", top5UsersQuery).then(function(data){            	
            	function renderdataForChart(dataHolder, systemCount, usageCount) {
                    var length = systemCount.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	
                    	dataHolder.push({"label" :systemCount[count].toUpperCase(), "data":usageCount[count]});
                    }
                    
                }
                
                var fileUsageNSystem = [],
                systemCount = data.getEach('key'),
                usageCount = data.getEach('doc_count');
                //console.log('systemCountNagesh '+systemCount);
                
                var length = systemCount.length;
                
                renderdataForChart(fileUsageNSystem, systemCount, usageCount );
                console.log('fileUsageNSystem'+fileUsageNSystem);
                var dataForChart = {
                		fileUsageNSystem: fileUsageNSystem
                }    
                return dataForChart;
            }),
            barFilesAccseedVsMonthData: this.store.find("FilesAccessedVsMonth", barFilesAccessedVsMonthQuery).then(function(chartData){
            	 function createChartArrays(dataHolder, ticksHolder, month, fileAccessed,customTicks) {
                     var length = month.length,
                         count = 0;
                     for (count; count < length; count++) {
                        // dataHolder.push([fileAccessed[count], count]);
                        // ticksHolder.push([count, month[count]]);
                         ticksHolder.push([month[count], count]);
                        dataHolder.push([count, fileAccessed[count]]);
                        customTicks = [[0,"1st Nov to 3rd Nov"],[1,'sajd askd']]; 
                       //customTicks.push([count,month[count]]);
                        
                     }
                     
                 }
             	
                 var chartDataArray = [],
                 chartTicksArray = [],
                 customTicks = [],
                 month = chartData.getEach("month"),
                 fileAccessed = chartData.getEach("count");
                 //month = month.reverse();
                 //fileAccessed = fileAccessed.reverse();
                 createChartArrays(chartDataArray, chartTicksArray, month , fileAccessed,customTicks);
                 console.log('bar '+chartDataArray);
                 console.log('barticks '+chartTicksArray);
                 var dataForChart = {
                 		chartDataArray: chartDataArray,
                 		chartTicksArray: chartTicksArray
                 }
                 //console.log(' '+dataForChart);
                 return dataForChart;
                                  
            }),
            
            pieFileUsageVsSystem: this.store.findQuery("fileUsageVsSystem", pieFileUsageVsSystemQuery).then(function(data){            	
            	function renderdataForChart(dataHolder, systemCount, usageCount) {
                    var length = systemCount.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	
                    	dataHolder.push({"label" :systemCount[count].toUpperCase(), "data":usageCount[count]});
                    }
                    
                }
                
                var fileUsageNSystem = [],
                systemCount = data.getEach('key'),
                usageCount = [];
                var length = systemCount.length;
                for(var count=0; count< length; count++){
                	usageCount.push(data.content[count]._data.usageCount.value);
                }  
                
             
                
                renderdataForChart(fileUsageNSystem, systemCount, usageCount );
                //console.log('fileUsageNSystemOriginal '+fileUsageNSystem);
                var dataForChart = {
                		fileUsageNSystem: fileUsageNSystem
                }    
                
                return dataForChart;
            }),
            
            pieFileNumbersVsSystem : this.store.findQuery("fileNumberVsSystem", pieFilesVsSystemQuery).then(function(data){
            	function renderdataForChart(dataHolder, systemValue, fileNumberValue) {
                    var length = systemValue.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	dataHolder.push({"label" :systemValue[count].toUpperCase(), "data":fileNumberValue[count]});
                    }
                }
                var fileNumberNSystem = [],
                systemValue = data.getEach("key"),
                fileNumberValue = data.getEach('doc_count');
                renderdataForChart(fileNumberNSystem, systemValue , fileNumberValue);
                var dataForChart = {
                		fileNumberNSystem: fileNumberNSystem
                }
                return dataForChart;
            }),
            
            chartSizeNOwner: this.store.findQuery("SizeNOwner", chartSizeNOwnerQuery).then(function(chartData){
            	function renderdataForChart(dataHolder, ownerValue, sizeValue) {
                    var length = ownerValue.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	dataHolder.push({"label" :ownerValue[count], "data":sizeValue[count]});
                    }
                }
                var sizenOwner = [],
                ownerValue = chartData.getEach("key"),
                sizeValue = chartData.getEach("sizeValue");
                renderdataForChart(sizenOwner, ownerValue , sizeValue);
                var dataForChart = {
                		sizenOwner: sizenOwner
                }
                return dataForChart;
            }),
            
           /* navigationLeftPanelData: this.store.findQuery("navigationLeftPanel", navigationLeftPanelQuery).then(function(data){
            	var topObj = [];
            	var systems = data.getEach('key');           	           	
            	for (var systemCount=0 ; systemCount < systems.length ; systemCount += 1){
            		topObj.push({
            			'system': systems[systemCount].toUpperCase()
            		});            		
            		var depts = [];
            		var departments = data.filterBy('key', systems[systemCount]).getEach("_data.department")[0].buckets;            		
            		for(var depCount= 0; depCount < departments.length; depCount++){
            			depts.push({
            				'departmentkey': departments[depCount].key,            				
            				'systemNDepartment': (systems[systemCount]+departments[depCount].key).replace(/\s/g, '-'),
            				'subjectArea': departments.filterBy('key', departments[depCount].key).getEach("subjectArea")[0].buckets,
            			}) ;  
            			var subjectAreaLength = depts[depCount].subjectArea.length;
            			for(var count = 0; count < subjectAreaLength; count++){
            				depts[depCount].subjectArea[count].system = systems[systemCount];
            				depts[depCount].subjectArea[count].departmt = depts[depCount].departmentkey;   
            				depts[depCount].subjectArea[count].encodedSubjectArea = encodeURIComponent(depts[depCount].subjectArea[count].key)
            				depts[depCount].subjectArea[count].systemNDepartment = (systems[systemCount]+depts[depCount].departmentkey).replace(/\s/g, '-');
            			}          			
            		}           		          	
            		topObj[systemCount].department = depts;     		
            	}
            	
            	return topObj;
            }),*/
            
            navigationLeftPanelData: this.store.findQuery("navigationLeftPanel", navigationLeftPanelQuery).then(function(data) {
                var topObj = [];
                var systems = data.getEach('key');

                for (var systemCount = 0; systemCount < systems.length; systemCount += 1) {
                    topObj.push({
                        'system': systems[systemCount].toUpperCase()
                    });
                    var deptDataArray = [];
                    var depValue = data.filterBy('key', systems[systemCount]).getEach("_data.department")[0];
                    if (depValue != undefined) {
                    var departments = data.filterBy('key', systems[systemCount]).getEach("_data.department")[0].buckets;

                    for (var depCount = 0; depCount < departments.length; depCount++) {
                        console.log("departments[depCount] ");
                        deptDataArray.push({
                            'departmentkey': departments[depCount].key,
                            'systemNDepartment': (systems[systemCount] + departments[depCount].key).replace(/\s/g, '-'),
                        });

                        var appDataArray = [];
                        var appValue = departments.filterBy('key', departments[depCount].key).getEach("application")[0];

                        if (appValue != undefined) {
                            var applications = departments.filterBy('key', departments[depCount].key).getEach("application")[0].buckets;
                            for (var appCount = 0; appCount < applications.length; appCount++) {
                                
                                appDataArray.push({
                                    'system': systems[systemCount],
                                    'departmt': deptDataArray[depCount].departmentkey,
                                    'encodedSubjectArea': encodeURIComponent(applications[appCount].key),
                                    'systemNDepartment': (systems[systemCount] + deptDataArray[depCount].departmentkey).replace(/\s/g, '-'),
                                    'sysDepNApplication': (systems[systemCount] + deptDataArray[depCount].departmentkey + applications[appCount].key).replace(/\s/g, '-'),
                                    'appkey': applications[appCount].key,
                                    'doc_count': applications[appCount].doc_count
                                });

                                var dashDataArray = [];
                                var dashValue = applications.filterBy('key', applications[appCount].key).getEach('dashBoard')[0];
                                
                                console.log("dashValue "+ dashValue);
                                
                                if (dashValue != undefined) {
                                    var dashBoards = applications.filterBy('key', applications[appCount].key).getEach('dashBoard')[0].buckets;
                                    console.log("dashBoards.length "+dashBoards.length);
                                    for (var dashCount = 0; dashCount < dashBoards.length; dashCount++) {
                                    console.log("Here ");
                                        dashDataArray.push({
                                            'systm': systems[systemCount],
                                            'deprtmt': deptDataArray[depCount].departmentkey,
                                            'encodedSubjArea': encodeURIComponent(applications[appCount].key),
                                            'encodedDashboard': encodeURIComponent(dashBoards[dashCount].key),
                                            'key': dashBoards[dashCount].key,
                                            'doc_count': dashBoards[dashCount].doc_count,
                                            'systemNDepartment': (systems[systemCount] + departments[depCount].key).replace(/\s/g, '-'),
                                            'sysDepNApplication': (systems[systemCount] + deptDataArray[depCount].departmentkey + applications[appCount].key).replace(/\s/g, '-'),
                                            'sysDepAppNDashboard': (systems[systemCount] + deptDataArray[depCount].departmentkey + applications[appCount].key + dashBoards[dashCount].key).replace(/\s/g, '-')
                                        });
                                    }
                                    appDataArray[appCount].dashboard = dashDataArray;
                                }
                            }
                            deptDataArray[depCount].application = appDataArray;
                        }
                    }
                    topObj[systemCount].department = deptDataArray;
                }
            }
            console.log(topObj);
            return topObj;
        }),

            
            obieeData: this.store.findQuery("ObieeNavigation", obieeNavigationBarQuery).then(function (navigationData){
            	//console.log(navigationData);
            	var topObj = [];
            	var systems = navigationData.getEach("key");
            	for (var systemCount=0 ; systemCount < systems.length ; systemCount += 1){
            		topObj.push({
                        'system': systems[systemCount].toUpperCase(),
            			'subjectArea': navigationData.filterBy("key", systems[0]).getEach("_data.rootFolder")[0].buckets            			
                    });
            	}
            	
            	var length = topObj[0].subjectArea.length;
            	var encodedSubjectArea = [];
            	for(count=0; count< length; count++){
            		topObj[0].subjectArea[count].encodedArea = encodeURIComponent(topObj[0].subjectArea[count].key)            		
            	}   
            	        	
            	console.log(topObj);
            	return topObj;
            }),
            
            obieeSubjectTileData: this.store.findQuery("ObieeSubjectTile", obieeSubjectTileQuery).then(function (subjectTilesData){
            	var subjectValues = [];
            	var subjectAreas = subjectTilesData.getEach("key");
            	var subjectCounts = subjectTilesData.getEach("doc_count");
            	for (var systemCount=0 ; systemCount < subjectAreas.length ; systemCount += 1){
            		subjectValues.push({
                        'subjectArea': subjectAreas[systemCount],
                        'subjectCount': subjectCounts[systemCount]
                    });
            	}
            	return subjectValues;
            	
            }),
            
            obieeReportTileData: this.store.findQuery("Result",obieeReportTileQuery ).then(function (reportTileData){
            	console.log('reportTileData');
            	console.log(reportTileData);
            	//return reportTileData;

            }),
            
            tilesData: this.store.findQuery("tile", tilesQuery),
           
            chartFlotFilesnLocation: this.store.findQuery("ObieeSubjectTile", obieeSubjectTileQuery).then(function (chartData) {
                function createChartArrays(dataHolder, ticksHolder, location, fileCount) {
                    var length = location.length,
                        count = 0;
                    for (count; count < length; count++) {
                        dataHolder.push([fileCount[count], count]);
                        ticksHolder.push([count, location[count]]);
                    }
                }
            	
                var chartDataArray = [],
                chartTicksArray = [],
                location = chartData.getEach("key"),
                fileCount = chartData.getEach("doc_count");
                location.splice(10, 5);
                fileCount.splice(10, 5);
                location = location.reverse();
                fileCount = fileCount.reverse();
                createChartArrays(chartDataArray, chartTicksArray, location , fileCount);
                var dataForChart = {
                		chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray
                }
                return dataForChart;
            }),
            
            chartSizenLocation: this.store.findQuery("SizenLocation", chartSizenLocationQuery).then(function(chartData){
            	 function renderdataForChart(dataHolder, location, size) {
                    var length = location.length,
                        count = length-1;
                    for (count; count >= 0; count--) {
                        dataHolder.push([size[count], location[count]])
                    }
                }
                var sizenLocation = [],
                    location = chartData.getEach("key"),
                    size = chartData.getEach("totalSizeGB");   
                renderdataForChart(sizenLocation, location, size);
                var dataForChart = {
                    sizenLocation: sizenLocation
                }
                return dataForChart;
            }),
            
            chartFlotSizenLocation: this.store.findQuery("SizenLocation", chartSizenLocationQuery).then(function(chartData){           	 
              function createChartArrays(dataHolder, ticksHolder, location, size) {
                var length = location.length,
                count = 0;
                for (count; count < length; count++) {
                   dataHolder.push([size[count], count]);
                   ticksHolder.push([count, location[count]]);
                 }
              }
           	
               var chartDataArray = [],
               chartTicksArray = [],
               location = chartData.getEach("key"),
               size = chartData.getEach("totalSizeGB"); 
               location = location.reverse();
               size = size.reverse();
               createChartArrays(chartDataArray, chartTicksArray, location , size);
               var dataForChart = {
               		chartDataArray: chartDataArray,
               		chartTicksArray: chartTicksArray
               }
               return dataForChart;   
           }),
           
            chartFileNFileType: this.store.findQuery("FilesnFilesType", chartFileNFileTypeQuery).then(function(chartData){
            	function renderdataForChart(dataHolder, fileType, fileCountValue) {
                    var length = fileType.length,
                        count = length-1;
                    for (count; count >= 0; count--) {
                        dataHolder.push([fileCountValue[count], fileType[count]])
                    }
                }
                var filenFileType = [],
                	fileType = chartData.getEach("key"),
                    fileCountValue = chartData.getEach("countInThousand");
                renderdataForChart(filenFileType, fileType, fileCountValue);
                var dataForChart = {
                		filenFileType: filenFileType
                }
                return dataForChart;                
            }),
            
            chartFlotFilesNLastAccessed: this.store.findQuery("FilesNLastAccessed", chartFilesNLastAccessedQuery).then(function(chartData){
            	function createChartArrays(dataHolder, ticksHolder, lastAccessed, fileCount) {
                    var length = lastAccessed.length,
                        count = 0;
                    for (count; count < length; count++) {
                        dataHolder.push([fileCount[count], count]);
                        ticksHolder.push([count, lastAccessed[count]]);
                    }
                }
            	
                var chartDataArray = [],
                chartTicksArray = [],
                lastAccessed = chartData.getEach("key"),
                fileCount = chartData.getEach("countInThousand");
                lastAccessed = lastAccessed.reverse();
                fileCount = fileCount.reverse();
                createChartArrays(chartDataArray, chartTicksArray, lastAccessed , fileCount);
                var dataForChart = {
                		chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray
                }
                return dataForChart;
            }),
            
            chartFilesNOwner: this.store.findQuery("FilesNOwner", chartFileNOwnerQuery).then(function(chartData){
            	function renderdataForChart(dataHolder, ownerCount, fileCount) {
                    var length = ownerCount.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	dataHolder.push({"label" :ownerCount[count], "data":fileCount[count]});
                    }
                }
                var filenOwner = [],
                ownerCount = chartData.getEach("key"),
                fileCount = chartData.getEach("doc_count");
                renderdataForChart(filenOwner, ownerCount , fileCount);
                var dataForChart = {
                		filenOwner: filenOwner
                }
                return dataForChart;
            }),
            
            dataForTagCloud : this.store.findQuery("tagCloud", dataForTagCloud).then(function(chartData){
            	function renderdataForChart(dataHolder, ownerValue, sizeValue) {
                    var length = ownerValue.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                        dataHolder.push({
                        	"key":ownerValue[count],
                        	"doc_count":sizeValue[count]
                        	})
                    }
                }
                var tagCloud = [],
                ownerValue = chartData.getEach("key"),
                sizeValue = chartData.getEach("doc_count");
                renderdataForChart(tagCloud, ownerValue , sizeValue);
                var dataForChart = {
                		tagCloud: tagCloud
                }
                return dataForChart;
            })
        })
        return completeData;
        /**
         * data contains the modified response which is being used in this Ember application.
         * Actual response is being modified in the tree like structure
         * ( DomainName --> Capability Name --> Application Name --> Object Name)
         */
        /*var query = {
        		"type":"navigationObjectURL",
        		"query":{
        			"from": 0,
        	        "size": 100,
        		    "_source": {
        		        "include": ["Object Name", "domain", "capability", "application"]
        		    },
        		    "query": {
        		        "query_string": {
        		            "query": "*"
        		        }
        		    }
        		}
        };
        var data = this.store.findQuery("navigation", query).then(function(data) {

            */
        /**
         * uniqueEntity function() return an array without any duplicate value in them
         */
        /*
                    function uniqueEntity(array) {
                        var uniqueNames = [];
                        $.each(array, function(i, el) {
                            if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                        });
                        return uniqueNames;
                    }
                    var topObj = [],
                        domainArr = data.getEach("domain"),
                        uniqueDNames = uniqueEntity(domainArr),
                        domainIndex,
                        capabilityIndex,
                        applicationIndex,
                        objectIndex,
                        capabArr = [];
                    for (domainIndex = 0; domainIndex < uniqueDNames.length; domainIndex += 1) {
                        topObj.push({
                            'domainName': uniqueDNames[domainIndex]
                        });
                        var filterByDomain = data.filterBy("domain", uniqueDNames[domainIndex]);
                        capabArr = filterByDomain.getEach("capability");
                        var uniqueCNames = uniqueEntity(capabArr);
                        for (capabilityIndex = 0; capabilityIndex < uniqueCNames.length; capabilityIndex += 1) {
                            if (!topObj[topObj.length - 1]['capability']) {
                                topObj[topObj.length - 1]['capability'] = [];
                            }
                            topObj[topObj.length - 1]['capability'].push({
                                'capabilityName': uniqueCNames[capabilityIndex]
                            });
                            var applicationArr = [],
                                filterByCapability = filterByDomain.filterBy("capability", uniqueCNames[capabilityIndex]);
                            applicationArr = filterByCapability.getEach("application");
                            var uniqueANames = uniqueEntity(applicationArr),
                                tempApp = topObj[domainIndex].capability;
                            for (applicationIndex = 0; applicationIndex < uniqueANames.length; applicationIndex += 1) {
                                if (!tempApp[tempApp.length - 1]["application"]) {
                                    tempApp[tempApp.length - 1]["application"] = [];
                                }
                                tempApp[tempApp.length - 1]["application"].push({
                                    'applicationName': uniqueANames[applicationIndex]
                                });
                                var ObjectArr = [],
                                    filterByApplication = filterByCapability.filterBy("application", uniqueANames[applicationIndex]);
                                ObjectArr = filterByApplication.filterBy("id");
                                var tempObj = tempApp[capabilityIndex].application;
                                for (objectIndex = 0; objectIndex < ObjectArr.length; objectIndex += 1) {
                                    if (!tempObj[tempObj.length - 1]["object"]) {
                                        tempObj[tempObj.length - 1]["object"] = [];
                                    }
                                    tempObj[tempObj.length - 1]["object"].push({
                                        'objectName': ObjectArr[objectIndex].get("objectName"),
                                        'objectId': ObjectArr[objectIndex].get("id"),
                                        'objectData': ObjectArr[objectIndex]
                                    })
                                }
                            }
                        }
                    }
                    return topObj;
                });
                return data;*/
    }
   
});