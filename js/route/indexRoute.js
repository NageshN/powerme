/**
 *  Index Route used here just for redirecting to navigation template
 */
PowerMe.IndexRoute = Ember.Route.extend({
    redirect: function() {
        this.transitionTo('navigation');
    }
})