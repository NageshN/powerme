
PowerMe.ReportsRoute = Ember.Route.extend({
	model: function(params){
		var self = this;
		this.controllerFor('results').set('type', "reports");
		$(".subjectAreaTiles .active").removeClass("active");
		params.subjectArea = decodeURIComponent(params.subjectArea);
		this.controllerFor('results').set('subjectAreaData',params.subjectArea);
		this.set('subjectAreaData',params.subjectArea);
		if (params.orderKey === "asc"){
			$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
				this.controllerFor('reports').set("order","ascending");
				
		}else{
			$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
			this.controllerFor('reports').set("order","descending");
		}
		$(".subjectAreaTiles .subjectTile").filter(function(index) { return $(this).text() === params.subjectArea; }).closest("a").addClass("activeAsc");
		var startIndex = (2*(params.pageNumber) - 2)*100;
		var currentPage = params.pageNumber;
		this.controllerFor('results').set('startPage', currentPage);
		
		var queryType = this.controllerFor('navigation').get("clickedSelectedOption");
		var queryString = this.controllerFor('navigation').get("search");
		/*var subjectResultsReportsQuery = {
        	    "type": "resultsReportsURL",
        	    "query":  {
        	    	"from":startIndex,
                    "size": 200,
                    "sort":{"usageCount" : { "order": params.orderKey,"mode" : "max", "missing" : "_last"}},
                    "_source": [],
                    "query": {
                        "match": {
                            "subjectArea": params.subjectArea
                        }
                    }
                }
        	};*/
		
		 var subjectNewQueryData = {
	        		"type": "newSearchResultURL",
	        		"query": {
                        "from": startIndex,
                        "size": 200,
                        "sort":{"usageCount" : { "order": params.orderKey,"mode" : "max", "missing" : "_last"}},
                        "_source": [
                            "reportName",
                            "subjectArea",
                            "owner",
                            "system",
                            "department",
                            "usageCount"
                        ],
                        "query": {
                            "match_phrase_prefix": {
                                "subjectArea": queryString.replace(/[&\/\\#,+()$~%.'":*-?<>{}]/g, ' ').trim() + "*"
                            }
                        },
                        "filter": {
                            "term": {
                                "subjectArea1": params.subjectArea
                            }
                        }
                    }
	        };
		//var editedQueryString = queryString
		/*var allResultsReportsQuery = {
        	    "type": "resultsReportsURL",
        	    "query":   {
        	    	"from":startIndex,
        	    	"size": 200,
        	    	"sort":{"usageCount" : { "order": params.orderKey,"mode" : "max", "missing" : "_last"}},
        	    	"query":{
        	    	    "filtered": {
        	    	    	  "query" : { "query_string" : {"query" :  queryString.replace(/[&\/\\#,+()$~%.'":*-?<>{}]/g, ' ').trim() + "*"} },
        	    	        "filter": {
        	    	            "bool": {
        	    	                "must": {
        	    	                    "term": {
        	    	                        "subjectArea": params.subjectArea
        	    	                    }
        	    	                }
        	    	            }
        	    	        }
        	    	    }
        	    	}
        	    }
        	};*/
		
		var allNewDataOnTileClick = {
				"type": "newSearchResultURL",
				"query": {
				    "from": startIndex,
				    "size": 200,
				    "sort": {
				        "usageCount": {
				            "order": params.orderKey,"mode" : "max", "missing" : "_last",
				            "mode": "max",
				            "missing": "_last"
				        }
				    },
				    "_source": [
				        "reportName",
				        "subjectArea",
				        "owner",
				        "system",
				        "department",
				        "usageCount"
				    ],
				    "query": {"multi_match": {
				    	 
				    	 
				    	 
                        "query": queryString.replace(/[&\/\\#,+()$~%.'":*-?<>{}]/g, ' ').trim() + "*",
                        "type": "phrase_prefix",
                        "fields": [ "reportName", "description"]
                        }},
				            "filter": {
				                "bool": {
				                    "must": {
				                        "term": {
				                            "subjectArea1": params.subjectArea
				                        }
				                    }
				                }
				            }
				        }
				    }
				}

		}
		
		
		var resultsReportsQuery = queryType === "All" ? allNewDataOnTileClick : subjectNewQueryData;
		return Ember.RSVP.hash({
			data : this.store.findQuery('Result', resultsReportsQuery).then(function(resultData) {
				var resultDataArray = resultData.content;  
				 for(var count= 0; count < resultDataArray.length; count++ ){
	                    resultDataArray[count]._data['systemUpperCase'] = resultDataArray[count]._data['system'].toUpperCase();
	                }	
	        	resultData.totalFiles = resultData.get("firstObject") ? resultData.get("firstObject")._data.totoalFile : 0;
	        	/**
	        	 * The ID of different reports extracted from the resultData and further 
	        	 * this ID is used to get information related to the bookmark of those report.
	        	 */
	        	var queryString = {
	            	    "type": "bookmarkURL",
	            	    "query": {
	            	    	"from":0,
	            	    	"size":200,
	            	        "query": {
	            	            "bool": {
	            	                "must": [{
	            	                    "query_string": {
	            	                        "default_field": "bookmark.docId",
	            	                        "query": resultData.getEach("id").toString().replace(/,/g," or ")
	            	                    }
	            	                }, {
	            	                    "query_string": {
	            	                        "default_field": "bookmark.userId",
	            	                        "query": self.session.get("email")
	            	                    }
	            	                }]
	            	            }
	            	        }
	            	    }
	            	};
	        	/**
	        	 * Request to retrieve information related to bookmark for specific report and specific user.
	        	 */
	        	var result = resultData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){
	        		var reportList = bookmarkData.getEach("docId");
	        		for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
	            		//var normalizedSource = normalizeData(responseData[responseIndex]._source);
	            		var reportData = resultData.filterBy("id",reportList[responseIndex]);
	            		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
	            		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
	            		reportData.setEach("bookmark.bookmarkState",true);
	            		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
	            		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);
	            	}
	        		return resultData;
	        	})
	        	return result;
	        })
		});
		console.log("in");
	},
	serialize : function(model){
		  // model is the keyword in your case
		  return {keyword: encodeURIComponent(model)};
		}
	
	
});