PowerMe.ResultsIndexRoute = Ember.Route.extend({
	renderTemplate: function(controller) {
        this.render('reports', {controller: controller});
    },
    model: function(params){
    	var self = this;
    	this.controllerFor('results').set('type', "resultsIndex");
    	
    	console.log("I am in resultsIndex");
    	 if(params.sort === "asc"){
    			this.controllerFor('results.index').set("order","ascending");
    		}else{
    			this.controllerFor('results.index').set("order","descending");
    		}
    	$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
    	
    	var startIndex = (2*(params.pageNumber) - 2)*100;
		var currentPage = params.pageNumber;
		this.controllerFor('results').set('startPage', currentPage);
        
        params.query = this.controllerFor('navigation').get('search');
        params.queryType = this.controllerFor('navigation').get('clickedSelectedOption');
        var self = this;
  
        var queryString = params.query;
        params.query = queryString === "*" ? queryString :  queryString.replace(/[&\/\\#,+()$~%.'":*-?<>{}]/g, ' ').trim() + "*";
       // PowerMe.userName=this.controllerFor('navigation').get("searchQuery");
        
        /*var subjectQueryData = {
        	    "type": "searchResultURL",
        	    "query":  {
        	        "from": startIndex,
        	        "size": 200,
        	        "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
        	        "query": {
        	            "match_phrase_prefix": {
        	                "subjectArea1": params.query
        	            }
        	        }
        	    }
        	};*/
        
        var subjectNewQueryData = {
        		"type": "newSearchResultURL",
        		"query": {
                    "from": startIndex,
                    "size": 200,
                    "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
                    "_source": [
                        "reportName",
                        "subjectArea",
                        "owner",
                        "system",
                        "department",
                        "usageCount"
                    ],
                    "query": {
                        "match_phrase_prefix": {
                            "subjectArea": params.query
                        }
                    }
                }

        };
        
        //var queryText = (params.query).replace(/[&\/\\#,+()$~%.'":?<>{}]/g, ' ').trim();
        /*var allQueryData = {
        	    "type": "searchResultURL",
        	    "query":  {
        	        "from": startIndex,
        	        "size": 200,
        	        "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
        	        "query" : { "query_string" : {"query" : params.query} }
        	    }
        	};*/
        
        var allNewQueryData = {
        		"type": "newSearchResultURL",
        		"query": {
        		    "from": startIndex,
        		    "size": 200,
        		    "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
        		    "_source": [
        		        "reportName",
        		        "subjectArea",
        		        "owner",
        		        "system",
        		        "department",
        		        "usageCount"
        		    ],
        		    "query" : { 
                        "multi_match": {
                           "query": params.query,
                           "type": "phrase_prefix",
                           "fields": [ "reportName", "description"]
                        } }
        		}
        };
        
        var queryData = (params.queryType==="All") ? allNewQueryData : subjectNewQueryData ;
        return Ember.RSVP.hash({
        	data : this.store.findQuery('Result', queryData).then(function(resultData) {
        	var resultDataArray = resultData.content;   
        	for(var count= 0; count < resultDataArray.length; count++ ){
                resultDataArray[count]._data['systemUpperCase'] = resultDataArray[count]._data['system'].toUpperCase();
            }
        	resultData.totalFiles = resultData.get("firstObject") ? resultData.get("firstObject")._data.totoalFile : 0;
        	/**
        	 * The ID of different reports extracted from the resultData and further 
        	 * this ID is used to get information related to the bookmark of those report.
        	 */
        	var queryString = {
            	    "type": "bookmarkURL",
            	    "query": {
            	    	"from":0,
            	    	"size":200,
            	        "query": {
            	            "bool": {
            	                "must": [{
            	                    "query_string": {
            	                        "default_field": "bookmark.docId",
            	                        "query": resultData.getEach("id").toString().replace(/,/g," or ")
            	                    }
            	                }, {
            	                    "query_string": {
            	                        "default_field": "bookmark.userId",
            	                        "query": self.session.get("email")
            	                    }
            	                }]
            	            }
            	        }
            	    }
            	};
        	/**
        	 * Request to retrieve information related to bookmark for specific report and specific user.
        	 */
        	var result = resultData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){
        		var reportList = bookmarkData.getEach("docId");
        		for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
            		//var normalizedSource = normalizeData(responseData[responseIndex]._source);
            		var reportData = resultData.filterBy("id",reportList[responseIndex]);
            		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
            		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
            		reportData.setEach("bookmark.bookmarkState",true);
            		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
            		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);
            	}
        		return resultData;
        	})
        	return result;
        })
        })
    }
	
});