
PowerMe.NavigationResultsRoute = Ember.Route.extend({
    model: function(params) {
    	var self = this;
    	var system = params.system;
    	var department = params.department;
    	
    	var decodedSubjectArea = decodeURIComponent(params.application);
        var decodedDashboard = decodeURIComponent(params.key);
    	
    	this.controllerFor('navigationResults').setProperties({
    		navigationQueryEncoded: params.key
        });

    	this.controllerFor('navigationResults').setProperties({
            navigationQuery: decodedSubjectArea
        });  
    	this.controllerFor('navigationResults').setProperties({
    		startPage: 0
    	});
    	
    	this.controllerFor("navigationResults").setProperties({
    		navigationSystem: system
    	});
    	
    	this.controllerFor("navigationResults").setProperties({
    		navigationDepartment: department
    	});
    	
    	var navigationResultsquery =  {
                "type": "obieeNavigationResultsURL",
                "query":  {
                	"from": 0,
                    "size": 200,
                    "sort":{"usageCount" : { "order": "desc","mode" : "max", "missing" : "_last"}},
                    "_source": [],
                    "query": {
                        "match": {
                            "subjectArea": decodedSubjectArea
                        }
                    }
                }
            };
    	
    	/*var navigationUpdatedResultQuery = {
    			"type": "navigationLeftResultsURL",
    			"query": {
    			    "from": 0,
    			    "size": 200,
    			    "sort":{"usageCount" : { "order": "desc","mode" : "max", "missing" : "_last"}},
    			    "_source": [
    			        "reportName",
    			        "subjectArea",
    			        "owner",
    			        "system",
    			        "department",
    			        "usageCount"
    			    ],
    			    "query": {
    			        "filtered": {
    			            "query": {
    			                "match": {
    			                    "subjectArea1": decodedSubjectArea
    			                }
    			            },
    			            "filter": {
    			                "bool": {
    			                    "must": [{
    			                        "term": {
    			                            "system": system
    			                        }
    			                    }, {
    			                        "term": {
    			                            "department1": department
    			                        }
    			                    }]
    			                }
    			            }
    			        }
    			    }
    			}
    	};*/
    	 var navigationUpdatedResultQuery = {
                 "type": "navigationLeftResultsURL",
                 "query": {
                     "from": 0,
                     "size": 200,
                     "sort":{"usageCount" : { "order": "desc","mode" : "max", "missing" : "_last"}},
                     "_source": [
                         "reportName",
                         "subjectArea",
                         "owner",
                         "system",
                         "department",
                         "usageCount"
                     ],
                     "query": {
                         "filtered": {
                             "query": {
                                 "match": {
                                     "dashBoard1": decodedDashboard
                                 }
                             },
                             "filter": {
                                 "bool": {
                                     "must": [{
                                         "term": {
                                             "system": system
                                         }
                                     }, {
                                         "term": {
                                             "department1": department
                                         }
                                     }, {
                                          "term": {
                                             "subjectArea1": decodedSubjectArea
                                          }
                                       }
                                     ]
                                 }
                             }
                         }
                     }
                 }
         };
         
    	 var chartFilesnLocationQuery = {
                 "type": "filenLocationURL",
                 "query": {
                     "size": 0,
                     "aggs": {
                         "group_by_rootFolder": {
                             "terms": {
                                 "field": "rootFolder",
                                 "size": 10
                             }
                         }
                     }
                 }
             };
         
         var chartFileNOwnerQuery = {
         		"type": "fileNOwnerURL",
         		"query":  {
         		    "size": 0,
         		    "query": {
         		        "bool" : {
         		        "must_not" : [{"term" : { "owner" :"null/null" }},
         		                             {"term" : { "owner" :"null" }},
         		                             {"term" : { "owner" :"" }}
         		        ]
         		    }
         		    },
         		    "aggs": {
         		        "owners": {
         		            "terms": {
         		                "field": "owner",
         		                "size": 10
         		            }
         		        }
         		    }
         		}       		
         };
         
    	var completeData = Ember.RSVP.hash({
    		data: this.store.findQuery('Result', navigationUpdatedResultQuery).then(function(resultData) {
    			var resultDataArray = resultData.content;        		
        		resultData.totalFiles = resultData.get("firstObject") ? resultData.get("firstObject")._data.totoalFile : 0;
        		resultData.system = resultData.get("firstObject") ? resultData.get("firstObject")._data.system : "";        		
        		resultData.subjectArea = decodedSubjectArea;
        		resultData.pagesCount = Math.ceil(resultData.totalFiles/200) - 1;   
        		self.controllerFor('navigationResults').set('arrayLength',resultData.content.length );
        	
            	/**
            	 * The ID of different reports extracted from the resultData and further 
            	 * this ID is used to get information related to the bookmark of those report.
            	 */
            	var queryString = {
                	    "type": "bookmarkURL",
                	    "query": {
                	    	"from":0,
                	    	"size":200,
                	        "query": {
                	            "bool": {
                	                "must": [{
                	                    "query_string": {
                	                        "default_field": "bookmark.docId",
                	                        "query": resultData.getEach("id").toString().replace(/,/g," or ")
                	                    }
                	                }, {
                	                    "query_string": {
                	                        "default_field": "bookmark.userId",
                	                        "query": self.session.get("email")
                	                    }
                	                }]
                	            }
                	        }
                	    }
                	};
            	/**
            	 * Request to retrieve information related to bookmark for specific report and specific user.
            	 */
            	var result = resultData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){
            		var reportList = bookmarkData.getEach("docId");
            		for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
                		//var normalizedSource = normalizeData(responseData[responseIndex]._source);
                		var reportData = resultData.filterBy("id",reportList[responseIndex]);
                		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
                		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
                		reportData.setEach("bookmark.bookmarkState",true);
                		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
                		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);
                	}
            		return resultData;
            	})
            	return result;
            }),
            
            startRecord: this.controllerFor('navigationResults').get('startRecord'),
            
            lastRecord: this.controllerFor('navigationResults').get('lastRecord'),
            
            fileNLocationChartData  : this.store.findQuery("FilesnLocation", chartFilesnLocationQuery).then(function (chartData) {
            	function createChartArrays(dataHolder, ticksHolder, location, fileCount) {
                    var length = location.length,
                        count = 0;
                    for (count; count < length; count++) {
                        dataHolder.push([fileCount[count], count]);
                        ticksHolder.push([count, location[count]]);
                    }
                }
            	
                var chartDataArray = [],
                chartTicksArray = [],
                location = chartData.getEach("key"),
                fileCount = chartData.getEach("countInThousand");
                location = location.reverse();
                fileCount = fileCount.reverse();
                createChartArrays(chartDataArray, chartTicksArray, location , fileCount);
                var dataForChart = {
                		chartDataArray: chartDataArray,
                		chartTicksArray: chartTicksArray
                }
                return dataForChart;
            }),
            
            fileNOwnerChartData: this.store.findQuery("FilesNOwner", chartFileNOwnerQuery).then(function(chartData){
            	function renderdataForChart(dataHolder, ownerCount, fileCount) {
                    var length = ownerCount.length,
                        count = 0;
                    for (count; count < length; count += 1) {
                    	dataHolder.push({"label" :ownerCount[count], "data":fileCount[count]});
                    }
                }
                var filenOwner = [],
                ownerCount = chartData.getEach("key"),
                fileCount = chartData.getEach("doc_count");
                renderdataForChart(filenOwner, ownerCount , fileCount);
                var dataForChart = {
                		filenOwner: filenOwner            }
                return dataForChart;
            })
    	});
    	
    	return completeData;
    	
    }
});

PowerMe.NavigationResultsIndexRoute = Ember.Route.extend({
	renderTemplate: function(controller) {
        this.render('navResultReports', {controller: controller});
    }
	
});

