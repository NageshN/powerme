PowerMe.NavResultReportsRoute = Ember.Route.extend({
	model: function(params){	
		console.log('In nav result report route');
		var self = this;
		var recordsPerPage = this.controllerFor('navigationResults').get('recordsPerPage');
		var startIndex = recordsPerPage*(params.pageNumber - 1);
		var currentPage = params.pageNumber - 1;		
		this.controllerFor('navigationResults').set('startPage', currentPage);		
		var subjectArea = this.controllerFor('navigationResults').get('navigationQuery');
		var system = this.controllerFor('navigationResults').get('navigationSystem');
		var department = this.controllerFor('navigationResults').get('navigationDepartment');	
		
		var navigationUpdatedResultQuery = {
    			"type": "navigationLeftResultsURL",
    			"query": {
    			    "from": startIndex,
    			    "size": 200,
    			    "sort":{"usageCount" : { "order": "desc","mode" : "max", "missing" : "_last"}},
    			    "_source": [
    			        "reportName",
    			        "subjectArea",
    			        "owner",
    			        "system",
    			        "department",
    			        "usageCount"
    			    ],
    			    "query": {
    			        "filtered": {
    			            "query": {
    			                "match": {
    			                    "subjectArea1": subjectArea
    			                }
    			            },
    			            "filter": {
    			                "bool": {
    			                    "must": [{
    			                        "term": {
    			                            "system": system
    			                        }
    			                    }, {
    			                        "term": {
    			                            "department1": department
    			                        }
    			                    }]
    			                }
    			            }
    			        }
    			    }
    			}
    	};
		return Ember.RSVP.hash({
    		data: this.store.findQuery('Result', navigationUpdatedResultQuery).then(function(resultData) {  
    			var resultDataArray = resultData.content;
        		/*for(var count= 0; count < resultDataArray.length; count++ ){
        			resultDataArray[count]._data['systemUpperCase'] = resultDataArray[count]._data['system'].toUpperCase();        			   			
        		}*/
    			self.controllerFor('navigationResults').set('arrayLength', resultData.content.length);
        		resultData.totalFiles = resultData.get("firstObject") ? resultData.get("firstObject")._data.totoalFile : 0;
        		resultData.system = resultData.get("firstObject") ? resultData.get("firstObject")._data.system : "";      
        		
            	/**
            	 * The ID of different reports extracted from the resultData and further 
            	 * this ID is used to get information related to the bookmark of those report.
            	 */
            	var queryString = {
                	    "type": "bookmarkURL",
                	    "query": {
                	    	"from":0,
                	    	"size":200,
                	        "query": {
                	            "bool": {
                	                "must": [{
                	                    "query_string": {
                	                        "default_field": "bookmark.docId",
                	                        "query": resultData.getEach("id").toString().replace(/,/g," or ")
                	                    }
                	                }, {
                	                    "query_string": {
                	                        "default_field": "bookmark.userId",
                	                        "query": self.session.get("email")
                	                    }
                	                }]
                	            }
                	        }
                	    }
                	};
            	/**
            	 * Request to retrieve information related to bookmark for specific report and specific user.
            	 */
            	var result = resultData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){
            		var reportList = bookmarkData.getEach("docId");
            		for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
                		var reportData = resultData.filterBy("id",reportList[responseIndex]);
                		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
                		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
                		reportData.setEach("bookmark.bookmarkState",true);
                		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
                		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);
                	}
            		return resultData;
            	})
            	return result;
            })
           
    	})
	}
});