/**
 * Results Route starts from here
 */
PowerMe.ResultsRoute = Ember.Route.extend({
    model: function(params) {
    	$(".subjectAreaTiles .activeAsc").removeClass("activeAsc");
    	
    	var decodedSearchQuery = decodeURIComponent(params.query);
    	
        this.controllerFor('navigation').setProperties({
            searchQuery: params.query,
            search: decodedSearchQuery,
            selectedOption :	params.queryType
        });
        var self = this;

        var queryString = decodedSearchQuery;
        decodedSearchQuery = queryString === "*" ? queryString :  queryString.replace(/[&\/\\#,+()$~%.'":*-?<>{}]/g, ' ').trim() + "*";

        var subjectQueryData = {
        	    "type": "searchResultURL",
        	    "query":  {
        	        "from": 0,
        	        "size": 200,
        	        "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
        	        "query": {
        	            "match_phrase_prefix": {
        	                "subjectArea1": decodedSearchQuery
        	            }
        	        }
        	    }
        	};
             
        
        //var queryText = (params.query).replace(/[&\/\\#,+()$~%.'":?<>{}]/g, ' ').trim();
        var allQueryData = {
        	    "type": "searchResultURL",
        	    "query":  {
        	        "from": 0,
        	        "size": 200,
        	        "sort":{"usageCount" : { "order": this.controllerFor('results.index').get("orderKey"),"mode" : "max", "missing" : "_last"}},
        	        "query" : { "query_string" : {"query" : decodedSearchQuery} }
        	    }
        	};
        var queryData = (params.queryType==="All") ? allQueryData : subjectQueryData ;
        var chartFilesnLocationQuery = {
                "type": "filenLocationURL",
                "query": {
                    "size": 0,
                    "aggs": {
                        "group_by_rootFolder": {
                            "terms": {
                                "field": "rootFolder",
                                "size": 10
                            }
                        }
                    }
                }
            };
        
        var chartFileNOwnerQuery = {
        		"type": "fileNOwnerURL",
        		"query":  {
        		    "size": 0,
        		    "query": {
        		        "bool" : {
        		        "must_not" : [{"term" : { "owner" :"null/null" }},
        		                             {"term" : { "owner" :"null" }},
        		                             {"term" : { "owner" :"" }}
        		        ]
        		    }
        		    },
        		    "aggs": {
        		        "owners": {
        		            "terms": {
        		                "field": "owner",
        		                "size": 10
        		            }
        		        }
        		    }
        		}       		
        };
        
       /* var resultsSubjectTileQueryInSubject = {
        		"type": "resultsSubjectTileURL",
        		"query":  {
        		    "size": 0,
        		    "query": {
        		         "match_phrase_prefix": {
        		             "subjectArea1": params.query
        		         }
        		     },
        		     "aggs": {
        		         "group_by_subjectArea": {
        		             "terms": {
        		                 "field": "subjectArea",
        		                 "size": 200
        		             }
        		         }
        		     }
        		 }       		
        };*/
        
        var newresultsSubjectTileQueryInSubject = {
        		"type": "newResultsSubjectTileURL",
        		"query":   {
                    "size": 0,
                    "query": {
                         "match_phrase_prefix": {
                             "subjectArea": decodedSearchQuery
                         }
                     },
                     "aggs": {
                         "group_by_subjectArea": {
                             "terms": {
                                 "field": "subjectArea1",
                                 "size": 200
                             }
                         }
                     }
                 }
        };
        
        
      /*  var resultsSubjectTileQueryInAll = {
        		"type": "resultsSubjectTileURL",
        		"query":  {
        		    "size": 0,
        		    "query" : { "query_string" : {"query" : params.query} },
        		     "aggs": {
        		         "group_by_subjectArea": {
        		             "terms": {
        		                 "field": "subjectArea",
        		                 "size": 200
        		             }
        		         }
        		     }
        		 }	
        };*/
        
        
        var newResultSubjectTileQueryInAll = {
        		"type": "newResultsSubjectTileURL",
        		"query" : {
        		    "size": 0,
        		    "query" : { "multi_match": {
        		    	 
                        "query": decodedSearchQuery,
                        "fields": [ "reportName", "description"]
                     } },
        		    "aggs": {
        		        "group_by_subjectArea": {
        		            "terms": {
        		                "field": "subjectArea1",
        		                "size": 200
        		            }
        		        }
        		    }
        		}

        };
        
        
        var resultsSubjectTileQuery = (params.queryType==="All") ? newResultSubjectTileQueryInAll : newresultsSubjectTileQueryInSubject ;
        /**
         * queryString is being sent to the result Adatpter.
         */
        
        return Ember.RSVP.hash({
        	/*data : this.store.findQuery('Result', queryData).then(function(resultData) {
        	resultData.totalFiles = resultData.get("firstObject") ? resultData.get("firstObject")._data.totoalFile : 0;
        	*//**
        	 * The ID of different reports extracted from the resultData and further 
        	 * this ID is used to get information related to the bookmark of those report.
        	 *//*
        	var queryString = {
            	    "type": "bookmarkURL",
            	    "query": {
            	    	"from":0,
            	    	"size":200,
            	        "query": {
            	            "bool": {
            	                "must": [{
            	                    "query_string": {
            	                        "default_field": "bookmark.docId",
            	                        "query": resultData.getEach("id").toString().replace(/,/g," or ")
            	                    }
            	                }, {
            	                    "query_string": {
            	                        "default_field": "bookmark.userId",
            	                        "query": PowerMe.userName
            	                    }
            	                }]
            	            }
            	        }
            	    }
            	};
        	*//**
        	 * Request to retrieve information related to bookmark for specific report and specific user.
        	 *//*
        	var result = resultData.store.findQuery('Bookmarking', queryString).then(function(bookmarkData){
        		var reportList = bookmarkData.getEach("docId");
        		for(var responseIndex=0; responseIndex < reportList.length ; responseIndex += 1){
            		//var normalizedSource = normalizeData(responseData[responseIndex]._source);
            		var reportData = resultData.filterBy("id",reportList[responseIndex]);
            		var temp = bookmarkData.filterBy("docId",reportList[responseIndex]);
            		reportData.setEach("bookmark.bookmarkName",temp.getEach("bookmarkName")[0]);
            		reportData.setEach("bookmark.bookmarkState",true);
            		reportData.setEach("bookmark.bookmarkId",temp.getEach("id")[0]);
            		reportData.setEach("bookmark.reportId",temp.getEach("docId")[0]);
            	}
        		return resultData;
        	})
        	return result;
        }),*/
        fileNLocationChartData  : this.store.findQuery("FilesnLocation", chartFilesnLocationQuery).then(function (chartData) {
        	function createChartArrays(dataHolder, ticksHolder, location, fileCount) {
                var length = location.length,
                    count = 0;
                for (count; count < length; count++) {
                    dataHolder.push([fileCount[count], count]);
                    ticksHolder.push([count, location[count]]);
                }
            }
        	
            var chartDataArray = [],
            chartTicksArray = [],
            location = chartData.getEach("key"),
            fileCount = chartData.getEach("countInThousand");
            location = location.reverse();
            fileCount = fileCount.reverse();
            createChartArrays(chartDataArray, chartTicksArray, location , fileCount);
            var dataForChart = {
            		chartDataArray: chartDataArray,
            		chartTicksArray: chartTicksArray
            }
            return dataForChart;
        }),
        
        fileNOwnerChartData: this.store.findQuery("FilesNOwner", chartFileNOwnerQuery).then(function(chartData){
        	function renderdataForChart(dataHolder, ownerCount, fileCount) {
                var length = ownerCount.length,
                    count = 0;
                for (count; count < length; count += 1) {
                	dataHolder.push({"label" :ownerCount[count], "data":fileCount[count]});
                }
            }
            var filenOwner = [],
            ownerCount = chartData.getEach("key"),
            fileCount = chartData.getEach("doc_count");
            renderdataForChart(filenOwner, ownerCount , fileCount);
            var dataForChart = {
            		filenOwner: filenOwner            }
            return dataForChart;
        }),
        
        resultsSubjectTileData : this.store.findQuery("ResultsSubjectTile", resultsSubjectTileQuery).then(function(tileData){
        	var subjectValues = [];
        	var subjectAreas = tileData.getEach("key");
        	var subjectCounts = tileData.getEach("doc_count");
        	for (var systemCount=0 ; systemCount < subjectAreas.length ; systemCount += 1){
        		subjectValues.push({
                    'subjectArea': subjectAreas[systemCount],
                    'encodedSubjectArea' : encodeURIComponent(subjectAreas[systemCount]),
                    'subjectCount': subjectCounts[systemCount]
                });
        	}
        	return subjectValues;
        })
        
        
        
        })
    },
    afterModel : function(model){
    	console.log("this hook is called");
    	//model.reload();
    }
})






