PowerMe.SecuredRoute = Ember.Route.extend({
	beforeModel: function(transition){
		console.log(".........inside ember secured route...........")
	    this._super();
		function getCookie(cname) {
		    var name = cname + "=";
		    var ca = document.cookie.split(';');
		    for(var i=0; i<ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1);
		        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
		    }
		    return "";
		}
		
		if(getCookie('sessionId') && getCookie('email')){
			this.session.set('sessionId', getCookie('sessionId'));
			this.session.set('userId', getCookie('userId'));
			this.session.set('firstName', getCookie('firstName'));
			this.session.set('lastName', getCookie('lastName'));
			this.session.set('email', getCookie('email'));
		}

	    if(this.session && !(this.session.get('isAuthenticated'))){
	    	var route = this.get('router').router.activeTransition.targetName;
	    	this.session.set('attemptedTransition',transition);
	    	this.transitionTo('loginPage');
	    	
	    }
		}
})