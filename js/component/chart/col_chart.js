    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
         ['Element', 'Density'],
         ['Copper', 8.94],            // RGB value
         ['Silver', 10.49],            // English color name
         ['Gold', 19.30],

       ['Platinum', 21.45 ], // CSS-style declaration
      ]);
      var view = new google.visualization.DataView(data);
      

      var options = {
       
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }