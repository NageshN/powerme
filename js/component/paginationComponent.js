PowerMe.PagiNationComponent = Ember.Component.extend({
	startPageChanged : function(){
		var containerId = this.get('containerId');
		var type = this.get("type");
		if(type == "navResultsPage"){
			var selectedPage = this.get('startPage');
		}
		else{
			var selectedPage = this.get('startPage')-1;
		}
		$('#'+ containerId).pagination('destroy');
		$('#'+ containerId).pagination('selectCurrentPage', selectedPage);
		$('#'+ containerId).pagination('redraw');
	}.observes('startPage'),
	
	totalFilesChanged : function(){
		var self = this;
		var containerId = this.get('containerId');
		var searchSubject = this.get('subjectArea');
		$('#'+ containerId).pagination('destroy');
		if(this.get("totalFiles") > 200 && (this.get("type")!= "navResultsPage")){
			$('#'+ containerId).pagination({
				items: this.get('totalFiles'),
				itemsOnPage: this.get('recordsPerPage'),
				cssStyle: 'light-theme',
				dispalyedPages: 5,
				edges: 1,
				onPageClick: function(pageNumber){		
					self.sendAction('create', pageNumber);				
				}	
			})
		}
		
	}.observes('totalFiles'),
	
	_initialize: function(){
		var self = this;
		var containerId = this.get('containerId');
		var searchSubject = this.get('subjectArea');
		$('#'+ containerId).pagination({
			items: this.get('totalFiles'),
			itemsOnPage: this.get('recordsPerPage'),
			cssStyle: 'light-theme',
			dispalyedPages: 5,
			edges: 1,
			onPageClick: function(pageNumber){	
				if(self.get("type") == "navResultsPage"){
					self.sendAction('create', pageNumber);
				}
				else{
					self.sendAction('create', pageNumber,self.get("type"));
				}								
			},
			onInit: function(){
				var type = self.get("type");
				console.log('Pagination Initialized');				
				$('#'+ containerId).pagination('destroy');
				if(type == "navResultsPage"){
					var selectedPage = self.get('startPage');
				}
				else{
					var selectedPage = self.get('startPage')-1;
				}				
				$('#'+ containerId).pagination('selectCurrentPage', selectedPage);
				$('#'+ containerId).pagination('redraw');
			}	
		});
		
	}.on('didInsertElement')	
});