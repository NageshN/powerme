PowerMe.FlotLinechartComponent = Ember.Component.extend({
	actions: {
		
	},
	_initialize: function(){
		var reportName = this.get('usageStatistics').get("name"),
	  	pathValue = this.get('usageStatistics').get("path"),
	  	department=this.get('usageStatistics').get("department"),
	  	system=this.get('usageStatistics').get("system").toLowerCase(),
	  	subjectArea = this.get('usageStatistics').get("subjectArea");
	  	chartId= this.get('chartId');
		
		var usageChartDataQuery = {
	  			"type": "updateUsageChartURL",
	  			"query": {
	  			    "size": 0,
	  			    "query": {
	  			        "bool": {
	  			            "must": [
	  			                {
	  			                    "term": {
	  			                        "system": system
	  			                    }
	  			                },
	  			                {
	  			                    "term": {
	  			                        "department": department
	  			                    }
	  			                },
	  			                {
	  			                    "term": {
	  			                        "SUBJECT_AREA_NAME": subjectArea
	  			                    }
	  			                },
	  			                {
	  			                    "term": {
	  			                        "SAW_SRC_PATH": pathValue
	  			                    }
	  			                }
	  			            ]
	  			        }
	  			    },
	  			    "aggs": {
	  			        "usageGraph": {
	  			            "terms": {
	  			                "field": "START_DT",
	  			                "size": 10
	  			            }
	  			        }
	  			    }
	  			}
	  	};
		//console.log('usageChartDataQuery '+usageChartDataQuery);
		 //console.log('URL '+PowerMe.Constants.serviceUrl.baseURL + PowerMe.Constants.serviceUrl.updateUsageChartURL);
		$.ajax({
			  type: "POST",
	          contentType: "application/json",
			  url: PowerMe.Constants.serviceUrl.baseURL + PowerMe.Constants.serviceUrl.updateUsageChartURL,
			 
			  data: JSON.stringify(usageChartDataQuery.query),
			  dataType: "json",
			 success: function(usageChartData) {
				console.log('In usage chart component');
				var chartDataArray = usageChartData.aggregations.usageGraph.buckets;		         
		         function sortFunction(a,b){  
		             return a.key - b.key;  
		         };
		          
		        console.log('chartDataArray.length '+chartDataArray.length);
		        chartDataArray.sort(sortFunction);
				var dateArray =[];
	     		var usageCountArray = [];			
	     		for(var count=0; count < chartDataArray.length; count++){
					dateArray.push([count, chartDataArray[count].key_as_string]);
					usageCountArray.push([chartDataArray[count].doc_count,count]);
				}				
	     		console.log('lineDate '+dateArray);
	     		console.log('lineUsage '+usageCountArray);

	     		var dataset = [
				               { data: usageCountArray }
				           ];
				var options = {
			            series: {
			                lines: {
			                    show: true,
								fillColor: {colors: [{ opacity: 1 }, { opacity: 1 } ]}	,
								lineWidth: 1
			                },
			              
			                points: {
			                    radius: 4,
			                    fill: true,
			                    show: true,
								fillColor: '#FAA41A'
			                }
			            },
			            xaxis: {
			                tickLength: 0,
			                axisLabel: "Date",
			                axisLabelUseCanvas: true,
			                axisLabelFontSizePixels: 12,
			                axisLabelFontFamily: 'Verdana, Arial',
			                axisLabelPadding: 20,
							ticks: dateArray,
		                    
			            },
			            yaxes: [{
							tickLength: 0,
			                axisLabel: "Usage Count",
			                axisLabelUseCanvas: true,
			                axisLabelFontSizePixels: 12,
			                axisLabelFontFamily: 'Verdana, Arial',
			                axisLabelPadding: 20
			                
			            }
			          ],
			            legend: {
							show: false,
			                noColumns: 0,
			                labelBoxBorderColor: "#000000",
			                position: "ne"
			            },
			            grid: {
			                hoverable: true,
			                borderWidth: {
								top: 0,
								right: 0,
								bottom: 2,
								left: 2
							},
			                borderColor: "#E6E6E6",
			                backgroundColor: { colors: ["#FFF", "#FFF"] }
			            },
			            colors: ["black"]
			        };
	    		
				$.plot($("#"+ chartId), dataset, options);
				useTooltip.apply($("#" + chartId));
	            var previousPoint = null, previousLabel = null; 
	    		
	   		 function useTooltip () {
	   				$(this).bind("plothover", function(event, pos, item) {
	   					if (item) {
	   						if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
	   							previousPoint = item.dataIndex;
	   							previousLabel = item.series.label;
	   							$("#tooltip").remove();
	   							var x = item.datapoint[0],
	   								y = item.datapoint[1];
	   							var color = item.series.color;
	   							showTooltip(pos.pageX - 10, pos.pageY + 30, "</strong>" + item.series.xaxis.ticks[x].label + ":<strong>&nbsp;" + y + "</strong>");
	   						} else {
	   							$("#tooltip").css("top", pos.pageY + 20);
	   							$("#tooltip").css("left", pos.pageX);
	   						}
	   					} else {
	   						$("#tooltip").remove();
	   						previousPoint = null;
	   					}
	   				});
	   			};
	   			
	   			function showTooltip(x, y, contents) {
	   				$('<div id="tooltip">' + contents + '</div>').css({
	   					position: 'absolute',
	   					display: 'none',
	   					top: y - 10,
	   					left: x + 10,
	   					padding: '10px',
	   					border: '1px solid #666',
	   					'font-size': '12px',
	   					'border-radius': '5px',
	   					'background-color': '#fff',
	   					'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
	   					opacity: 0.9,
	   				}).appendTo("body").fadeIn(200);
	   			}
	    	  
	    	  console.log("inside");
			 }
			 
		  });
		  
	}.on('didInsertElement')
});
