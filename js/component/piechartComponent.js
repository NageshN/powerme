PowerMe.PieChartComponent = Ember.Component.extend({
	click: function(evt){
	},
	actions: {
	  
	},
	_initialize: function(){
		  var data = this.get("chartData");
		  var pieChartId = this.get("chartId");
		  var plot1 = jQuery.jqplot (pieChartId, [data], {
			  seriesDefaults: {
			         renderer: jQuery.jqplot.PieRenderer,
			         rendererOptions: {
			             showDataLabels: true,
			             shadow: false
			         }
			     },
			     /*seriesColors: ['red', 'orange', 'yellow', 'green', 'blue', 'indigo'],*/
			     legend: {
			         show: true,
			         location: 'e'
			     },
			     title: {
			         text: this.get("chartTitle")
			     },
			     highlighter: {
			         show: true,
			         //tooltipAxes: 'pieref', // exclusive to this version
			         useAxesFormatters: false,
			         formatString: '%s: %P',
			         tooltipLocation: 'se',
			         sizeAdjust: -7
			         //tooltipOffset: 30
			     },
			     grid: {
			         borderColor: 'transparent',
			         shadow: false,
			         shadowColor: 'transparent',			        
			         background: 'rgba(0, 0, 0, 0)'
			     }
			}
		  );
	}.on('didInsertElement')
})