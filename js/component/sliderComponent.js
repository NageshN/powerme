PowerMe.MainSliderComponent = Ember.Component.extend({
  didInsertElement: function(){
    var thisSlider = this.$('.main_slider');

    thisSlider.cycle({
      slideResize:0
    });
    thisSlider.css({
        'width':'auto'
    });
    $(window).resize(function(){
        var height = thisSlider.height();
        thisSlider.css("height",height + 'px');
    });
  }
});