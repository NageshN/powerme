/**
 * Controller for usagechart component is defined here.
 */
PowerMe.TagCloudComponent = Ember.Component.extend({
	click: function(evt){
		//$(evt.target).toggleClass("bookmark_icon unbookmark_icon");
		//console.log("is");
	},
  actions: {
	  
  },
  _initialize : function(){
	  function shuffle(sourceArray) {
		    for (var n = 0; n < sourceArray.length - 1; n++) {
		        var k = n + Math.floor(Math.random() * (sourceArray.length - n));

		        var temp = sourceArray[k];
		        sourceArray[k] = sourceArray[n];
		        sourceArray[n] = temp;
		    }
		}
	  var tagCloudData = this.get("tagCloudData"),
	  	  str = "";
	  shuffle(tagCloudData);
	  
	  for(var i=0; i< tagCloudData.length; i++) {
			str += '<a rel="' + tagCloudData[i].doc_count + '">' + tagCloudData[i].key + '</a>';
			str +=' ';
		}
	  $('#tagCloud').html(str);
		
		$("#tagCloud a").tagcloud({
		 size: {
		   start: 10, 
		   end: 26, 
		   unit: 'pt'
		 }, 
		 color: {
		   start: "#9999FF", 
		   end: "#FF0066"
		 },
		 colorDataRange : ["#759cb5", "#f6d1da", "#5acbf5", "#2f2679", "#b2d234", "#3fba0c", "#f0860d", "#165a95", "#fbbb7d", "#5acbf5", "#8aa528", "#4d7f1d", "#d74278", "#bda056","#5acbf5", "#4d7f1d", "#164a7a", "#66b6e2", "#ad1a7e", "#02c3ef", "#e55453", "#848484", "#ff6633", "#33cc99", "#1d75bb", "#df4792", "#2a5699", "#49186e","#ed1a34", "#ffe600", "#c40c1d", "#2a5699", "#4d7f1d", "#f7931e", "#662d91", "#77b82a", "#c40c1d", "#8fb5cf", "#ecac03", "#4d7f1d", "#754c24", "#2a5699","#2a5699", "#29abe2", "#c69c6d", "#E0A315"]
		})
	  
	  console.log("in");
  }.on('didInsertElement')
});