PowerMe.BarChartComponent = Ember.Component.extend({
	click: function(evt){
		//$(evt.target).toggleClass("bookmark_icon unbookmark_icon");
		//console.log("is");
	},
	actions: {
	  
	},
	_initialize: function(){	
		var placeHolder = this.get("chartId");    	
		var chartData = this.get("chartData");
	    if(placeHolder == 'fileLocationSearch'){

		    var plot6 = $.jqplot('fileLocationSearch', 
		    		[
		    		 chartData
		    		], 
		    		{
			    	seriesColors: ["#57BCC1"],
			        seriesDefaults: {
			            renderer: $.jqplot.BarRenderer,
			            pointLabels: {
			                show: true,
			                location: 'e',
			                edgeTolerance: -15
			            },
			            shadowAngle: 135,
			            shadow: false,
			            rendererOptions: {
			                barDirection: 'horizontal',
			                barWidth: 10
			            },
		
			        },
			        axesDefaults: {
			            rendererOptions: {
			                drawBaseline: true
			            },
			            tickOptions: {
			                showGridline: true,
			                fontSize: '13px'
			            }
			        },
			        axes: {
			            xaxis: {
			                label: this.get("chartXLabel")
			            },
			            yaxis: {
			                renderer: $.jqplot.CategoryAxisRenderer,
			                label: this.get("chartYLabel"),
			                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
			            }
			        },
			        title: {
			            text: this.get("chartTitle")
			        },
			        highlighter: {
			            show: true,
			            tooltipContentEditor: tooltipContentEditor,
			            tooltipLocation: 'nw',
			            tooltipOffset: 7,
			            sizeAdjust: 5
			        },
			        grid: {
			            drawBorder: flase,
			            borderColor: 'transparent',
			            shadow: false,
			            shadowColor: 'transparent',
			            background: 'rgba(0, 0, 0, 0)'
			        }
		        }
		    	);					
		}
		
		else if(placeHolder == 'fileLocationNavigationSearch'){

		    var plot7 = $.jqplot('fileLocationNavigationSearch', 
		    		[
		    		 chartData
		    		], 
		    		{
			    	seriesColors: ["#57BCC1"],
			        seriesDefaults: {
			            renderer: $.jqplot.BarRenderer,
			            pointLabels: {
			                show: true,
			                location: 'e',
			                edgeTolerance: -15
			            },
			            shadowAngle: 135,
			            shadow: false,
			            rendererOptions: {
			                barDirection: 'horizontal',
			                barWidth: 10
			            },
		
			        },
			        axesDefaults: {
			            rendererOptions: {
			                drawBaseline: true
			            },
			            tickOptions: {
			                showGridline: true,
			                fontSize: '13px'
			            }
			        },
			        axes: {
			            xaxis: {
			                label: this.get("chartXLabel")
			            },
			            yaxis: {
			                renderer: $.jqplot.CategoryAxisRenderer,
			                label: this.get("chartYLabel"),
			                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
			            }
			        },
			        title: {
			            text: this.get("chartTitle")
			        },
			        highlighter: {
			            show: true,
			            tooltipContentEditor: tooltipContentEditor,
			            tooltipLocation: 'nw',
			            tooltipOffset: 7,
			            sizeAdjust: 5
			        },
			        grid: {
			            drawBorder: true,
			            borderColor: 'transparent',
			            shadow: false,
			            shadowColor: 'transparent',
			            background: 'rgba(0, 0, 0, 0)'
			        }
		        }
		    	);					
		}
	    	function tooltipContentEditor(str, seriesIndex, pointIndex, plot) {	
	    		return plot.data[seriesIndex][pointIndex][1] + ": " + plot.data[seriesIndex][pointIndex][0];
	    	};
	}.on('didInsertElement')
});