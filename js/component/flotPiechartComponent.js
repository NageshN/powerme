PowerMe.FlotPiechartComponent = Ember.Component.extend({
    click: function(evt) {},
    actions: {
    	updateReportsTile: function(dataArray, dataModel){
    		this.sendAction('updateTopReportsTiles', dataArray, dataModel);    		
    	},    	
    	updateUsersTile: function(dataArray, dataModel){
    		this.sendAction('updateTopUsersTiles', dataArray, dataModel);
    	}
    },
    _initialize: function() {
    
    	    
    	
    	var self= this;
    	var dataModel = this.get('dataModel');
        var data = this.get("chartData");
        var pieChartId = this.get("chartId");
        var chartTitleId = this.get("chartTitleId");
        var chartTitle = this.get('chartTitle');
        var previousPoint = null,
            previousLabel = null;
        
        var clickedSystemForNumberofFiles;
        
        renderPieChart(pieChartId, data);
        
        function renderPieChart(pieChartId, data){
        	 $.plot("#" + pieChartId, data, {
                 series: {
                     pie: {
                         show: true,
                         offset: {
                        	 'top': 1
                         }
                     },
                     
                 },
                 legend: {
			         show: true,
			         rendererOptions: {
			                numberRows: 1
			            },
			            
			         location: 'w'
			     },
			     
                 grid: {
                     hoverable: true,
                     clickable: false
                 },
                 colors: ["#e87352","#60cd9b", "#66b5d7", "#eec95a","#8876d6"]
             });
        	 
        	 $('#' + chartTitleId).html(chartTitle); 
        	 showMemo.apply($("#" + pieChartId));
        }
       

        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 10,
                left: x + 10,
                padding: '10px',
                border: '1px solid #666',
                'font-size': '12px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }

        function showMemo() {
            $(this).bind("plothover", function(event, pos, item) {
            	
                if (item) {
                    if ((previousLabel != item.series.label) ||
                        (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];

                        var color = item.series.color;            
                       
                        
                        showTooltip(pos.pageX - 10,
                            pos.pageY + 30,
                            color,
                            "</strong>" + item.series.label +
                            " : <strong>" + item.series.data[0][1] + "</strong>");
                    } else {
                        $("#tooltip").css("top", pos.pageY + 20);
                        $("#tooltip").css("left", pos.pageX);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        }
        
        
        /*Code for File Usage Vs System drill down */
        
        $("#filesUsageSystemPie").bind("plotclick", function (event, pos, item) {
			var clickedSystemValue = item.series.label;
			clickedSystemValue = clickedSystemValue.toLowerCase();
			
			//First call to fetch data for subject Area level chart
			$.ajax({
				type: "POST",
                contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
                    "size": 0,
                    "filter": {
                        "term": {
                            "system": clickedSystemValue
                        }
                    },
                    "aggs": {
                        "systems": {
                            "terms": {
                                "field": "subjectArea1",
                                "order": {
                                    "usageCount": "desc"
                                },
                                "size": 20
                            },
                            "aggs": {
                                "usageCount": {
                                    "sum": {
                                        "field": "usageCount"
                                    }
                                }
                            }
                        }
                    }
                }),
                dataType: "json",
                success: function(chartData){
                	var payload = chartData.aggregations.systems.buckets;
                	if (payload.length > 0){
                		$('#fileUsageSubjectAreaPie').css('display', 'block');
                		var pieChartId = "fileUsageSubjectAreaPie";
                		function renderdataForChart(dataHolder, subjectAreaCount, usageCount) {
                            var length = subjectAreaCount.length,
                                count = 0;
                            for (count; count < length; count += 1) {
                            	dataHolder.push({"label" :subjectAreaCount[count], "data":usageCount[count]});
                            }
                        }
                        
                        var fileUsageNSubjectArea = [],
                        subjectAreaCount = payload.getEach('key'),
                        usageCount = [];
                        var length = subjectAreaCount.length;
                        for(var count=0; count< length; count++){
                        	usageCount.push(payload[count].usageCount.value);
                        }
                        
                        renderdataForChart(fileUsageNSubjectArea, subjectAreaCount, usageCount );
                        renderPieChart(pieChartId, fileUsageNSubjectArea);
                        $('#fileUsageSystemPie').parent('div.panel wrapper').css('display', 'none');
                        $('#filesUsageSystemPie').css('display', 'none');
                        $('#fileUsageSubjectBack').show();
                        $('#fileUsageNSystemTitle').html('Report Usage by Application');
                	}
                }
			});
			
			//second call to fetch data for Top 5 report within the clicked system
			$.ajax({
				type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
                    "size": 5,
                    "_source": [
                        "reportName",
                        "system",
                        "subjectArea",
                        "usageCount",
                        "owner",
                        "department"
                    ],
                    "sort": {
                        "usageCount": {
                            "order": "desc",
                            "mode": "max",
                            "missing": "_last"
                        }
                    },
                    "filter": {
                        "term": {
                            "system": clickedSystemValue
                        }
                    }
                }),
                dataType: "json",
                success: function(data){
                	var idArray = data.hits.hits.getEach('_id');
                	var dataArray = data.hits.hits.getEach('_source');              	
                	var dataArrayLength = dataArray.length;
                	for(count=0; count< dataArrayLength; count++){
                		dataArray[count].docId = idArray[count];
                	}
                	if(dataArrayLength > 0){
                		self.send('updateReportsTile', dataArray, dataModel);               		                 		          		
                	}
                	
                }
			});
			
			//Third call to fetch top users within the clicked system
			
			$.ajax({
				type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/usagegraph/_search",
                data: JSON.stringify({
                    "size": 0,
                    "filter": {
                        "term": {
                            "system": clickedSystemValue
                        }
                    },
                    "aggs": {
                        "users": {
                            "terms": {
                                "field": "USER_NAME",
                                "size": 5
                            }
                        }
                    }
                }),
                dataType: "json",
                success: function(data){
                	var dataArray = data.aggregations.users.buckets;
                	var dataArrayLength = dataArray.length;
                	if(dataArrayLength> 0){
                		self.send('updateUsersTile', dataArray, dataModel);
                		
                	}
                }
			});
		});
        
        /*Code ends for File Usage Vs. System*/
        
        /*Code for No. of Files Vs System drill down */
        $("#filesCountSystemPie").bind("plotclick", function (event, pos, item){
        	var clickedSystemValue = item.series.label;	
        	clickedSystemValue = clickedSystemValue.toLowerCase();
        	clickedSystemForNumberofFiles = clickedSystemValue;   	
        	//First call to fetch subject area data
        	
        	$.ajax({
        		type: "POST",
                contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
				    "size": 0,
				    "filter": {
				        "term": {
				            "system": clickedSystemValue
				        }
				    },
				    "aggs": {
				        "subjectArea": {
				            "terms": {
				                "field": "subjectArea1",
				                "size":20
				            }
				        }
				    }
				}),
				dataType: "json",
				success: function(data){
					var payload = data.aggregations.subjectArea.buckets;
					if(payload.length > 0){
						$('#filesCountSubjectAreaPie').css('display', 'block');
                		var pieChartId = "filesCountSubjectAreaPie";
						function renderdataForChart(dataHolder, subjectAreaCount, filesCount) {
                            var length = subjectAreaCount.length,
                                count = 0;
                            for (count; count < length; count += 1) {
                            	dataHolder.push({"label" :subjectAreaCount[count], "data":filesCount[count]});
                            }
                        }
                        
                        var fileNumberNSubjectArea = [],
                        subjectAreaCount = payload.getEach('key'),
                        filesCount =  payload.getEach('doc_count');                                               
                        renderdataForChart(fileNumberNSubjectArea, subjectAreaCount, filesCount );
                        
                        renderPieChart(pieChartId, fileNumberNSubjectArea);
                        //$('#filesCountSystemPie').parent('div.panel wrapper').css('display', 'none');
                        $('#filesCountSystemPie').css('display', 'none');
                        $('#fileNumberSubjectBack').show();
                        $('#fileCountNSystemTitle').html('Reports by Application');
					}
				}
        	});
        	
        	//Second call to fetch top reports of the clicked system
        	
        	$.ajax({
				type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
                    "size": 5,
                    "_source": [
                        "reportName",
                        "system",
                        "subjectArea",
                        "usageCount",
                        "owner",
                        "department"
                    ],
                    "sort": {
                        "usageCount": {
                            "order": "desc",
                            "mode": "max",
                            "missing": "_last"
                        }
                    },
                    "filter": {
                        "term": {
                            "system": clickedSystemValue
                        }
                    }
                }),
                dataType: "json",
                success: function(data){
                	var idArray = data.hits.hits.getEach('_id');
                	var dataArray = data.hits.hits.getEach('_source');              	
                	var dataArrayLength = dataArray.length;
                	for(count=0; count< dataArrayLength; count++){
                		dataArray[count].docId = idArray[count];
                	}
                	if(dataArrayLength > 0){
                		self.send('updateReportsTile', dataArray, dataModel);               		                 		          		
                	}               	
                }
			});
        	
        	//Third call to fetch top users within clicked system
        	$.ajax({
				type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/usagegraph/_search",
                data: JSON.stringify({
                    "size": 0,
                    "filter": {
                        "term": {
                            "system": clickedSystemValue
                        }
                    },
                    "aggs": {
                        "users": {
                            "terms": {
                                "field": "USER_NAME",
                                "size": 5
                            }
                        }
                    }
                }),
                dataType: "json",
                success: function(data){
                	var dataArray = data.aggregations.users.buckets;
                	var dataArrayLength = dataArray.length;
                	if(dataArrayLength> 0){
                		self.send('updateUsersTile', dataArray, dataModel);
                		
                	}
                }
			});
        	
        });
        
        /*Code for Number of Files Vs. System drill down end here*/
        
        /*Code for Number of Files Vs. Subject Area drill down*/
        $("#filesCountSubjectAreaPie").bind("plotclick", function (event, pos, item){
        	var clickedSubjectArea = item.series.label;	
        	
        	/*First call to fetch file and their usage within the clicked system and subject Area*/
        	$.ajax({
        		type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
                	"size": 20,
                    "_source": [
                                "reportName",
                                "usageCount"
                            ],
                            "sort": {
                                "usageCount": {
                                    "order": "desc",
                                    "mode": "max",
                                    "missing": "_last"
                                }
                            },
                            "filter": {
                                "bool": {
                                    "must": [
                                        {
                                            "term": {
                                                "system": clickedSystemForNumberofFiles
                                            }
                                        },
                                        {
                                            "term": {
                                                "subjectArea1": clickedSubjectArea
                                            }
                                        }
                                    ]
                                }
                            }
                        }),
                dataType: "json",
                success: function(data){
                	var payload = data.hits.hits;
                	if(payload.length > 0){
                		$('#filesNUsageCountPie').css('display', 'block');
                		var pieChartId = "filesNUsageCountPie";
                		function renderdataForChart(dataHolder, fileCountArray, UsageCountArray) {
                            var length = fileCountArray.length,
                                count = 0;
                            for (count; count < length; count += 1) {
                            	dataHolder.push({"label" :fileCountArray[count], "data":UsageCountArray[count]});
                            }
                        }
                        
                		var fileCountArray = [];
                		var UsageCountArray = [];
                		var fileUsageNFiles = [];
                		for(var fileCount= 0; fileCount< payload.length; fileCount++){
                			fileCountArray.push(payload[fileCount]._source.reportName);
                			UsageCountArray.push(payload[fileCount]._source.usageCount);
                		}
                		var isUsageZero = true;
                		for(var check=0; check< UsageCountArray.length; check++){
                			if(UsageCountArray[check] != 0){  
                				isUsageZero = false;
                				break;
                			}
                		}
                		renderdataForChart(fileUsageNFiles, fileCountArray, UsageCountArray );                		
                    	renderPieChart(pieChartId, fileUsageNFiles);                    		
                          		
                		if(isUsageZero == true){
                			$('#filesNUsageCountPie').innerHTML = '';                			
                			$('#filesNUsageCountPie').html('<div class="usageDataNotFound">No Usage Data Found</div>');
                		}
                		
                		$('#filesCountSubjectAreaPie').css('display', 'none');
                        $('#fileNFileUsageBack').show();
                        $('#fileCountNSystemTitle').html('Reports by Usage Count');                        
                       
                	}
                }
        	});
        	
        	/*Second call to fetch top reports for the clicked System and Subject Area*/
        	
        	$.ajax({
        		type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/system/_search",
                data: JSON.stringify({
                    "size": 5,
                    "_source": [
                        "reportName",
                        "system",
                        "subjectArea",
                        "usageCount",
                        "owner",
                        "department"
                    ],
                    "sort": {
                        "usageCount": {
                            "order": "desc",
                            "mode": "max",
                            "missing": "_last"
                        }
                    },
                    "filter": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "system": clickedSystemForNumberofFiles
                                    }
                                },
                                {
                                    "term": {
                                        "subjectArea1": clickedSubjectArea
                                    }
                                }
                            ]
                        }
                    }
                }),
                success: function(data){
                	var idArray = data.hits.hits.getEach('_id');
                	var dataArray = data.hits.hits.getEach('_source');              	
                	var dataArrayLength = dataArray.length;
                	for(count=0; count< dataArrayLength; count++){
                		dataArray[count].docId = idArray[count];
                	}
                	if(dataArrayLength > 0){
                		self.send('updateReportsTile', dataArray, dataModel);               		                 		          		
                	}
                	
                }
        	});
        	
        	/*Third call to fetch top users for the clicked system and subject Area*/
        	
        	$.ajax({
        		type: "POST",
				contentType: "application/json",
                url: PowerMe.Constants.serviceUrl.baseURL + "/usagegraph/_search",
                data: JSON.stringify({
                    "size": 0,
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "term": {
                                        "system": clickedSystemForNumberofFiles
                                    }
                                },
                                {
                                    "term": {
                                        "PRESENTATION_NAME": clickedSubjectArea
                                    }
                                }
                            ]
                        }
                    },
                    "aggs": {
                        "users": {
                            "terms": {
                                "field": "USER_NAME",
                                "size": 5
                            }
                        }
                    }
                }),
                success: function(data){
                	var dataArray = data.aggregations.users.buckets;
                	var dataArrayLength = dataArray.length;
                	if(dataArrayLength> 0){
                		self.send('updateUsersTile', dataArray, dataModel);
                		
                	}
                }
        	});
        });
                
        
    }.on('didInsertElement')
})