PowerMe.FlotBarchartComponent = Ember.Component.extend({
    click: function(evt) {},
    actions: {

    },
    _initialize: function() {
    	//alert('flot');
    	$('#topDashboard, #fileAccessedMonthBar, #filesUsageSystemPie').css('height',Math.round(screenHeight*(23.5/100))+'px');
        $(".padder-v").css('padding-top',paddingTop);
        $(".padder-v").css('padding-bottom',paddingBottom);
    	var placeholder = this.get('chartId');
        var chartDataArray = this.get('chartDataArray');
        var chartTicksArray = this.get('chartTicksArray');
        var chartXLabel = this.get('chartXLabel');
        var chartYLabel = this.get('chartYLabel');
        var chartTitle = this.get('chartTitle');
        var chartTitleContainer = this.get('chartTitleId');
        var customTicks = this.get('customTicks');
        console.log("chartDataArray "+chartDataArray );
        console.log("chartTicksArray "+chartTicksArray );

        console.log('first '+chartDataArray);
        console.log('firstTicks '+customTicks);
        /*dataSet = [{
            data: chartDataArray,
            color: "#0ab596"
        }];*/
        var dataSet = [];
        var colors = ["#e87352","#60cd9b", "#66b5d7", "#eec95a","#8876d6"];
        //console.log('asd '+chartDataArray);
        for(var j=0;j<chartDataArray.length;j++){
        	console.log('in a for loop '+chartDataArray[j]);
        	dataSet.push({
        		"color" : colors[j],
        		"data" : [chartDataArray[j]]
        	});
        }
       
     /* var  dataSet = [
                     {color: '#ff00aa', data: [[0,3533]]},
                     {color: 'red', data: [[1,3325]]},
                     {color: 'yellow', data: [[2,3003]]},
                     
                 ];*/
      console.log('dataSetw '+ dataSet[0]['data']);
        var options = createOptionsArray(chartTicksArray, chartXLabel, chartYLabel);
        renderChartandTooltip(placeholder, dataSet, options);
        $('#' + chartTitleContainer).html(chartTitle);


        /*Drill down function for 'No. of Files Vs. Location' bar chart*/

        $("#fileLocationBar").bind("plotclick", function(event, pos, item) {
        	
        	if(item != null){
        		var clickedYAxisLabel = item.series.yaxis.ticks[item.datapoint[0]].label;
        		$.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: PowerMe.Constants.serviceUrl.baseURL + "/doc/_search?pretty",
                    data: JSON.stringify({
                        "size": 0,
                        "query": {
                            "bool": {
                                "must": {
                                    "term": {
                                        "rootFolder": clickedYAxisLabel	
                                    }
                                }
                            }
                        },

                        "aggs": {
                            "group_by_rootFolder": {
                                "terms": {
                                    "field": "fileExtension",
                                    "size": 10
                                }
                            }
                        }
                    }),
                    dataType: "json",
                    success: function(chartData) {
                        var payload = chartData.aggregations.group_by_rootFolder.buckets;
                        if (payload.length > 0) {
                            $('#fileLocationDrillDownBar').css('display', 'block');
                            function createChartArrays(dataHolder, ticksHolder, fileType, fileCountValue) {
                                var length = fileType.length,
                                    count = 0;
                                for (count; count < length; count++) {
                                    dataHolder.push([fileCountValue[count], count]);
                                    ticksHolder.push([count, fileType[count]]);
                                }
                            }
                            var chartDataArray = [],
                                chartTicksArray = [],
                                fileType = payload.getEach("key"),
                                fileCountValue = payload.getEach("doc_count");
                            fileType = fileType.reverse();
                            fileCountValue = fileCountValue.reverse();
                            createChartArrays(chartDataArray, chartTicksArray, fileType, fileCountValue);

                            var dataSet = [{
                                data: chartDataArray,
                                color: "#57BCC1"
                            }];
                            var chartXLabel = "No. of Files",
                                chartYLabel = "File Type",
                                placeholder = "fileLocationDrillDownBar";
                            var options = createOptionsArray(chartTicksArray, chartXLabel, chartYLabel);
                            renderChartandTooltip(placeholder, dataSet, options);
                            $('#fileLocationBackButton').show();
                            $('#fileLocationBar').css('display', 'none');
                            $('#fileNSizeTitle').html("No. of Files Vs. File Type (top 10)");
                        }
                    }
                })
        	}           
        });

        /*Function ended for 'No. of Files Vs. Location'*/

        /*Drill down function for 'Size Vs. Location' bar chart*/

        $("#sizeLocationBar").bind("plotclick", function(event, pos, item) {
        	if(item != null){
        		var clickedYAxisLabel = item.series.yaxis.ticks[item.datapoint[0]].label;
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: PowerMe.Constants.serviceUrl.baseURL + "/doc/_search?pretty",
                    data: JSON.stringify({
                        "size": 0,
                        "query": {
                            "bool": {
                                "must": {
                                    "term": {
                                        "rootFolder": clickedYAxisLabel
                                    }
                                }
                            }
                        },
                        "aggs": {
                            "filetype": {
                                "terms": {
                                    "field": "fileExtension",
                                    "size": 10
                                },
                                "aggs": {
                                    "totalFileSize": {
                                        "sum": {
                                            "field": "sizeInKB"
                                        }
                                    }
                                }
                            }
                        }
                    }),
                    dataType: "json",
                    success: function(chartData) {
                        var payload = chartData.aggregations.filetype.buckets;
                        if (payload.length > 0) {
                            $('#sizeLocationDrillDownBar').css('display', 'block');
                            function createChartArrays(dataHolder, ticksHolder, fileSizeType, fileSizeCountValue) {
                                var length = fileSizeType.length,
                                    count = 0;
                                for (count; count < length; count++) {
                                    dataHolder.push([fileSizeCountValue[count], count]);
                                    ticksHolder.push([count, fileSizeType[count]]);
                                }
                            }
                            var chartDataArray = [],
                                chartTicksArray = [],
                                fileSizeType = payload.getEach("key"),
                                fileSizeCountValue = payload.getEach("doc_count");
                            fileSizeType = fileSizeType.reverse();
                            fileSizeCountValue = fileSizeCountValue.reverse();
                            createChartArrays(chartDataArray, chartTicksArray, fileSizeType, fileSizeCountValue);

                            var dataSet = [{
                                data: chartDataArray,
                                color: "#57BCC1"
                            }];
                            var chartXLabel = "No. of Files",
                                chartYLabel = "File Type",
                                placeholder = "sizeLocationDrillDownBar";
                            var options = createOptionsArray(chartTicksArray, chartXLabel, chartYLabel);
                            renderChartandTooltip(placeholder, dataSet, options);
                            $('#sizeLocationBackButton').show();
                            $('#sizeLocationBar').css('display', 'none');
                            $('#fileNSizeTitle').html("No. of Files Vs. File Type (top 10)");
                        }
                    }
                })
        	}         
        });

        /*Function ended for 'Size Vs. Location'*/

        /*Drill down function for 'No. of Files Vs. Last Accessed' bar chart*/

        $("#fileLastAccessedBar").bind("plotclick", function(event, pos, item) {
        	if(item != null){
                var clickedYAxisLabel = item.series.yaxis.ticks[item.datapoint[1]].label;
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: PowerMe.Constants.serviceUrl.baseURL + "/doc/_search?pretty",
                    data: JSON.stringify({
                        "size": 0,
                        "query": {
                            "bool": {
                                "must": {
                                    "term": {
                                        "yearLastAccessed": clickedYAxisLabel
                                    }
                                }
                            }
                        },
                        "aggs": {
                            "fileExtension": {
                                "terms": {
                                    "field": "fileExtension",
                                    "size": 10
                                }
                            }
                        }
                    }),
                    dataType: "json",
                    success: function(chartData) {
                        var payload = chartData.aggregations.fileExtension.buckets;
                        if (payload.length > 0) {
                            $('#fileLastAccessedDrillDownBar').css('display', 'block');
                            function createChartArrays(dataHolder, ticksHolder, fileLastAccessedType, fileLastAccessedCountValue) {
                                var length = fileLastAccessedType.length,
                                    count = 0;
                                for (count; count < length; count++) {
                                    dataHolder.push([fileLastAccessedCountValue[count], count]);
                                    ticksHolder.push([count, fileLastAccessedType[count]]);
                                }
                            }

                            var chartDataArray = [],
                                chartTicksArray = [],
                                fileLastAccessedType = payload.getEach("key"),
                                fileLastAccessedCountValue = payload.getEach("doc_count");
                            fileLastAccessedType = fileLastAccessedType.reverse();
                            fileLastAccessedCountValue = fileLastAccessedCountValue.reverse();
                           
                            createChartArrays(chartDataArray, chartTicksArray, fileLastAccessedType, fileLastAccessedCountValue);

                            var dataSet = [{
                                data: chartDataArray,
                                color: "#57BCC1"
                            }];
                            var chartXLabel = "No. of Files",
                                chartYLabel = "File Type",
                                placeholder = "fileLastAccessedDrillDownBar";
                            var options = createOptionsArray(chartTicksArray, chartXLabel, chartYLabel);
                            renderChartandTooltip(placeholder, dataSet, options);
                            $('#fileLastAccessedBackButton').show();
                            $('#fileLastAccessedBar').css('display', 'none');
                            $('#fileLastAccessedTitle').html("No. of Files Vs. File Type (top 10)");
                        }
                    }
                })
        	}
 
        });

        /*Function ended for 'No. of Files Vs. Last Accessed'*/

        /*Function to create the options object of the charts*/
        
        function createOptionsArray(chartTicksArray, chartXLabel, chartYLabel) {
            var options = {
            		
            		
            	series: {
                    bars: {
                        show: true
                    },
                    
                },
               
                bars: {
                    align: "center",
                    barWidth: .5,
                    horizontal: false,
                    fillColor: {
                        colors: [{
                            opacity: 0.5
                        }, {
                            opacity: 1
                        }]
                    },
                    lineWidth: .5
             
                },
                axesDefaults: {
                	ticks: chartDataArray
                },
                xaxis: {
                	ticks: chartTicksArray,
                	axisLabel: chartXLabel,
                	axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 10,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickColor: "#CCC",
                	//renderer: $.jqplot.CategoryAxisRenderer,
                	
                },
                yaxis: {
                	//ticks: chartDataArray,
                	axisLabel: chartYLabel,
                	 axisLabelUseCanvas: true,
                     axisLabelFontSizePixels: 10,
                     axisLabelFontFamily: 'Verdana, Arial',
                     axisLabelPadding: 20,
                     tickColor: "#CCC",
                    // axisLabel: chartYLabel,
                	

                },
                legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#858585",
                    position: "ne"
                },
                grid: {
                    hoverable: true,
                    borderWidth: 0.1,
                    clickable: true,
                    
                }
            };

            return options;
        }

        /*Function ended */

        /*Function to render chart and tool tip*/
        function renderChartandTooltip(placeholder, dataSet, options) {
            $.plot($("#" + placeholder), dataSet, options);
            setTitleAttribute.apply($("#" + placeholder));
            useTooltip.apply($("#" + placeholder));
        }

        /*Function ended */

        /*Function to show tool tip and set title attribute*/
        var previousPoint = null,
            previousLabel = null;

        function useTooltip() {
            $(this).bind("plothover", function(event, pos, item) {
            	//console.log(item);
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();
                        //console.log(item.datapoint);
                       // console.log(item.datapoint[0]);
                        var x = item.datapoint[1],
                            y = item.datapoint[0];
                        var color = item.series.color;
                       // console.log(y);
                        showTooltip(pos.pageX - 10, pos.pageY + 30, "</strong>" + item.series.xaxis.options.ticks[y][0] + ":<strong>&nbsp;" + x + "</strong>");
                    } else {
                        $("#tooltip").css("top", pos.pageY + 20);
                        $("#tooltip").css("left", pos.pageX);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };

        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 10,
                left: x + 10,
                padding: '6px',
                border: '1px solid #666',
                'font-size': '12px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            }).appendTo("body").fadeIn(200);
        }

        function setTitleAttribute() {
            var dataArrayLength = $('.yAxis').children('.tickLabel').length;
            for (var t = 0; t < dataArrayLength; t++) {
                var content = $('.yAxis .tickLabel')[t].innerHTML;
                $('.yAxis .tickLabel')[t].setAttribute('title', content);
                //$('.yAxis > .tickLabel')[t].innerHTML = $('.yAxis > .tickLabel')[t].innerHTML.slice(0,4);
            }
        }

        /*Function ended*/

    }.on('didInsertElement')
})