/**
 * Controller for usagechart component is defined here.
 */
PowerMe.UsageChartComponent = Ember.Component.extend({
	click: function(evt){
		//$(evt.target).toggleClass("bookmark_icon unbookmark_icon");
		//console.log("is");
	},
  actions: {
	  
  },
  _initialize : function(data){
	  var ctx = this.$(".canvas")[0].getContext("2d");
	  
	  var reportName = this.get('usageStatistics').get("name"),
	  	pathValue = this.get('usageStatistics').get("path");
	  
	  var usageChartDataQuery = {
  			"type": "usageChartURL",
  			"query": {
  			    "size": 1000,
  			    "_source": [
  			        "accessDate",
  			        "usageCount"
  			    ],
  			    "query": {
  			        "bool": {
  			            "should": [
  			                {
  			                    "term": {
  			                        "path": pathValue ? pathValue : "/shared/Distributor/EMEA QBR/graph4"
  			                    }
  			                },
  			                {
  			                    "term": {
  			                        "reportName": reportName ? reportName : "graph4"
  			                    }
  			                }
  			            ]
  			        }
  			    }
  			}
  	};
	  
	  $.ajax({
		  type: "POST",
          contentType: "application/json",
		  url: PowerMe.Constants.serviceUrl.baseURL + '/usagegraph/_search',
		  data: JSON.stringify(usageChartDataQuery.query),
		  dataType: "json",
		 success: function(usageChartData) {
			console.log('In usage chart coomponent');
			var dateArray =[];
     		var usageCountArray = [];
			var chartObject = usageChartData.hits.hits;
			for(var count=0; count< chartObject.length; count++){
				dateArray.push(chartObject[count]._source.accessDate);
				usageCountArray.push(chartObject[count]._source.usageCount);
			}
			dateArray = (dateArray.length > 1)? dateArray: [];
			console.log(dateArray);
			console.log(usageCountArray);
     		
     		var scalexLabel =[];     		
     		var options = {
    				responsive: true,
    				animation: false,
    				bezierCurve: false,
    				scaleShowGridLines : false,
    				datasetFill : false,
    				pointDotRadius : 4.5,
    				tooltipTemplate : "<%if (label){%><%=label%>: <%}%><%= value %>",
    				pointHitDetectionRadius: 0,
    				tooltipXPadding: 13,
    				scaleFontColor : "black",
    				scaleFontSize : 9,
    				scaleBeginAtZero: true,
    				tooltipCaretSize: 0,
    				scalexLabel:scalexLabel
    			};
    		var lineChartData = {
    			labels : dateArray,
    			datasets : [
    				{
    					label: "My first dataset",
    					fillColor : "rgba(151,187,205,0.2)",
    					strokeColor : "black",
    					pointColor : "#FAA41A",
    					pointStrokeColor : "black",
    					pointHighlightFill : "#fff",
    					pointHighlightStroke : "rgba(151,187,205,1)",
    					data : usageCountArray
    				}
    			]
    		}
    		
    		
    		window.myLine = new Chart(ctx).Line(lineChartData,options );
    		
    	  
    	  console.log("inside");
		 }
		 
	  });
	  
	 /* var graphSize = this.get('graphSize'),
	      scalexLabel =[],
	  	  labels = this.get('usageStatistics.date');
	  if(graphSize === "small"){
		  for(var i=0;i < labels.length ; i++){
			  scalexLabel.push(labels[i].charAt(0));
		 }
	  }*/
	  //earlier implementation
	  //tooltipTemplate : "<%if (label){switch (label){case 'Jan':%>January :<%break;case 'Feb':%>February :<%break;case 'Mar':%>March :<%break;case 'Apr':%>April :<%break;case 'May':%>May :<%break;case 'Jun':%>June :<%break;case 'Jul':%>July :<%break;case 'Aug':%>August :<%break;case 'Sep':%>September :<%break;case 'Oct':%>October :<%break;case 'Nov':%>November :<%break;case 'Dec':%>December :<%break;case 'default':%><%=label%>: <%}}%><%= value %>",
	  
  }.on('didInsertElement')
});