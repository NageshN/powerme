/**
 * Controller for bookmarklet component is defined here.
 */
PowerMe.BookMarkletComponent = Ember.Component.extend({
	click: function(evt){
		//$(evt.target).toggleClass("bookmark_icon unbookmark_icon");
		//console.log("is");
	},
  actions: {
	  /**
	   * when user click on bookmark icon request comes here."data" contains the respective information related to that node.
	   * set and get functions will change the bookmarkState and then the action will be send to the parent tempate inside 
	   * which bookmarklet component is defined along with the data.
	   */
    bookmark: function(data) {
    	var self = this;
    	
    	if(data.get("bookmark.bookmarkState")){
    		/**
    		 *  destroyRecord used here will delete the bookmarkInformation from backend 
    		 */
    		data.store.find('Bookmarking', data.get("bookmark.bookmarkId")).then(function (bookmarkEntry) {
    			  bookmarkEntry.destroyRecord(); 
    			  data.set("bookmark.bookmarkState",false);
    			  /**
      			   * action will be sent to the parent template
      			   */
    	    	  self.sendAction('unBookmark',data);
    		});
    		
    	}else{
    		/**
    		 *  New bookmak will be creadted for the specific report and specific user here
    		 */
    		var newBookmark = data.store.createRecord('Bookmarking', {
    		    bookmarkName: "created by "+ self.session.get("email"),
    		    userId: self.session.get("email"),
    		    docId: data.get("id"),
    		    objectName: data.get("objectName"),
    		    objectType: data.get("objectType"),
    		    domain: data.get("domain"),
    		    capability: data.get("capability"),
    		    application: data.get("application")
    			});
    		var onSuccess = function(data){
    			console.log("inside me success");
    		}
    		newBookmark.save().then(function(dest){
    			data.set("bookmark.bookmarkState",true);
    			data.set("bookmark.bookmarkId",PowerMe.bookmarkData._id);
    			/**
    			 * action will be sent to the parent template
    			 */
        		self.sendAction('bookmark',data);
    			console.log("inside me success");
    		});
    	}
    }
  }
});