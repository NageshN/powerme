/**
 * Router map for all of the templates is defined here.
 */
PowerMe.Router.map(function() {
	this.resource('errorPage');
	this.resource('loginPage');
	this.resource('logout');
	this.resource('register');
    this.resource('forgotPassword');
    // put your routes here
    this.resource('navigation', {path: 'start'}, function() {
    	this.resource("results", { path: "/result/:queryType/:query"},function(){
    		this.route("index", { path: "/searchquery/:pageNumber/:sort"});
    		this.resource("reports" , { path: "subjectarea/:subjectArea/:pageNumber/:orderKey"});
    	});
    	this.resource("navigationResults", { path: "/rootfolder/:system/:department/:application/:key/"}, function(){
    		this.route("index", {path: "/"});
    		this.resource("navResultReports", {path: "/:pageNumber"});
    	});
    	this.route("index", { path: "homepage"});
    	this.resource('newArrival');
    	this.resource('bookmarks');
    	this.resource('sankey',{path: 'chart/:id'});
        this.resource('content', {path: 'object/:content_id'});
    });
});