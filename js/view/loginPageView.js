PowerMe.LoginPageView = Ember.View.extend({
    templateName: 'loginPage',
    theValue: null,
    init: function() {
        this._super();
    },
    didInsertElement: function() {
        /**
         * fnResize() function() will resize the navigation bar by adjusting its height
         */
        function fnResize() {
            //document.getElementById("content").style.height = window.innerHeight - 235 + "px";            
        }
        Ember.$(window).resize(function() {
            fnResize();
        });
        fnResize();
    }
});

/**
 * Loading login templates
 */
/*
Ember.$.ajax({
    url: 'template/loginPage.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['loginPage'] = Ember.Handlebars.compile(resp)
    }
});*/

Ember.$.ajax({
    url: 'template/logout.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['logout'] = Ember.Handlebars.compile(resp)
    }
});