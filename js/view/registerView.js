PowerMe.registerView = Ember.View.extend({
    templateName: 'register',
    theValue: null,
    init: function() {
        this._super();
    }
});

/**
 * Loading register templates
 */

Ember.$.ajax({
    url: 'template/register.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['register'] = Ember.Handlebars.compile(resp)
    }
});