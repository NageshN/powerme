PowerMe.BrowseByView = Ember.View.extend({
	systemExpand:function(){
        return this.get('controller.isSystemExpanded');
    }.property('controller.isSystemExpanded'),
	click: function(e){
		console.log('In expanded system list');  
		if(!e){e = window.event;}		
    	var target = $(e.target) || $(e.srcElement);   
    	var parentLI = $(target.parents('li'));
    	if(!($(target.parents('ul')[0]).hasClass('mCustomScrollbar'))){
    		var itemList = parentLI.find('ul li');
    		$(".customScrollBar").mCustomScrollbar({});
        	if(this.get('systemExpand') == false){
        		this.set('systemExpand', true);               	
            	if(itemList.length > 6){
                	totalHeight = itemList.eq(1).height() + itemList.eq(2).height() + itemList.eq(3).height() + itemList.eq(4).height() + itemList.eq(5).height();
                	parentLI.find("ul").css("height",totalHeight+320);
                	//$(".customScrollBar").mCustomScrollbar({});
                } 
        	}
        	else{
        		this.set('systemExpand', false);
        		if(itemList.length > 6) {parentLI.find("ul").css("height",0);}        		
        	}
    	}   	
	}
});