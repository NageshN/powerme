PowerMe.ContentView = Ember.View.extend({
    templateName: 'content',
    didInsertElement: function() {
        //manipulating view after all elements are inserted once
    	var commentList = Ember.$(".commentList");
		commentList.scrollTop(commentList.prop("scrollHeight"));
		
		
		commentList.bind('DOMNodeInserted', function() {
			commentList.scrollTop(commentList.prop("scrollHeight"));
		});
		if($('#collapse-menu').hasClass('hidden') != true){
    		console.log('true');
    		$('.app-content').css('margin-left','250px');
        	
    	}
    	else{
    		$('.app-content').css('margin-left','60px');
    		console.log('false');
    	}
    }
});

/**
 * Loading content templates
 */
Ember.$.ajax({
    url: 'template/content.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['content'] = Ember.Handlebars.compile(resp)
    }
});

PowerMe.sliderView = Ember.View.extend({
    templateName : "slider",
    elementSelector : ".slider",
    didInsertElement: function() {
        this.$('#banner-slide').bjqs({
            animtype      : 'slide',
            height        : 320,
            width         : 620,
            responsive    : true,
            randomstart   : true
          });
    } 
});