PowerMe.NavigationView = Ember.View.extend({

    /**
     * After all the elements are added in the DOM for Navigation template,below jquery function function bind the event on Navigation container
     * and with the help of event delegation when user click on minus/plus image the navigation menu contract and expand accordingly
     */

    templateName: 'navigation',
    didInsertElement: function() {
    	var self = this;
        /**
         * fnResize() funtion() will resize the navigation bar by adjusting its height
         */
        function fnResize() {
            //document.getElementById("content").style.height = window.innerHeight - 235 + "px";
            //document.getElementById("navigation").style.height = window.innerHeight - 265 + "px";
        }
        Ember.$(window).resize(function() {
            fnResize();
        });
        /**
         * below code handles which link is active home or bookmark or newarrival etc.
         */
       /* if(!($(".menuItems a").hasClass("active"))){
        	$(".menuItems a").eq(0).addClass("active")
        }
        Ember.$(".menuItems a").off("click").on("click", function(event) {
        	var element=$(event.target).parent();
        	if(element.index() > 0){
        		$(".menuItems a").eq(0).removeClass("active");
        		//element.removeClass("active");
        	}else {
        		$(".menuItems a").removeClass("active");
        		element.addClass("active");
        	}
        	console.log("inside");
        });
        */
        /*Ember.$(".parent_system_dropDown").off("click").on("click",".system_dropDown", function(event) {
            var target = $(event.delegateTarget),
            	itemList = target.find("ul li");
            target.toggleClass("active");
            if(itemList.length > 6){
            	totalHeight = itemList.eq(1).height() + itemList.eq(2).height() + itemList.eq(3).height() + itemList.eq(4).height() + itemList.eq(5).height();
            	target.find("ul").css("height",totalHeight+10);
            	$(".customScrollBar").mCustomScrollbar({
            		//setHeight: totalHeight
            	});
            	if(!target.hasClass("active")){
                	target.find("ul").css("height",0);
                }
            }
            
            
             
        });*/
        fnResize();
        /**
         * this command will keeps open the first navigation link in the navigation bar.
         */
        /*if(!state.get("controller").persistId){
			$("#navigation").find("li.clickableLI").eq(0).find("img").trigger("click");
		}*/
        var persistId;
        var state = this;

        /**
         * Below event binding will keep tracks of the content page which user is viewing in the navigation bar
         */
        Ember.$("#navigationArrow").off("click").on("click", function() {
            persistId = state.get("controller").persistId;
            if (persistId) {
                Ember.$(document.getElementById(persistId)).parents("ul.ULCollapse").css("display", "block").prev("li.clickableLI").find("img.plus").attr("src", "images/minus.png").removeClass("plus");
                //$('li#'+persistId+'').parents("ul.ULCollapse").css("display","block").prev("li.clickableLI").find("img.plus").attr("src","images/minus.png").removeClass("plus");
            }
        });
        
     /*   Ember.$(".selectionOption").off("click").on("click", function(event) {
        	//self.set("selectedOption",Ember.$(event.target).text());
        	//Ember.$(".selectedArea").html(Ember.$(event.target).text()+' <b class="caret"></b>');
        });*/
        
        
    }
});

/**
 * Loading navigation templates
 */
Ember.$.ajax({
    url: 'template/navigation.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['navigation'] = Ember.Handlebars.compile(resp)
    }
});
Ember.$.ajax({
    url: 'template/navigationResults.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['navigationResults'] = Ember.Handlebars.compile(resp)
    }
});

/*Ember.$.ajax({
    url: 'template/navigationResults/index.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['navigationResults/index'] = Ember.Handlebars.compile(resp)
    }
});*/

Ember.$.ajax({
    url: 'template/navResultReports.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['navResultReports'] = Ember.Handlebars.compile(resp)
    }
});