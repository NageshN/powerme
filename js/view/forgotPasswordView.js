PowerMe.forgotPasswordView = Ember.View.extend({
    templateName: 'forgotPassword',
    theValue: null,
    init: function() {
        this._super();
    }
});

/**
 * Loading forgotPassword templates
 */

Ember.$.ajax({
    url: 'template/forgotPassword.hbs',
    dataType: 'text',
    success: function(resp) {
        Ember.TEMPLATES['forgotPassword'] = Ember.Handlebars.compile(resp)
    }
});