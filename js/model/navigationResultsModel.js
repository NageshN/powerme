PowerMe.NavigationResult = DS.Model.extend({
	system : DS.attr(),
    dayLastAccessed: DS.attr(),
    monthLastAccessed: DS.attr(),
    yearLastAccessed: DS.attr(),
    rootFolder: DS.attr(),
    name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    dateCreated: DS.attr(),
    dateLastModified: DS.attr(),
    dateLastAccessed: DS.attr(),
    fileExtension: DS.attr(),
    reportSize: DS.attr(),
    totalFile : DS.attr(),
    size: DS.attr(),
    bookmark: function(){
    	var temp = {
                "bookmarkName":"",
				"bookmarkState": false,
				"bookmarkId":"",
				"reportId": ""
			};
    	return temp;
    }.property("owner"),
    fileExtensionFromName : function(){
    	var name = this.get("name"),
    	extractfileExtension = name ? name.split("."): "" ;
    	return extractfileExtension[extractfileExtension.length -1].toLowerCase();
    }.property("name"),
    usageStatistics:function(){
    	var usageStatistics={
    			"week" : ["Jan","Feb","Mar","Apr","May","Jun","Jul"],
        		"data" : [8,1,2,9,3,0,10]
        }
    	return usageStatistics;
    }.property("name"),
    flatIconClass: function(){
    	return "flaticon-"+this.get("fileExtensionFromName");
    }.property("fileExtensionFromName"),
    systemUpperCase: DS.attr()
    
});

/**
 * REST Serializer
 */
/*PowerMe.NavigationResultSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    *//**
     * Normalize function for normalizing response
     *//*
    attrs: {
        internalExternal: 'internal/External?',
        complianceRelated: 'complianceRelated?',
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for (var key in srcobj) {
            json[key] = srcobj[key];
        }
        var foundBookmark = false;
        var bookmarkLength = json.bookmarks ? json.bookmarks.length : 0;
        for(var bookmarkIndex=0;bookmarkIndex < bookmarkLength; bookmarkIndex++){
        	if(json.bookmarks[bookmarkIndex].bookmark.userId === PowerMe.userName){
        		foundBookmark = true;
        		json.bookmark={
                        "bookmarkName": json.bookmarks[bookmarkIndex].bookmarkName,
        				"bookmarkState": true
        			}
        	}
        }
        if(!foundBookmark){
        	json.bookmark={
                    "bookmarkName": "",
    				"bookmarkState": false
    			}
        }
        json.bookmark={
                "bookmarkName":"",
				"bookmarkState": false,
				"bookmarkId":"",
				"reportId": ""
			}
        if(!json.usageStatistics){
        	json.usageStatistics={
            		"week" : ["21","22","23","24","25","26","27","28","29","30","31","32"],
            		"data" : [200,223,345,756,23,779,365,100,201,111,475,29]
            }
        }
        json.usageStatistics={
        		"week" : ["21","22","23","24","25","26","27","28","29","30","31","32"],
        		"data" : [200,223,345,756,23,779,365,100,201,111,475,29,19,188]
        }
        return this._super(type, json, property);
    },
    *//**
     * extract function for adding root to payload
     *//*
    extract: function(store, type, payload, id, requestType) {
        payload = payload.hits.hits;
        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);

        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, tempPayload, id, requestType);
    }
});*/