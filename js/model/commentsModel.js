PowerMe.Comments = DS.Model.extend({
	userName:	DS.attr(),
	userId:		DS.attr(),
	docId:		DS.attr(),
	message:	DS.attr(),
	timing: 	DS.attr(),
	didCreate: function(record, data) {
		console.log("_-----------record created in didCreate----------")
	    record.set("id",PowerMe.commentkData._id);
	  }
});

/**
 * PowerMe Bookmarking REST adapter
 */
PowerMe.CommentsAdapter = DS.RESTAdapter.extend({

    findQuery: function(store, type, queryData) {
        var constant = PowerMe.Constants.serviceUrl;
        var findQueryURL = constant.baseURL + constant[queryData.type];
        return this.ajax(findQueryURL, 'POST', {
            data: queryData.query
        })
    },
    find: function(store, type, id) { //copied from ember-data.js - line 1134
        var constant = PowerMe.Constants.serviceUrl;
        var findQueryURL = constant.baseURL + "/bookmark/" + id;
        //var findURL = "http://54.191.26.201:9200/powerme/doc/"+id; 
        return this.ajax(findQueryURL, 'GET');
        // return this.ajax(this.buildfindURL(type.typeKey, id), 'GET');
    },
    deleteRecord: function(store, type, record) {
        var data = this.serialize(record, {
            includeId: true
        });
        var id = record.get('id');
        var constant = PowerMe.Constants.serviceUrl;
        var url = constant.baseURL + constant.bookmarkDelete + id;

        return new Ember.RSVP.Promise(function(resolve, reject) {
            jQuery.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                data: data
            }).then(function(data) {
                console.log("record deleted with id :" + data._id);
                //Ember.run(null, resolve, data);
            }, function(jqXHR) {
                //jqXHR.then = null; // tame jQuery's ill mannered promises
                //Ember.run(null, reject, jqXHR);
            });
        });
    },
    createRecord: function(store, type, record) {
            console.log("create record");
            var constant = PowerMe.Constants.serviceUrl;
            var data = this.serialize(record, {
                includeId: true
            });
            var url = constant.baseURL + constant.addCommentsURL;
            /*var modifiedData = {
                "bookmark name": data.bookmarkName,
                "user id": data.userId,
                "doc id": data.docId,
                "object name": data.objectName,
                "object type": data.objectType,
                "domain": data.domain,
                "capability": data.capabiliy,
                "application": data.application
            };*/
            //var query = JSON.stringify(modifiedData);

            return this.ajax(url, 'POST', {
                data: data
            }).then(function(data) {
                console.log("successfully created the record :"+data._id);
                PowerMe.commentkData = data;
            });
        }
        // return this.ajax(this.buildURL(type.typeKey), 'POST', { data: query });

})

