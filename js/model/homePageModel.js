PowerMe.Tiles = DS.Model.extend({
   
});
PowerMe.FilesLocation = DS.Model.extend({
   
});
PowerMe.SizeLocation = DS.Model.extend({
    
});
PowerMe.FilesAccessed = DS.Model.extend({
    
});
PowerMe.FilesOwner = DS.Model.extend({
    name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    dateCreated: DS.attr(),
    dateLastModified: DS.attr(),
    dateLastAccessed: DS.attr(),
    fileExtension: DS.attr(),
    reportSize: DS.attr(),
    bookmark: DS.attr()
});
PowerMe.SizeOwner = DS.Model.extend({
    name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    dateCreated: DS.attr(),
    dateLastModified: DS.attr(),
    dateLastAccessed: DS.attr(),
    fileExtension: DS.attr(),
    reportSize: DS.attr(),
    bookmark: DS.attr()
});
PowerMe.TagCloud = DS.Model.extend({
    name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    dateCreated: DS.attr(),
    dateLastModified: DS.attr(),
    dateLastAccessed: DS.attr(),
    fileExtension: DS.attr(),
    reportSize: DS.attr(),
    bookmark: DS.attr()
});