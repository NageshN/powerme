/**
 * Result model is defined here 
 */
PowerMe.ResultAdapter = DS.RESTAdapter.extend({
	findQuery: function(store, type, queryData) {
    	console.log("result find query command executed");
        var constant = PowerMe.Constants.serviceUrl;        
        var findQueryURL = constant.baseURL + constant[queryData.type];
        return this.ajax(findQueryURL, 'POST', {
            data: queryData.query
        });
    },
    
	find: function(store, type, id) { //copied from ember-data.js - line 1134
    	console.log("result model find command executed");
        var findURL = PowerMe.Constants.serviceUrl.baseURL+"/sales1/" + id;
        return this.ajax(findURL, 'GET');
    }
});

/* To get array data type*/
DS.ArrayTransform = DS.Transform.extend({
	  deserialize: function(serialized) {
	    return (Ember.typeOf(serialized) == "array")
	        ? serialized 
	        : [];
	  },

	  serialize: function(deserialized) {
	    var type = Ember.typeOf(deserialized);
	    if (type == 'array') {
	        return deserialized
	    } else if (type == 'string') {
	        return deserialized.split(',').map(function(item) {
	            return jQuery.trim(item);
	        });
	    } else {
	        return [];
	    }
	  }
});

PowerMe.register("transform:array", DS.ArrayTransform);

PowerMe.Result = DS.Model.extend({
	name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    system: DS.attr(),
    columnName: DS.attr(),
    folder: DS.attr(),
    formula: DS.attr(),
    subjectArea: DS.attr(),
    tableName: DS.attr(),
    description: DS.attr(),
    idDeleted: DS.attr(),
    subjectArea1: DS.attr(),
    presentationTable: DS.attr(),
    presentationColumn: DS.attr(),
    descPresentationColumn: DS.attr(),
    businessModel: DS.attr(),
    derivedLogicalTable: DS.attr(),
    derivedLogicalColumn: DS.attr(),
    descDerivedLogicalColumn: DS.attr(),
    logicalTable: DS.attr(),
    logicalColumn: DS.attr(),
    descLogicalColumn: DS.attr(),
    logicalTableSource: DS.attr(),
    initializationBlock: DS.attr(),
    variable: DS.attr(),
    database: DS.attr(),
    physicalCatalog: DS.attr(),
    physicalSchema: DS.attr(),
    physicalTable: DS.attr(),
    alias: DS.attr(),
    physicalColumn: DS.attr(),
    descPhysicalColumn: DS.attr(),
    bookmark: DS.attr(),
    extension: DS.attr(),
    flatIconClass: function(){
    	return "flaticon-"+this.get("extension");
    }.property("extension"),
    usageStatistics: DS.attr(),
    usageCount: DS.attr(),
    department: DS.attr(),
    reportName: DS.attr(),
    systemUpperCase: function(){
    	return this.get('system').toUpperCase();
    }.property('system'),
    systemIcon: function(){
    	return this.get('system')+ "_icon";
    }.property('system')
});

PowerMe.ResultsSubjectTile = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});

/**
 * REST Serializer
 */

PowerMe.ResultSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
    attrs: {
        internalExternal: 'internal/External?'
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for (var key in srcobj) {
            json[key] = srcobj[key];
        }
               json.bookmark={
                "bookmarkName":"",
				"bookmarkState": false,
				"bookmarkId":"",
				"reportId": ""
			}
        json.usageStatistics = {};
        
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	console.log('Inside result model');
    	if(payload.hits != undefined){
    		console.log('Result or Navigation Result data');
    		if(payload.hits.hits.length != 0){
        		payload.hits.hits[0].totoalFile=payload.hits.total;
        	}     	  	
            payload = payload.hits.hits;
    	} 
        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);
        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, tempPayload, id, requestType);
    }
});