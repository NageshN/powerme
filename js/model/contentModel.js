/**
 * Content model is defined here 
 */
PowerMe.ContentAdapter = DS.RESTAdapter.extend({
	findQuery: function(store, type, queryData) {
    	console.log("Content model find query command executed");
        var constant = PowerMe.Constants.serviceUrl;           
        var findQueryURL = constant.baseURL + constant[queryData.type];
        return this.ajax(findQueryURL, 'POST', {
            data: queryData.query
        });
    },
    
	find: function(store, type, id) { //copied from ember-data.js - line 1134
    	console.log("Content model find command executed");
        var findURL = PowerMe.Constants.serviceUrl.baseURL+"/system/_report?id="+id ;
        return this.ajax(findURL, 'GET');
    }
});

/* To get array data type*/
DS.ArrayTransform = DS.Transform.extend({
	  deserialize: function(serialized) {
	    return (Ember.typeOf(serialized) == "array")
	        ? serialized 
	        : [];
	  },

	  serialize: function(deserialized) {
	    var type = Ember.typeOf(deserialized);
	    if (type == 'array') {
	        return deserialized
	    } else if (type == 'string') {
	        return deserialized.split(',').map(function(item) {
	            return jQuery.trim(item);
	        });
	    } else {
	        return [];
	    }
	  }
});

PowerMe.register("transform:array", DS.ArrayTransform);

PowerMe.Content = DS.Model.extend({
	name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    system: DS.attr(),
    columnName: DS.attr(),
    folder: DS.attr(),
    formula: DS.attr(),
    subjectArea: DS.attr(),
    tableName: DS.attr(),
    description: DS.attr(),
    descriptionDerivedLogicalColumn: DS.attr(),
    descriptionLogicalColumn: DS.attr(),
    descriptionPhysicalColumn: DS.attr(),
    descriptionPresentationColumn: DS.attr(),
    idDeleted: DS.attr(),
    subjectArea1: DS.attr(),
    presentationTable: DS.attr(),
    presentationColumn: DS.attr(),
    descPresentationColumn: DS.attr(),
    businessModel: DS.attr(),
    derivedLogicalTable: DS.attr(),
    derivedLogicalColumn: DS.attr(),
    descDerivedLogicalColumn: DS.attr(),
    logicalTable: DS.attr(),
    logicalColumn: DS.attr(),
    descLogicalColumn: DS.attr(),
    logicalTableSource: DS.attr(),
    initializationBlock: DS.attr(),
    variable: DS.attr(),
    database: DS.attr(),
    physicalCatalog: DS.attr(),
    physicalSchema: DS.attr(),
    physicalTable: DS.attr(),
    physicalColumn: DS.attr(),
    alias: DS.attr(),
    descPhysicalColumn: DS.attr(),
    bookmark: DS.attr(),
    extension: DS.attr(),
    flatIconClass: function(){
    	return "flaticon-"+this.get("extension");
    }.property("extension"),
    usageStatistics: DS.attr(),
    usageCount: DS.attr(),
    expression1: DS.attr(),
    expression2: DS.attr(),
    department: DS.attr(),
    reportName: DS.attr(),
    systemUpperCase: function(){
    	return this.get('system').toUpperCase();
    }.property('system'),
    systemIcon: function(){
    	return this.get('system')+ "_icon";
    }.property('system') ,
    qlikViewFlag: function(){
    	  return this.get("system") === 'QlikView';
    	    }.property('system'),
    OBIEEFlag: function(){
    	    	  return this.get("system") === 'Obiee';
    	    	    }.property('system'),
    tableauFlag: function(){
    	    	  return this.get("system") === 'Tableau';
    	    	    }.property('system')
});


PowerMe.SimilarityReport = DS.Model.extend({
	name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    system: DS.attr(),
    columnName: DS.attr(),
    folder: DS.attr(),
    formula: DS.attr(),
    subjectArea: DS.attr(),
    tableName: DS.attr(),
    description: DS.attr(),
    idDeleted: DS.attr(),
    subjectArea1: DS.attr(),
    presentationTable: DS.attr(),
    presentationColumn: DS.attr(),
    descPresentationColumn: DS.attr(),
    businessModel: DS.attr(),
    derivedLogicalTable: DS.attr(),
    derivedLogicalColumn: DS.attr(),
    descDerivedLogicalColumn: DS.attr(),
    logicalTable: DS.attr(),
    logicalColumn: DS.attr(),
    descLogicalColumn: DS.attr(),
    logicalTableSource: DS.attr(),
    initializationBlock: DS.attr(),
    variable: DS.attr(),
    database: DS.attr(),
    physicalCatalog: DS.attr(),
    physicalSchema: DS.attr(),
    physicalTable: DS.attr(),
    alias: DS.attr(),
    physicalColumn: DS.attr(),
    descPhysicalColumn: DS.attr(),
    bookmark: DS.attr(),
    extension: DS.attr(),
    flatIconClass: function(){
    	return "flaticon-"+this.get("extension");
    }.property("extension"),
    usageStatistics: DS.attr(),
    usageCount: DS.attr(),
    similarityPercentage: DS.attr(),
    similarityPerFixed: function(){
    	return this.get('similarityPercentage').toFixed(2);
    }.property("similarityPercentage"),
    reportName: DS.attr(),
    department: DS.attr(),
    systemUpperCase: function(){
    	return this.get('system').toUpperCase();
    }.property('system'),
    systemIcon: function(){
    	return this.get('system')+ "_icon";
    }.property('system')  
});

/**
 * REST Serializer
 */

PowerMe.SimilarityReportSerializer = DS.RESTSerializer.extend({
	attrs: {
       
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
       
        json.bookmark={
                "bookmarkName":"",
				"bookmarkState": false,
				"bookmarkId":"",
				"reportId": ""
			}
        json.usageStatistics = {};
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	console.log('Inside similar report content model');    	     	
        this.extractMeta(store, type, payload);
        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, payload, id, requestType);
    }
});

PowerMe.ContentSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
    attrs: {
        internalExternal: 'internal/External?'
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        
        json.bookmark={
                "bookmarkName":"",
				"bookmarkState": false,
				"bookmarkId":"",
				"reportId": ""
			}
        json.usageStatistics = {};
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	
        this.extractMeta(store, type, payload);
        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, payload, id, requestType);
    }
});