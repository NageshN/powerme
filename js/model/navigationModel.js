/**
 * Navigation model is defined here
 */

PowerMe.Navigation = DS.Model.extend({
    system: DS.attr(),
    rootFolder: DS.attr(),
    key: DS.attr()
});

PowerMe.ObieeNavigation = DS.Model.extend({
	key: DS.attr()
});

PowerMe.ObieeSubjectTile = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});

PowerMe.ObieeReportTile = DS.Model.extend({
	name: DS.attr(),
	owner: DS.attr(),
	system: DS.attr(),
	subjectArea: DS.attr(),
	extension: DS.attr()
});


PowerMe.Tile = DS.Model.extend({
	systems : DS.attr(),
	subjectArea: DS.attr(),
    totalNoOfFiles: DS.attr(),
    dashboard: DS.attr()
});

PowerMe.FilesnLocation = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr(),
	countInThousand: function(){
		return this.get("doc_count")/1000;
	}.property("doc_count")
});

PowerMe.FilesnFilesType = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr(),
	countInThousand: function(){
		return this.get("doc_count")/1000;
	}.property("doc_count")
});

PowerMe.SizenLocation = DS.Model.extend({
	key: DS.attr(),
    totalSize: DS.attr(),
    totalSizeValue: function(){
    	return Math.round(this.get("totalSize.value")*100)/100
    }.property("totalSize"),
    totalSizeMB: function(){
    	var size = this.get("totalSize.value")/1024;
    	return Math.round(size*100)/100;
    }.property("totalSize"),
    totalSizeGB: function(){
    	var size = this.get("totalSize.value")/1048576;
    	return Math.round(size*100)/100;
    }.property("totalSize")
});

PowerMe.FilesNLastAccessed = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr(),
	countInThousand: function(){
		return this.get("doc_count")/1000;
	}.property("doc_count")
});
PowerMe.FilesNOwner = DS.Model.extend({
	doc_count: DS.attr(),
	owner: DS.attr(),
	key: DS.attr()
});
PowerMe.SizeNOwner = DS.Model.extend({
	key: DS.attr(),
	size: DS.attr(),
	sizeValue: function(){
		return Math.round(this.get("size.value")*100)/100
	}.property("size")
});
PowerMe.TagCloud = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});


PowerMe.Top5Reporting = DS.Model.extend({
	system: DS.attr(),
	usageCount: DS.attr(),
	subjectArea: DS.attr(),
	reportName: DS.attr(),	
	owner: DS.attr(),
	department: DS.attr(),
	docId: DS.attr()
});

PowerMe.Top5UsingPanel = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});

PowerMe.NavigationLeftPanel = DS.Model.extend({
	key: DS.attr()
});


PowerMe.FileUsageVsSystem = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});

PowerMe.FileNumberVsSystem = DS.Model.extend({
	key: DS.attr(),
	doc_count: DS.attr()
});

PowerMe.FilesAccessedVsMonth = DS.Model.extend({
	month: DS.attr(),
	count: DS.attr()
});

/**
 * REST Serializer
 */
/*PowerMe.NavigationSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    *//**
     * Normalize function for normalizing response
     *//*
    attrs: {
        internalExternal: 'internal/External?',
        complianceRelated: 'compliance Related?',
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for(var key in srcobj) {
        	   json[key] = srcobj[key];
        	}
        return this._super(type, json, property);
    },
    *//**
     * extract function for adding root to payload
     *//*
    extract: function(store, type, payload, id, requestType) {
    	payload=payload.hits.hits;
        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);

        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, tempPayload, id, requestType);
    }
});*/
PowerMe.TileSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
	attrs: {
               
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };
        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        for(var key in srcobj) {
        	   json[key] = srcobj[key];
        	}
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	payload.aggregations.id="12345";
    	payload=payload.aggregations;
        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);
        var payloadArray=[];
        payloadArray.push(tempPayload.tile);
        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return payloadArray;
    }
});
