/**
 * PowerMe Bookmarking REST adapter
 */
PowerMe.BookmarkingAdapter = DS.RESTAdapter.extend({

    findQuery: function(store, type, queryData) {
        var constant = PowerMe.Constants.serviceUrl;
        var findQueryURL = constant.baseURL + constant[queryData.type];
        return this.ajax(findQueryURL, 'POST', {
            data: queryData.query
        })
    },
    find: function(store, type, id) { //copied from ember-data.js - line 1134
        var constant = PowerMe.Constants.serviceUrl;
        var findQueryURL = constant.baseURL + "/bookmark/" + id;
        //var findURL = "http://54.191.26.201:9200/powerme/doc/"+id; 
        return this.ajax(findQueryURL, 'GET');
        // return this.ajax(this.buildfindURL(type.typeKey, id), 'GET');
    },
    deleteRecord: function(store, type, record) {
        var data = this.serialize(record, {
            includeId: true
        });
        var id = record.get('id');
        var constant = PowerMe.Constants.serviceUrl;
        var url = constant.baseURL + constant.bookmarkDelete + id;

        return new Ember.RSVP.Promise(function(resolve, reject) {
            jQuery.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                data: data
            }).then(function(data) {
                console.log("record deleted with id :" + data._id);
                //Ember.run(null, resolve, data);
            }, function(jqXHR) {
                //jqXHR.then = null; // tame jQuery's ill mannered promises
                //Ember.run(null, reject, jqXHR);
            });
        });
    },
    createRecord: function(store, type, record) {
            console.log("create record");
            var constant = PowerMe.Constants.serviceUrl;
            var data = this.serialize(record, {
                includeId: true
            });
            var url = constant.baseURL + constant.createBookmark;
            /*var modifiedData = {
                "bookmark name": data.bookmarkName,
                "user id": data.userId,
                "doc id": data.docId,
                "object name": data.objectName,
                "object type": data.objectType,
                "domain": data.domain,
                "capability": data.capabiliy,
                "application": data.application
            };*/
            //var query = JSON.stringify(modifiedData);

            return this.ajax(url, 'POST', {
                data: data
            }).then(function(data) {
                console.log("successfully created the record :"+data._id);
                PowerMe.bookmarkData = data;
            });
        }
        // return this.ajax(this.buildURL(type.typeKey), 'POST', { data: query });

})
/**
 * Bookmarking model is defined here ,This is the base for bookmark template
 */
PowerMe.Bookmarking = DS.Model.extend({
    bookmarkName: DS.attr(),
    userId: DS.attr(),
    docId: DS.attr(),
});
/**
 * Bookmark model is defined here ,This is the extended from the Bookmarking. Bookmarking model is used for
 * creating and deleting the record.
 */
PowerMe.Bookmark = PowerMe.Bookmarking.extend({
    name: DS.attr(),
    path: DS.attr(),
    owner: DS.attr(),
    dateCreated: DS.attr(),
    dateLastModified: DS.attr(),
    dateLastAccessed: DS.attr(),
    size:DS.attr(),
    extension: DS.attr(),
    reportSize: DS.attr(),
    usageStatistics:DS.attr(),
    system : DS.attr(),
    dayLastAccessed: DS.attr(),
    monthLastAccessed: DS.attr(),
    yearLastAccessed: DS.attr(),
    rootFolder: DS.attr(),
    bookmark: function(){
    	var obj= {
    			"bookmarkName": this.get("bookmarkName"),
    	        "bookmarkState": true,
    	        "bookmarkId": this.get("id"),
    	        "reportId": this.get("docId")
    	}
    	return obj;
    }.property("bookmarkName","userId","docId"),
    fileExtensionFromName : function(){
    	var name = this.get("name"),
    	extractfileExtension = name ? name.split("."):"";
    	return extractfileExtension[extractfileExtension.length -1].toLowerCase();
    }.property("name"),
    flatIconClass: function(){
    	return "flaticon-"+this.get("extension");
    }.property("extension"),
    usageCount: DS.attr(),
    systemUpperCase: function(){
    	return this.get('system').toUpperCase();
    }.property('system'),
    systemIcon: function(){
    	return this.get('system')+ "_icon";
    }.property('system')
});


/**
 * REST Serializer
 */
PowerMe.BookmarkSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for (var key in srcobj) {
            json[key] = srcobj[key];
        }
        /* var foundBookmark = false;
        var bookmarkLength = json.bookmarks ? json.bookmarks.length : 0;
        for(var bookmarkIndex=0;bookmarkIndex < bookmarkLength; bookmarkIndex++){
        	if(json.bookmarks[bookmarkIndex].bookmark.userId === PowerMe.userName){
        		foundBookmark = true;
        		json.bookmark={
                        "bookmarkName": json.bookmarks[bookmarkIndex].bookmarkName,
        				"bookmarkState": true
        			}
        	}
        }
        if(!foundBookmark){
        	json.bookmark={
                    "bookmarkName": "",
    				"bookmarkState": false
    			}
        }*/
        /*json.bookmark = {
            "bookmarkName": json.bookmarkName,
            "bookmarkState": true,
            "bookmarkId": json.id,
            "reportId": json.docId
        }*/
        json.reportName = "";
        json.department = "",
        json.path = "";
        json.owner = "";
        json.fileExtension = "";
        json.system = "";
        json.dayLastAccessed = "";
        json.monthLastAccessed = "";
        json.yearLastAccessed = "";
        json.rootFolder = "";
        json.size = "";
        if(!json.usageStatistics){
        	json.usageStatistics={
        			"week" : ["Jan","Feb","Mar","Apr","May","Jun","Jul"],
            		"data" : [8,1,2,9,3,0,10]
            }
        }
        /*json.usageStatistics = {
            "week": ["21","22","23","24","25","26","27","28","29","30","31","32"],
            "data": [200,223,345,756,23,779,365,100,201,111,475,29,19,188]
        }*/
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	if(payload.hits.hits.length != 0){
    		payload.hits.hits[0].totoalFile=payload.hits.total;
    	}
    	payload = payload.hits.hits;
        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);

        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, tempPayload, id, requestType);
    }
});
PowerMe.BookmarkingSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };

        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for (var key in srcobj) {
            json[key] = srcobj[key];
        }
               return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
        if (requestType === "findQuery") {
            payload = payload.hits.hits;
        } else {
            payload = payload;
        }

        var tempPayload = {};
        tempPayload[type.typeKey] = payload;
        this.extractMeta(store, type, tempPayload);

        var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
        return this[specificExtract](store, type, tempPayload, id, requestType);
    }
});