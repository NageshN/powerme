/**
 * PowerMe application template REST adapter by default request comes here,
 * for any template until/unless you override application REST Adapter. 
 */
PowerMe.ApplicationAdapter = DS.RESTAdapter.extend({
    /*
    This is the function envoked for basic search queries from homepage.
    findQueryURL needs to adjusted based on server and environment settings.
    */
    findQuery: function(store, type, queryData) {    	
    	console.log("application find query command executed");    	
        var constant = PowerMe.Constants.serviceUrl;               
        var findQueryURL = constant.baseURL + constant[queryData.type];
        return this.ajax(findQueryURL, 'POST', {
            data: queryData.query
        });
    },

    /*
    This is the function envoked for search queries on individual IDs. It is also called when content page is refreshed.
    findQueryURL needs to adjusted based on server and environment settings.
    */
    find: function(store, type, id) { //copied from ember-data.js - line 1134
    	console.log("application find command executed");
        var findURL = PowerMe.Constants.serviceUrl.baseURL+"/sales/" + id;
        return this.ajax(findURL, 'GET');
        // return this.ajax(this.buildfindURL(type.typeKey, id), 'GET');
    }

});
PowerMe.ApplicationSerializer = DS.RESTSerializer.extend({
    //primaryKey: "objectName", //here objectName acts as an uniqueId.
    /**
     * Normalize function for normalizing response
     */
	attrs: {
        internalExternal: 'internal/External?'
    },
    normalize: function(type, hash, property) {
        var json = {
            id: hash.id
        };
        for (var prop in hash) {
            json[prop.camelize()] = normalizeData(hash[prop]);

        }
        var srcobj = JSON.parse(JSON.stringify(json.source));
        delete json.source;
        for(var key in srcobj) {
        	   json[key] = srcobj[key];
        	}
        return this._super(type, json, property);
    },
    /**
     * extract function for adding root to payload
     */
    extract: function(store, type, payload, id, requestType) {
    	function buildId(objectData){
    		var count=0;
    		for(count;count < objectData.length;count += 1){
    			objectData[count].id=count+1;
    		}
    	}
    	if(type.typeKey === "obieeNavigation"){
    		buildId(payload.aggregations.system.buckets);
    		payload=payload.aggregations.system.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
    		
    	}else if(type.typeKey === "top5UsingPanel"){
    		buildId(payload.aggregations.users.buckets);
    		payload=payload.aggregations.users.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "navigationLeftPanel"){
    		buildId(payload.aggregations.system.buckets);
    		payload=payload.aggregations.system.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "fileUsageVsSystem"){
    		buildId(payload.aggregations.systems.buckets);
    		payload=payload.aggregations.systems.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "fileNumberVsSystem"){
    		buildId(payload.aggregations.systems.buckets);
    		payload=payload.aggregations.systems.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "obieeSubjectTile"){
    		buildId(payload.aggregations.group_by_rootFolder.buckets)
    		payload=payload.aggregations.group_by_rootFolder.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "resultsSubjectTile"){
    		buildId(payload.aggregations.group_by_subjectArea.buckets)
    		payload=payload.aggregations.group_by_subjectArea.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "filesnLocation"){
    		buildId(payload.aggregations.group_by_rootFolder.buckets)
    		payload=payload.aggregations.group_by_rootFolder.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "filesnFilesType"){
    		buildId(payload.aggregations.group_by_rootFolder.buckets)
    		payload=payload.aggregations.group_by_rootFolder.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "sizenLocation"){
    		buildId(payload.aggregations.group_by_rootFolder.buckets)
    		payload=payload.aggregations.group_by_rootFolder.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
    		
    	}else if(type.typeKey === "filesNLastAccessed"){
    		buildId(payload.aggregations.lastAccessed.buckets)
    		payload=payload.aggregations.lastAccessed.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "filesNOwner"){
    		buildId(payload.aggregations.owners.buckets)
    		payload=payload.aggregations.owners.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
    		
    	}else if(type.typeKey === "sizeNOwner"){
    		buildId(payload.aggregations.owner.buckets)
    		payload=payload.aggregations.owner.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
    		
    	}else if(type.typeKey === "navigation"){
    		buildId(payload.aggregations.system.buckets)
    		payload=payload.aggregations.system.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
            
    	}else if(type.typeKey === "tagCloud"){
    		buildId(payload.aggregations.fileExtension.buckets)
    		payload=payload.aggregations.fileExtension.buckets;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return tempPayload[type.typeKey];
    		
    	}else{
    		if(payload.hits.hits.length != 0){
        		payload.hits.hits[0].totoalFile=payload.hits.total;
        	}
    		payload = payload.hits.hits;
            var tempPayload = {};
            tempPayload[type.typeKey] = payload;
            this.extractMeta(store, type, tempPayload);
            var specificExtract = "extract" + requestType.charAt(0).toUpperCase() + requestType.substr(1);
            return this[specificExtract](store, type, tempPayload, id, requestType);
    	}
    	
    }
});
/**                  
 *avoidSpaceInArr,avoidSpaceInObj & normalizeData these functions is used globally across the application.
 *
 */
/**
 * avoidSpaceInArr function is to avoid space in Object within array
 */
function avoidSpaceInArr(arr) {
        var returnVal = []
        for (var i = 0; i < arr.length; i++) {
            returnVal[i] = normalizeData(arr[i]);
        }
        return returnVal;
    }
    /**
     * avoidSpaceInObj function is to avoid space in Object
     */

function avoidSpaceInObj(obj) {

        var jsons = {};
        for (var props in obj) {
            jsons[props.camelize()] = normalizeData(obj[props]);
        }

        return jsons;
    }
    /**
     * normalizeData function is to normalize data, depends on data type
     */

function normalizeData(item) {
    if (Ember.typeOf(item) == "array") {
        return avoidSpaceInArr(item)
    } else if (Ember.typeOf(item) == "object") {
        return avoidSpaceInObj(item)
    } else
        return item;
}

