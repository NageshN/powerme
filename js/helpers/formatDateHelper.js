Ember.Handlebars.helper('format-date',function(value) {
	var month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
		date = new Date(value),
		formattedDate = date.getDay() +" " +month[date.getMonth()-1]+" "+date.getFullYear()+", at "+ (date.getHours()<10?'0':'') + date.getHours()+":"+
		(date.getMinutes()<10?'0':'') + date.getMinutes() +":"+(date.getSeconds()<10?'0':'') + date.getSeconds();
	return formattedDate;
});

Ember.Handlebars.helper('upper-case',function(value) {
	return value.toUpperCase();
});



