/**
 * Ember application is initialized with PowerMe
 */
PowerMe = Ember.Application.create({
	LOG_STATE_TRANSITIONS: true,
    LOG_TRANSITIONS: true,
    LOG_BINDINGS : true
});

var inflector = Ember.Inflector.inflector;

inflector.uncountable('comments');

//PowerMe.count = 0;
